<?php
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_File"></div>';
// ==== menu: menus-ksfv3 ==== //
$mn='ksfv3';
$pagePath=PAGESLOCALES_ROOT."$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath.$p.'.php');
    //$m->setAttr($p,'visible',1);
    //$m->setAttr($p,'visible',ISCONNECT===1?0:1);
    $m->setAttr($p,'menuTitre','Agoria');                  // afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
    $m->setAttr($p,'menuTitle','Agoria');               // afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
    $m->setAttr($p,'titre','Agoria le jeux de r&ocirc;le Grandeur Nature');           // titre de la page: afficher dans le bas de page
//  $m->setMeta($p,'title','tutoriels - accueil(meta)');   // meta <title> (si non definit title=titre)
//  $m->addCssA($p,'dossier1');                          // applique le style dossier1 a la balise <a>

        
if(ISJOUEUR===1){
    // - joueur(profil)) - //
    $p='profils';
    $m->addCallPage($p,$pagePath.'joueurs/'.$p.'.php');
        //$m->setAttr($p,'menuTitre','joueur ('.$joueur->get('jou_nom').')');
        //$m->setAttr($p,'menuTitre',JOUNOM);
        $m->setAttr($p,'menuTitre','Joueur');
        $m->setAttr($p,'menuTitre','joueur');
        $m->setAttr($p,'titre','profil du joueur');

    // - personnage(fiche) - //

    // -- SELECT des personnages -- //
    $p='perso-fiche';
    $m->addCallPage($p,$pagePath.'perso/'.$p.'.php');
        //$m->setAttr($p,'menuTitre','personnage('.$persoActif->get('per_prenom').' '.$persoActif->get('per_nom').')');
        $m->setAttr($p,'menuTitre','personnage');
        $m->setAttr($p,'titre','personnage');

    $p='messagerie';
    $m->addCallPage($p,$pagePath."orgas/orgas-messagerie.php"); // réunir les 2 en 1
        $m->setAttr($p,'menuTitre','messagerie');
        $m->setAttr($p,'titre','messagerie commune MJ / perso');

    $p='perso-messagerie';
    $m->addCallPage($p,$pagePath."perso/$p.php");
        $m->setAttr($p,'visible',0);
        $m->setAttr($p,'menuTitre','messagerie');
        $m->setAttr($p,'titre','messagerie');


    $p='perso-echDec';
    $m->addCallPage($p,$pagePath.'perso/'.$p.'.php');
        $m->setAttr($p,'menuTitre','&eacute;changes/d&eacute;couvertes');
        $m->setAttr($p,'titre','échange entre personnages et d&eacute;couvertes');    

    $p='perso-echanges';
    $m->addCallPage($p,$pagePath.'perso/'.$p.'.php');
        $m->setAttr($p,'visible',0);
        $m->setAttr($p,'menuTitre','échanges');
        $m->setAttr($p,'titre','échange entre personnages');    

    $p='perso-decouvertes';
    $m->addCallPage($p,$pagePath.'perso/'.$p.'.php');
        $m->setAttr($p,'visible',0);
        $m->setAttr($p,'menuTitre','découverte');
        $m->setAttr($p,'titre','découvrir un message/ Liste des messages découverts');    
}//if(ISJOUEUR==1){


// -- connexion/deconnexion -- //
if(ISCONNECT===1){
    // -- bug & toDo -- //
    $p='bugsToDo';
    $m->addCallPage($p,$pagePath.'devs/'.$p.'.php');
        $m->setAttr($p,'visible',0);// invisible
        $m->setAttr($p,'menuTitre','rapport de bugs');
        $m->setAttr($p,'titre','rapport de bugs et toDo');
        $m->addCssLI($p,'msDevs');


    // -- connecter montre le menu déconnection -- //
    $p='deconnect';
    $m->addCallPage($p,$pagePath.$p.'.php');
        $menuTitre='se déconnecter';
    $titre=$menuTitre;
}
else{
    // -- deconnecter montre le menu connection -- //
    $p='connect';
    $m->addCallPage($p,$pagePath.$p.'.php');
        $menuTitre='se connecter';
    $titre=$menuTitre;
}

$m->setAttr($p,'menuTitre',$menuTitre);
$m->setAttr($p,'titre',$titre);

