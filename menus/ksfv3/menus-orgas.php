<?php
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_File"></div>';
global $menuOrgas;
$ext='.html';
// ==== menu: menus-orgas ==== //

$mn='orgas';
$pagePath=PAGESLOCALES_ROOT."ksfv3/$mn/";

$p='accueil';
$m=$menuOrgas->addMenu($mn,$p,$pagePath.$p.'.php');
    $m->addClasses('msOrgas');
    $m->setAttr($p,'visible',0);
    $m->setAttr($p,'menuTitre',"organisateurs"); 
    $m->setAttr($p,'menuTitle',"organisateurs");
    $m->setAttr($p,'titre',"accées organisateurs");

$p='orgas-joueurs';
$m->addCallPage($p,$pagePath.$p.'.php');
    $m->setAttr("$p",'menuTitre','joueurs');
    $m->setAttr("$p",'titre',"les joueurs");


$p='orgas-persos';
$m->addCallPage($p,$pagePath.$p.'.php');
    $m->setAttr("$p",'menuTitre','personnages');
    $m->setAttr("$p",'titre',"Vue d'ensemble des personnages");
    
$p='orgas-plannings';
$m->addCallPage($p,$pagePath.$p.'.php');
    $m->setAttr("$p",'menuTitre','plannification');
    $m->setAttr("$p",'titre',"plannification");

$p='orgas-decouvertes';
$m->addCallPage($p,$pagePath.$p.'.php');
    $m->setAttr("$p",'menuTitre','decouvertes');
    $m->setAttr("$p",'titre','gestion des DECOUVERTES');

$p='orgas-competences';
$m->addCallPage($p,$pagePath.$p.'.php');
    $m->setAttr("$p",'menuTitre','les compétences');
    $m->setAttr("$p",'titre','les compétences');
    
$p='orgas-items';
$m->addCallPage($p,$pagePath.$p.'.php');
    $m->setAttr("$p",'menuTitre','items');
    $m->setAttr("$p",'titre','items');

$p='orgas-stats';
$m->addCallPage($p,$pagePath.$p.'.php');
    $m->setAttr("$p",'menuTitre','les stats');
    $m->setAttr("$p",'titre','les stats');

$p='orgas-logs';
$m->addCallPage($p,$pagePath.$p.'.php');
    $m->setAttr("$p",'menuTitre','les logs');
    $m->setAttr("$p",'titre','les journaux');

$p='orgas-logsTextes';
$m->addCallPage($p,$pagePath.$p.'.php');
    $m->setAttr("$p",'menuTitre','logs-textes');
    $m->setAttr("$p",'titre','les textes des logs');

$p='orgas-logs-connect';
$m->addCallPage($p,$pagePath.$p.'.php');
    $m->setAttr("$p",'menuTitre','logs-connect');
    $m->setAttr("$p",'titre',"log connexion des joueurs");
