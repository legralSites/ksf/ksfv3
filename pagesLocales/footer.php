<!-- footer -->
<!-- footer gauche-->
<div id="footer">
<div id="footerGauche">
<?php
define('DUREE',microtime(TRUE)-SCRIPTDEBUT);
//if(( defined('VERSIONSTATIQUE')) AND (VERSIONSTATIQUE == 1) ){
//	echo 'version statique(g&eacute;n&eacute;r&eacute;e le: '.date('d/m/Y').')';
//}
//else{
//	echo 'page g&eacute;n&eacute;r&eacute; en: '.DUREE.'s';
//}
?></div><!-- footer gauche: fin-->

<!-- footer centre -->
<?php global $gestMenus;
if(ISDEV===1){
	echo $gestMenus->getLastTitre().'<br>';   //afficher le titre de la page
	echo 'joueurs connéctés: ';
	
	// - table des joueurs connectes - //
	$jc=new gestTable('ksfv3',TBLPREFIXE.'joueurs','jou_id'
		,[
		'SELECT'=>'jou_id,jou_nom'
		,'WHERE'=>'jou_ts + INTERVAL 900 SECOND >= CURRENT_TIMESTAMP'
		]
	);
	foreach ($jc->get() as $noms)echo $noms['jou_nom'].' ';


/*
<span class="licence">
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
<img alt="Licence Creative Commons" src="./styles/img/licenceCCBY-88x31.png" /></a><br />Mise &agrave; disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">licence Creative Commons Attribution 4.0 International</a>.<br />
<a xmlns:dct="http://purl.org/dc/terms/" href="http://legral.fr" rel="dct:source">http://legral.fr</a>.</span>
 */?>
<!-- footer centre: fin -->

<!-- footer droit -->
<div id="footerDroit"><a href="?ksfv3=bugsToDo">Rapporter un bug</a><br>
<?php
 echo 'serveur reseau:'.SERVER_RESEAU.' - ';
 echo 'serveur nom:'.SERVER_NAME.' - ';
 echo 'IP:'.SERVER_ADDR.'<br>';

?>
</div>
<!-- footer droit: fin -->

</div><!-- footer -->
<?php include PAGESLOCALES_ROOT.'piwik.php';

}//if(ISDEV===1){

// - log de la connexion  du joueur - //
$menu='';
$page='';
if(
	SERVER_RESEAU ==='LOCALHOST'
OR	(JOUNO!=1 AND JOUNO!=14 AND JOUNO!=24 AND JOUNO!=25 AND ISCONNECT===1)
){
	if(isset($gestMenus->ariane->pile[0]['page'])){
		$menu=$gestMenus->ariane->pile[0]['menu'];
		$page=$gestMenus->ariane->pile[0]['page'];
	}
	// surcharge
	//echo gestLib_inspect('$menuOrgas->ariane->pile[0]["page"]',$menuOrgas->ariane->pile[0]['page']);
    elseif(isset($menuOrgas->ariane->pile[0]['page'])){
		$menu=$menuOrgas->ariane->pile[0]['menu'];
		$page=$menuOrgas->ariane->pile[0]['page'];
	}
    $dbksfV3->sql->clear();
    $userAgent=$_SERVER['HTTP_USER_AGENT'];
	$dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'logs_connect` (`engine-version`,`instance-version`,loc_jouId,loc_duree,loc_IP,loc_menu,loc_page,loc_userAgent) VALUES ("'.AGORIA_VERSION.'","'.AGORIA_INSTANCE_VERSION.'",'.JOUNO.',"'.DUREE.'","'.REMOTE_ADDR.'"'.",'$menu','$page','$userAgent')");
	$sql=$dbksfV3->query();	$dbksfV3->queryClose();
	if(ISDEV===1)echo "<div class='coding_code'>$sql</a></div>";
}
