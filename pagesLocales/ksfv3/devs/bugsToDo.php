<h1 class="h1">Rapport de bugs et chose (demande) à faire</h1>


<?php
global $dbksfV3;


// ********************* //
//     bugs et todos    //
// ********************* //
function isSelected($genre,$Genre){
	return ($genre==$Genre)?'style="background-color:cyan" selected="selected"':'style="background-color:transparent"';
}

global $strGenre,$strPriorite,$strEtat;

$strGenre=array();
$strGenre[0]='N/A';
$strGenre[1]='bug';
$strGenre[2]='toDo';

$strPriorite=array();
$strPriorite[0]='aucune';
$strPriorite[1]='tres basse';
$strPriorite[2]='basse';
$strPriorite[3]='normal';
$strPriorite[4]='haute';
$strPriorite[5]='tres haute';
$strPriorite[6]='critique';

$strEtat=array();
$strEtat[0]='pas commencé';
$strEtat[1]='en dev';
$strEtat[2]='à testé';
$strEtat[3]='en prod';

// - edition des bugsEtTodo - //
if( (isset($_POST['bugNb']) AND $_POST['bugNb']>0 ) 
OR  (isset($_POST['todNb']) AND $_POST['todNb']>0 ) ) {
	//echo __FILE__.' edition de bigToDo'; 
	$dbksfV3->sql->clear();
	$sqlOp='';
	foreach($_POST as $key => $value){
		$sql='';

		$cmd=substr($key,0,11);	// on recupere la cmd sans la refId
		$id= substr($key,11,strlen($key));	//on recupere id en enlevant de  la cmd (tailleFixe)

		switch($cmd){
			case 'tdoGenrEdit':case 'bugGenrEdit':
				$sql='bug_genre='.$value;
				break;

			case 'tdoPrioEdit':case 'bugPrioEdit':
				$sql='bug_priorite='.$value;
				break;

			case 'tdoEtatEdit':case 'bugEtatEdit':
				$sql='bug_etat='.$value;
				break;

			case 'tdoDescEdit':case 'bugDescEdit':
				$sql='bug_description="'.addslashes($value).'"';
				break;
		}
		if($sql!="")$sqlOp.='UPDATE '.TBLPREFIXE."bugs Set $sql WHERE bug_id=$id;\n";
	}
	$dbksfV3->sql->setOPERATION($sqlOp); $sql=$dbksfV3->query(); 	$dbksfV3->queryClose();
	if(ISDEV===1)echo "<div class='coding_code'>$sql</a></div>";
	//logAdd(PERSONO,  'dev'    ,1);
	
}//if(isset($_POST['bugNb'])){

global $bugsEtTodos;$bugsEtTodos=array();
$dbksfV3->sql->clear();
$dbksfV3->sql->setORDERBY('bug_priorite DESC');
tableVarLoad(TBLPREFIXE.'bugs',$bugsEtTodos,'bug_id',0);

//if(ISDEV===1)echo gestLib_inspect('$bugsEtTodos',$bugsEtTodos);

function todosShow(){
	global $bugsEtTodos,$strGenre,$strPriorite,$strEtat;
	$out= '';
	foreach($bugsEtTodos as   $key => $bug){

	if ($bug['bug_genre']!=='2')continue;// on ne veut que les todos

		$bug_id=$bug['bug_id'];

		// defini les noms des champs du form,  defini les valeurs des champs
		$genre=$bug['bug_genre'];
		$etat=$bug['bug_etat'];
		$priorite=$bug['bug_priorite'];
		$description=$bug['bug_description'];
		$o='';
		$o.= '<span class="">'.$bug_id.'</span>';

		$o.= '<span class="">'.$strGenre[$genre].'</span>';
		$o.= '<span class="">'.$strEtat [$etat].'</span>';
		$o.= '<span class="">'.$strPriorite [$priorite].'</span>';
		$o.= '<span class="">'.$description.'</span>';
		$out.= $o;
	}	// foreach($bugsEtTodo as   $key => $val){
return $out;
}//function todosShow()



function todosEditShow(){
	global $bugsEtTodos,$strGenre,$strPriorite,$strEtat;
	
$out= '<a name="toDos"></a><form method="POST" action="?'.ARIANE_AGORIA.'#toDos">';
$out.='<table><caption>toDo (<a href="?create=toDo&amp;'.ARIANE_AGORIA.'">ajouter</a> - <input type="submit" value="sauver" />) </caption>';
$out.='<thead><tr><th style="display:none;">id</th> <th>genre</th> <th>priorité</th> <th>etat</th><th>description</th></thead></tr>';
$bugNb=0;

foreach($bugsEtTodos as $key => $bug){

	if ($bug['bug_genre']==='2'){// on ne veut que les toDos
		$bugNb++;
		$bug_id=$bug['bug_id'];

		// defini les noms des champs du form,  defini les valeurs des champs
		$gid="tdoGenr$bug_id";	$genre=$bug['bug_genre'];
		$pid="tdoPrio$bug_id";	$priorite=$bug['bug_priorite'];
		$eid="tdoEtat$bug_id";	$etat=$bug['bug_etat'];
		$did="tdoDesc$bug_id";	$description=$bug['bug_description'];

		$o='';
		$o.= '<tr><td style="display:none;">'.$bug_id.'</td>';

		$o.= 
			'<td>'.IMG_EDITNC.' onclick="inlineSwitch(\''.$gid.'\')" />'.$strGenre[$genre]
		.	'<span id="'.$gid.'" style="display:none;">'
		.		'<br><select name="tdoGenrEdit'.$bug_id.'">';
					for ($i=1;$i<count($strGenre);$i++){$o.='<option value='.$i.' '.isSelected($genre,$i).'>'.$strGenre[$i].'</option>';}
		$o.=		'</select>'
		.	'</span></td>';

		$o.=	 '<td>';
		if(ISDEV===1){
			$o.=	IMG_EDITNC.' onclick="inlineSwitch(\''.$pid.'\')" />'.$strPriorite[$priorite]
			.	'<span id="'.$pid.'" style="display:none;">'
			.		'<br><select name="tdoPrioEdit'.$bug_id.'">';
						for ($i=count($strPriorite)-1;$i>=0;$i--){$o.= '<option value='.$i.' '.isSelected($priorite,$i).'>'.$strPriorite[$i].'</option>';}
			$o.=		'</select></span>';
		}
		else $o.=$strPriorite[$priorite];
		echo'</td>';

		$o.='<td>';
		if(ISDEV===1){
			$o.=IMG_EDITNC.' onclick="inlineSwitch(\''.$eid.'\')" />'.$strEtat[$etat]
			.	'<span id="'.$eid.'" style="display:none;">'
			.		'<br><select name="tdoEtatEdit'.$bug_id.'">';
						for ($i=count($strEtat)-1;$i>=0;$i--){$o.='<option value='.$i.' '.isSelected($etat,$i).'>'.$strEtat[$i].'</option>';}
			$o.= 		'</select></span>';
		}
		else $o.=$strEtat[$etat];
		$o.='</td>';

		//$style=($etat==3)?' style="text-decoration:line-through" ':'';
		$style='';
		$o.=
			'<td '.$style.'>'.IMG_EDITNC.' onclick="inlineSwitch(\''.$did.'\')" />'.ln2br($description)
		.	'<span id="'.$did.'" style="display:none;">'
		.		'<textarea style="width:100%; min-height:2em;" name="tdoDescEdit'.$bug_id.'" type="text"  value="'.$description.'">'.$description.' </textarea>'
		.	'</span></td>'

		.	'</tr>';
		$out.= $o;
	}

}	// foreach($bugsEtTodo as   $key => $val){

$out.= '</table>';
$out.= '<input name="todNb" type="hidden" value="'.$bugNb.'" />';
$out.= '</form>';
return $out;
}//function todosEditShow()



function bugsShow(){
	global $bugsEtTodos,$strGenre,$strPriorite,$strEtat;
	$out= '';
	foreach($bugsEtTodos as   $key => $bug){

	if ($bug['bug_genre']!=='1')continue;// on ne veut que les bugs

		$bug_id=$bug['bug_id'];

		// defini les noms des champs du form,  defini les valeurs des champs
		$genre=$bug['bug_genre'];
		$etat=$bug['bug_etat'];
		$priorite=$bug['bug_priorite'];
		$description=$bug['bug_description'];
		$o='';
		$o.= '<span class="">'.$bug_id.'</span>';

		$o.= '<span class="">'.$strGenre[$genre].'</span>';
	 	$o.= '<span class="">'.$strEtat [$etat].'</span>';
		$o.= '<span class="">'.$strPriorite [$priorite].'</span>';
		$o.= '<span class="">'.$description.'</span>';
		$out.= $o;
	}	// foreach($bugsEtTodo as   $key => $val){
return $out;
}//function bugsShow()



function bugsEditShow(){
	global $bugsEtTodos,$strGenre,$strPriorite,$strEtat;
	
$out= '<a name="bugs"></a><form method="POST" action="?'.ARIANE_AGORIA.'#bugs">';
$out.='<table><caption>bugs (<a href="?create=bug&amp;'.ARIANE_AGORIA.'">ajouter</a> - <input type="submit" value="sauver" />) </caption>';
$out.='<thead><tr><th style="display:none;">id</th> <th>genre</th> <th>priorité</th> <th>etat</th><th>description</th></thead></tr>';
$bugNb=0;
foreach($bugsEtTodos as   $key => $bug){

	if ($bug['bug_genre']=='1'){// on ne veut que les bugs
		$bugNb++;
		$bug_id=$bug['bug_id'];

		// defini les noms des champs du form,  defini les valeurs des champs
		$gid="bugGenr$bug_id";	$genre=$bug['bug_genre'];
		$pid="bugPrio$bug_id";	$priorite=$bug['bug_priorite'];
		$eid="bugEtat$bug_id";	$etat=$bug['bug_etat'];
		$did="bugDesc$bug_id";	$description=$bug['bug_description'];

		$o='';
		$o.= '<tr><td style="display:none;">'.$bug_id.'</td>';

		$o.= 
			'<td>'.IMG_EDITNC.' onclick="inlineSwitch(\''.$gid.'\')" />'.$strGenre[$genre]
		.	'<span id="'.$gid.'" style="display:none;">'
		.	'<br><select name="bugGenrEdit'.$bug_id.'">';
				for ($i=1;$i<count($strGenre);$i++){$o.='<option value='.$i.' '.isSelected($genre,$i).'>'.$strGenre[$i].'</option>';}
		$o.=	'</select>'
		.	'</span></td>'
		;

		$o.= '<td>';
		if(ISDEV===1){
			$o.=	IMG_EDITNC.' onclick="inlineSwitch(\''.$pid.'\')" />'.$strPriorite[$priorite]
			.	'<span id="'.$pid.'" style="display:none;">'
			.	'<br><select name="bugPrioEdit'.$bug_id.'">';
					for ($i=0;$i<count($strPriorite);$i++){$o.='<option value='.$i.' '.isSelected($priorite,$i).'>'.$strPriorite[$i].'</option>';}
			$o.=	'</select></span>';
		}
		else $o.=$strPriorite[$etat];
		$o.='</td>';

		$o.='<td>';
		if(ISDEV===1){
			$o.=IMG_EDITNC.' onclick="inlineSwitch(\''.$eid.'\')" />'.$strEtat[$etat]
			.	'<span id="'.$eid.'" style="display:none;">'
			.	'<br><select name="bugEtatEdit'.$bug_id.'">';
					for ($i=0;$i<count($strEtat);$i++){$o.='<option value='.$i.' '.isSelected($etat,$i).'>'.$strEtat[$i].'</option>';}
			$o.= 	'</select></span>';
		}
		else $o.=$strEtat[$etat];
		$o.='</td>';

		$style=($etat==3)?' style="text-decoration:line-through" ':'';
		$o.=
			'<td '.$style.'>'.IMG_EDITNC.' onclick="inlineSwitch(\''.$did.'\')" />'.ln2br($description)
		.	'<span id="'.$did.'" style="display:none;">'
		.		'<textarea style="width:100%; min-height:2em;" name="bugDescEdit'.$bug_id.'" type="text"  value="'.$description.'">'.$description.' </textarea>'
		.	'</span></td>'

		.	'</tr>';
		$out.= $o;
	}

}	// foreach($bugsEtTodo as   $key => $val){

$out.= '</table>';
$out.= '<input name="bugNb" type="hidden" value="'.$bugNb.'" />';
$out.= '</form>';
return $out;
}//function bugsEditShow()





echo todosEditShow();
echo bugsEditShow();


//echo phpinfo();

?>
