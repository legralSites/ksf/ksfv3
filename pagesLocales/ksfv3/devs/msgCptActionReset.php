<?php
/* reset les compteurs de message en phase action
 * eventuellement quand phase ACTION ON
 * script appeller via index.php ?
 */

define('DOCUMENT_ROOT',$_SERVER['DOCUMENT_ROOT'].'/');
define('INDEX_ROOT','../../../');

$lib='gestLib';
include (INDEX_ROOT."lib/legral/php/$lib/$lib.php");

// - gestionnaire de connexion sql - //
$lib='gestPDO';
include(INDEX_ROOT."./lib/legral/php/$lib/$lib.php");
	//$gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);
include(INDEX_ROOT."./config/translates/$lib-erreurs.php");

$lib='configDB';
//include(INDEX_ROOT."config/$lib.php");
include(INDEX_ROOT."configLocal/$lib.php");

$lib='gestTables';
include(INDEX_ROOT."lib/legral/php/gestPDO/$lib.php");
	//$gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);

$dbksfV3=new legralPDO('ksfv3');
//echo $gestLib->libs['ksfv3']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,'$dbksfV3',$dbksfV3);

unset($lib);


// - on recherche si on est en phase action - //


?>
<h2>Réinitialisation de la table de compteur de phase d'action:</h2>
<?php

// - reset de la table msgCptAction - //
// - gestionnaire de connexion des users - //
$dbksfV3->sql->clear();
$dbksfV3->sql->setOPERATION('DELETE FROM `ksfv3_msgCptAction`');	$dbksfV3->query();	$dbksfV3->queryClose();

// - on log l'événement - //
$dbksfV3->sql->clear();
$sql ='INSERT INTO `ksfv3_logs` (`fromLogin`,`toPersoNo`,`domaine`,`actionNo`,`extra`) ';
$sql.="VALUES ('systeme', 0, 'orga', 0, 'la table du compteur de message des actions a été réinitilisé');";
$dbksfV3->sql->setOPERATION($sql); $dbksfV3->query();	$dbksfV3->queryClose();
?>
<h2>Mise à jours de la variable varBD['var_actionRunDate']:</h2>
<?php
$dbksfV3->sql->clear();
$sql='UPDATE `ksfv3_variables` SET `var_actionRunDate`=CURRENT_TIMESTAMP';
$dbksfV3->sql->setOPERATION($sql); $dbksfV3->query();	$dbksfV3->queryClose();
?>





