<h1>Editeur de style</h1>
<!--
<link rel="stylesheet" href="../../lib/codemirror.css">
<link rel="stylesheet" href="../../addon/hint/show-hint.css">
<script src="css.js"></script>
<script src="../../addon/hint/show-hint.js"></script>
<script src="../../addon/hint/css-hint.js"></script>
-->
<link rel="stylesheet" href="lib/tiers/js/codemirror-5.3/lib/codemirror.css">
<link rel="stylesheet" href="lib/tiers/js/codemirror-5.3/addon/hint/show-hint.css">
<script src="lib/tiers/js/codemirror-5.3/mode/css/css.js"></script>
<script src="lib/tiers/js/codemirror-5.3/addon/hint/show-hint.js"></script>
<script src="lib/tiers/js/codemirror-5.3/addon/hint/css-hint.js"></script>


<?php
$cssDir='./styles/templates/';

// - Charger le nom du fichiers selectionné - //
$cssFilename='';
if(isset($_GET['cssFilename']))$cssFilename=$_GET['cssFilename'];
if(isset($_POST['cssFilename']))$cssFilename=$_POST['cssFilename'];
if($cssFilename!=='')$cssFilename=str_replace(['\\','/'],['',''],$cssFilename);// - protection:remplacement des slashes et antislashes - //

$cssFile=$cssDir.$cssFilename;


// - Sauver les modifs du fichier selectionné ./styles - //
if(isset($_POST['sauver'])){
	//chmod("$cssDir$css",0775);
	//echo '<div class="notewarning">Sauvegarde en cours</div>';
	$datas=$_POST['datas'];
	$handle = @fopen("$cssFile", "w");
	if($handle){
		if(fwrite($handle, $datas) !== false){
			echo "<div class='noteclassic'>Modification sauver</div>";
		}
		else{
			echo "<div class='notewarning'>Erreur lors de la sauvegarde (fichier: $cssFile)</div>";
		}
	fclose($handle);
	}
	else {echo "<div class='notewarning'>Erreur: Ouverture du fichier $cssFile impossible</div>";}
}



// - Charger les noms des fichiers du repertoire ./styles - //
$d = dir($cssDir);
//echo "Handle: " . $d->handle . "\n";
echo "Repertoire de styles: " . $d->path . "<br>";
echo '<select onchange="this.options[this.selectedIndex].value && (window.location=\'?cssFilename=\'+this.options[this.selectedIndex].value+\'&amp;ksfv3=devs-stylEdit\');">';
echo "<option>fichiers:</option>";

while (false !== ($entry = $d->read())){
	if($entry[0]=='.')continue;
	$selected=($entry==$cssFilename)?' selected="selected" class="selected"':'';
	echo "<option$selected>$entry</option>";
}
echo '</select>';
$d->close();

if($cssFilename!=''){
	// - chargement du fichier - //
	$datas=file_get_contents($cssFile);

	// - affichage - //
	echo'<form method="POST" action="?'.ARIANE.'">';
	echo'<input type="submit" name="sauver" value="sauver"/>';
	echo'<input type="hidden" name="cssFilename" value="'.$cssFilename.'"/>';
	echo'<textarea id="datas" name="datas" style="min-height:50em">'.$datas.'</textarea>';
	echo'<input type="submit" value="sauver"/>';
	echo'</form>';


?>
<script>

var editor = CodeMirror.fromTextArea(document.getElementById("datas"), {
	extraKeys: {"Ctrl-Space": "autocomplete"}
});
</script>


<?php } //if($css!='')

?>

