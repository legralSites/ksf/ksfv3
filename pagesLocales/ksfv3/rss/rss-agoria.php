<?php
/*
 * charge les messages d'un personnage et les renvoie sous forme rss
 * une clef uuid doit etre fournit, utiliser en tant que mot de passe
 *
 */

require_once(LIB_ROOT.'tiers/php/RSS-Generator/Feed-2.1.1.php');

$feed = new \Zelenin\Feed;

// $feed->addChannel();
//$feed->addChannel('http://example.com/rss.xml');
$feed->addChannel('http://legral.fr/agoria/stable/?rss=agoria');

// required channel elements
$feed
    ->addChannelTitle('Agoria - Actualité du moteur de jeu')
    ->addChannelLink('http://legral.fr/agoria/stable/?rss=agoria')
    ->addChannelDescription('Nouvelles fonctionnalités et modifications du moteur de jeu.');

// optional channel elements
$feed
    ->addChannelLanguage('fr-FR')
    ->addChannelCopyright('Channel copyright, ' . date('Y'))
//    ->addChannelManagingEditor('editor@example.com (John Doe)')
    ->addChannelWebMaster('pascal.toledo@legral.fr (pascal TOLEDO)')
    ->addChannelPubDate(1300000000) // timestamp/strtotime/DateTime
    ->addChannelLastBuildDate(1300000000) // timestamp/strtotime/DateTime
    ->addChannelTtl(60) // minutes
    ;

/*
$feed->addItem();
$feed
    ->addItemTitle('')
    ->addItemDescription('');
*/

$feed->addItem();
$feed
    ->addItemTitle('Fonction Ajouter')
    ->addItemDescription('Le bug de la fonction "Ajouter à" dans la page messagerie est corrigé');


$feed->addItem();
$feed
    ->addItemTitle('Design allégé')
    ->addItemDescription('Le design de certaines pages à été épurée.');

$feed->addItem();
$feed
    ->addItemTitle('Ajout des flux RSS')
    ->addItemDescription('20/11/2015 - L\'actualité du moteur de jeu et les messages du personnage peut desormais être suivi via les flux rss.');


$feed->addItem();
$feed
    ->addItemTitle('Notification par mail')
    ->addItemDescription('19/11/2015 - Les destinataires des messages sont prévenus par mail.');

/*
$feed->addItem();
$feed
    ->addItemTitle('')
    ->addItemDescription('');
 */

//$feed
//    ->addItemPubDate(1300000000) // timestamp/strtotime/DateTime
//    ;

echo $feed;
