<?php
/*
 * charge les messages d'un personnage et les renvoie sous forme rss
 * une clef uuid doit etre fournit, utiliser en tant que mot de passe
 *
 */
global $dbksfV3;
// - recherche de l'UUID du flux rss du - //
$dbksfV3->sql->clear();
$dbksfV3->sql->setOPERATION('SELECT per_id FROM `'.TBLPREFIXE.'personnages` WHERE `per_rssUUID`="'.RSS_UUID.'"');
$sql=$dbksfV3->query();
$dbksfV3->fetch();
define ('PERSONO',$dbksfV3->lignes['per_id']);
$dbksfV3->queryClose();

$where='msg_de_perId='.PERSONO. ' OR msg_pour_perId='.PERSONO;//si pas orga limiter au message inter perso

$messageries=new gestTable('ksfv3',TBLPREFIXE.'messageries','msg_id',
    [
        'SELECT' => 'msg_id'
            .    ',DATE_FORMAT(msg_ts,"%d/%m/%Y %H:%i:%s") AS tsf,DATE_FORMAT(msg_ts,"%j") AS tsj'
            .    ',msg_type'
            .    ',msg_login,msg_de_perId,msg_de_txt,msg_pour_perId'
            .    ',msg_codeD'
            .    ',msg_sujet,msg_message'
            .    ',per_villeId ,CONCAT(per_prenom," ",UCASE(per_nom))  AS de_perPN' // ville, prenom NOM de l'emmetteur
            .    ',dec_id,dec_message'
//            .    "\n".','
    ,    'JOIN'   => 'JOIN '.TBLPREFIXE.'personnages ON msg_de_perId =  per_id'."\n" // pour recup villeId de l'emmetteur
            .    ' LEFT JOIN '.TBLPREFIXE.'decouvertes ON dec_code=msg_codeD'."\n"
    ,    'WHERE'  => $where."\n"
    ,    'ORDERBY'  => 'msg_ts DESC'."\n"
//    ,    'LIMIT'  => 50 
    ,    'clear'  => 0
    ]);
$sql=$messageries->dbTable->sql->getSQL();
//if(ISDEV===1)
//echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.$sql.'</div>';



require_once(LIB_ROOT.'tiers/php/RSS-Generator/Feed-2.1.1.php');

$feed = new \Zelenin\Feed;

// $feed->addChannel();
//$feed->addChannel('http://example.com/rss.xml');
$feed->addChannel('http://legral.fr/agoria/stable/?rss=messagerie&rssUUID='.RSS_UUID);

// required channel elements
$feed
    ->addChannelTitle('Agoria - messagerie')
    ->addChannelLink('http://legral.fr/agoria/stable/?rss=messagerie&rssUUID='.RSS_UUID)
    ->addChannelDescription('Votre boites aux lettres. Cette fonction est encore expérimentale.');

// optional channel elements
$feed
    ->addChannelLanguage('fr-FR')
    ->addChannelCopyright('Channel copyright, ' . date('Y'))
//    ->addChannelManagingEditor('editor@example.com (John Doe)')
    ->addChannelWebMaster('pascal.toledo@legral.fr (pascal TOLEDO)')
    ->addChannelPubDate(1300000000) // timestamp/strtotime/DateTime
    ->addChannelLastBuildDate(1300000000) // timestamp/strtotime/DateTime
//    ->addChannelCategory('Channel category', 'http://example.com/category')
//    ->addChannelCloud('rpc.sys.com', 80, '/RPC2', 'myCloud.rssPleaseNotify', 'xml-rpc')
    ->addChannelTtl(60) // minutes
//    ->addChannelImage('http://example.com/channel.jpg', 'http://example.com', 88, 31, 'Image description')
//    ->addChannelRating('PICS label')
//    ->addChannelTextInput('Title', 'Description', 'Name', 'http://example.com/form.php')
//    ->addChannelSkipHours(array(1, 2, 3))
//    ->addChannelSkipDays(array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'));
    ;
/*
$feed
    ->addChannelElement('test', 'desc', array('attr1' => 'val1', 'attr2' => 'val2'))
    ->addChannelElementWithSub('testsub', array('attr1' => 'val1', 'attr2' => 'val2'))
    ->addChannelElementWithIdentSub('testidentsub', 'child', array('val1', 'val2'));
*/
/*
$feed->addItem();
$feed
    ->addItemTitle('sujet')
    ->addItemDescription('message');

$feed
    ->addItemLink('http://example.com/post1')
    ->addItemAuthor('author@example.com (John Doe)')
    ->addItemCategory('Item category', 'http://example.com/category')
    ->addItemComments('http://example.com/post1/#comments')
    ->addItemEnclosure('http://example.com/mp3.mp3', 99999, 'audio/mpeg')
    ->addItemGuid('http://example.com/post1', true)
    ->addItemPubDate(1300000000) // timestamp/strtotime/DateTime
    ->addItemSource('RSS title', 'http://example.com/rss.xml');
*/



foreach($messageries->get() as $msg){
    $ts        =$msg['tsf'];
    $msg_type   =$msg['msg_type'];
    //$typeCSS    =" msg_type$msg_type";

    //$login      =$msg['msg_login'];
    //$emmeteurId =$msg['msg_de_perId'];
    $emmeteurPN =$msg['de_perPN'];    // $emmeteurPN    =$destinataires->get($emmeteurId,'perPN');
    //$emmeteurTxt=$msg['msg_de_txt'];
    //$destPN     =$destinataires->get($destId,'perPN');

    $codeD      =$msg['msg_codeD'];
    $dec_message=ln2br($msg['dec_message']);

    $sujet      =$msg['msg_sujet'];
    $message    =ln2br($msg['msg_message']);

    $txt ='Message de '.$emmeteurPN.'<br>';
    $txt.='date:'.$ts.'<br>';
    $txt.='Type:'.$msg_type.'<br>';
    $txt.='D&eacute;couverte:'.$codeD.':'.$dec_message.'<br>';
    $txt.=$message.'<br>';

    $feed
        ->addItem()
//    $feed
        ->addItemTitle($sujet)
        ->addItemDescription($txt);
}


echo $feed;
// $feed->save(realpath(__DIR__ . '/rss.xml'));
