<?php
//define('AGORIA_VERSION','sid');
define('AGORIA_VERSION','v0.2.3');

if(AGORIACONFIG!==1)exit();
error_reporting(E_ALL);
//error_reporting(NULL);

define('VERSIONSTATIQUE',0); // attention est valide des que defini (meme si false)
if (defined('VERSIONSTATIQUE')){if (VERSIONSTATIQUE === 1){error_reporting(NULL);/*display_errors(0);*/}}

define('REMOTE_ADDR',$_SERVER['REMOTE_ADDR']);

//  === bufferisation ====  //
ob_start();

//  ==== session ==== //
session_name(SESSION_NAME);session_start();

//  === gestionnaire de librairies ====  //
require (LIB_ROOT."legral/php/gestLib/gestLib.php");


//  ==== configuration du projet ==== //
require("./configLocal/configDB.php");


$gestLib->loadLib('ksfv3',__FILE__,AGORIA_VERSION,"portail v3 du GN");
$gestLib->libs['ksfv3']->setErr(LEGRALERR::DEBUG);
$gestLib->setEtat('ksfv3',LEGRAL_LIBETAT::LOADED);
$gestLib->end('ksfv3');

// - gestionnaire de menus - //
$lib='menuStylisee';
require(LIB_ROOT."legral/php/gestMenus/$lib.php");
//include("./lib/legral/php/gestMenus/$lib-v0.1.php");
//      $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);

$lib='gestMenus';
require(LIB_ROOT."legral/php/$lib/$lib.php");
//      $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);

// - gestionnaire de connexion sql - //
$lib='gestPDO';
require(LIB_ROOT."legral/php/$lib/$lib.php");
    //$gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);
require("./config/translates/$lib-erreurs.php");

$lib='gestTables';
require(LIB_ROOT."legral/php/gestPDO/$lib.php");
    //$gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);

$dbksfV3=new legralPDO('ksfv3');
//echo $gestLib->libs['ksfv3']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,'$dbksfV3',$dbksfV3);

//  ==== rss: envoie un flux RSS ==== //
if (isset($_GET['rss'])){

    switch ($_GET['rss']){
        case 'messagerie':
            define ('RSS_UUID',isset($_GET['rssUUID'])?$_GET['rssUUID']:'');
            if (RSS_UUID !==''){
                require(PAGESLOCALES_ROOT.'ksfv3/rss/rss-messages.php');
                exit();
            }
        case 'agoria':{
                require(PAGESLOCALES_ROOT.'ksfv3/rss/rss-agoria.php');
                exit();
        }
    }

}


// - gestionnaire de connexion des users - //
$lib='gestLogins';
require(LIB_ROOT."legral/php/$lib/$lib.php");
//    $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);
//$lib='configLogins';
//include("./config/$lib.php");./lib/legral/php/gestMenus/pagesLocales/gestMenus.php
//include("./configLocal/$lib.php");


unset($lib);

//  ==== psp-query ==== //
$psp=parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
//$varGET = parse_query($_SERVER['REQUEST_URI']);//(parse_query cf gestMenus)


//  ==== gestion des joueurs ==== //
//  on charge tous les joueurs!  //
$joueurs=new gestTable('ksfv3',TBLPREFIXE.'joueurs','jou_id',['SELECT' => 'jou_id,jou_login,jou_pwd','clear'=>0]);
//if(ISDEV===1)echo $joueurs->tableau();

$gestLogins=new gestLogins();

foreach( $joueurs->get() as $jou_id =>$joueur){
    $login=$joueur['jou_login'];
    $pwd=$joueur['jou_pwd'];
    $gestLogins->addUser($login,$pwd);
    $gestLogins->setAttr($login,'jou_id', $joueur['jou_id']);
}

//  ==== gestion du joueur ==== //
//include('./pagesLocales/ksfv3/connexion.php');
if (isset($_POST['submit'])){
    $login=(isset($_POST['login'])) ? $_POST['login'] : '';
    $pwd = (isset($_POST['pass']))  ? $_POST['pass']  : '';
    define ('LOGINOK',$gestLogins->connect($login,$pwd));
}

$gestLogins->reconnect();


// ==================================== //
// - recherche des id joueur et perso - //
// =================================== //
define('ISCONNECT',$gestLogins->isConnect());
define('LOGIN',$gestLogins->whoIsConnected());

// -- recuperation du no du joueur loggué -- //
define ('JOUNO',$gestLogins->getAttr(LOGIN,'jou_id'));//! string!!
//echo 'JOUNO'.JOUNO.'<br>';

// - chargement du joueur actif - //
// -- toutes les infos du joueurs sauf login et pwd (qui est gérer par gestLogins) -- //
$joueur=new gestLigne($dbksfV3,TBLPREFIXE.'joueurs',JOUNO,'jou_id',
    [    'SELECT'=>'jou_ts,jou_id,jou_nom,jou_villeId,jou_persoNo,jou_isFONDATEUR,jou_isADMIN,jou_isMJ,jou_isJOUEUR,jou_isDEV','clear'=>0]
);
//$gestLogins->setAttr(LOGIN,'villeId', $joueur->get('jou_villeId'));
//echo $gestLogins->tableau();

$persoNo=$joueur->get('jou_persoNo');

define('JOUID',   $joueur->get('jou_id'));
define('JOUNOM',  $joueur->get('jou_nom'));
define('VILLEID', $joueur->get('jou_villeId'));    // villeId du joueur (MJ) et non d'un personnage

define('ISFONDATEUR', $joueur->get('jou_isFONDATEUR')==1?1:0);
define('ISADMIN', $joueur->get('jou_isADMIN')==1?1:0);
define('ISMJ',    $joueur->get('jou_isMJ')==1?1:0);
define('ISORGA',  ISMJ);
define('ISJOUEUR',$joueur->get('jou_isJOUEUR')==1?1:0);
define('ISDEV',   $joueur->get('jou_isDEV')==1?1:0);
//if(ISDEV===1)echo $joueur->tableau();
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($joueurs->dbTable->sql->getSQL()).'</div>';
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">/* joueur */'.ln2br($joueur->legralPDO->sql->getSQL()).'</div>';
unset($joueurs,$joueur);


// -  Gestion des chemins - //
define ('SCRIPTPATH',ISDEV===1?AGORIA_VERSION_ROOT.'locales/scripts.js':AGORIA_VERSION_ROOT.'locales/scripts.min.js');
//define ('STYLESPATH',ISDEV===1?AGORIA_VERSION_ROOT.'styles/styles.css':AGORIA_VERSION_ROOT.'styles/styles.min.css');
define ('STYLESPATH',ISDEV===1?AGORIA_VERSION_ROOT.'styles/styles.css':AGORIA_VERSION_ROOT.'styles/styles.css');

// - constante globale - //
define('JOURNAL_SHOW',0);    // affiche ou non les journaux (joueur/perso)


// - gestion du/des personnage(s) - //
define ('PER_ETAT_INACTIF',0);
define ('PER_ETAT_RUN',1);
define ('PER_ETAT_PAUSE',2);
define ('PER_ETAT_MORT',3);

$per_etatTxt[PER_ETAT_INACTIF]='Inactif';
$per_etatTxt[PER_ETAT_RUN]='En jeu';
$per_etatTxt[PER_ETAT_PAUSE]='En pause';
$per_etatTxt[PER_ETAT_MORT]='Mort';

// - PHP 5.6 - //
//const DEFAULT_ROLES = array('guy', 'development team');
//class PER_ETAT{
//    public $txt=}


// ---  surcharge de persoNO  from la feuile orgas-perso valide seulement pour les orgas --- //
if( ISMJ === 1 AND isset($_GET['orgaPersoNo']) ){
    $persoNo=$_GET['orgaPersoNo'];    // selection du persoNo from la feuile orgas-perso
    $_SESSION['orgaPersoNo']=$persoNo;
}
if( isset($_SESSION['orgaPersoNo']))    $persoNo=$_SESSION['orgaPersoNo'];
if($persoNo!==NULL)define ('PERSONO',$persoNo);
unset($persoNo);

global $perso,$persos;
$perso=-1;
$rssUUID=NULL;
if(ISCONNECT===1){
    // - mise ajours du TS du joueur - //
    $dbksfV3->sql->clear();
    $dbksfV3->sql->setOPERATION('UPDATE `'.TBLPREFIXE.'joueurs` SET `jou_ts`=CURRENT_TIMESTAMP WHERE `jou_id`='.JOUNO);
    $sql=$dbksfV3->query();    $dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

	//  ==== Chargement des fichiers-functions ==== //
	include(PAGESLOCALES_ROOT.'ksfv3/functions-communes.php');
	if(ISORGA===1)include(PAGESLOCALES_ROOT.'ksfv3/functions-orgas.php');
	include(PAGESLOCALES_ROOT.'ksfv3/functions-reload.php');
	if(defined('PERSONO'))include(PAGESLOCALES_ROOT.'ksfv3/functions-personnage.php');// doit etre apres functions-reload (cause PHASE_A8R_NB) //
}//if(ISCONNECT===1)




//  ==== Gestion des menus et pages  ==== //
// - menu JOUEUR - //
global $gestMenus;
$gestMenus=new gestMenus('ksfv3');
$gestMenus->pageContentShow=0;// desactivation de l'affichage automatique de pageContent
$gestMenus->metasPrefixes['title']='Agoria -';
include(MENU_ROOT.'menus-systeme.php');
require(MENU_ROOT.'ksfv3/menus-ksfv3.php');

// - menu ORGAS - //
if(ISMJ===1){
    global $menuOrgas;
    $menuOrgas=new gestMenus('orgas');
    $menuOrgas->metasPrefixes['title']='Agoria - Organisation';
    require(MENU_ROOT.'ksfv3/menus-orgas.php');
}

// - menu ADMIN - //
if(ISADMIN===1){
    $menuAdmins=new gestMenus('admins');
    $menuAdmins->metasPrefixes['title']='Agoria - Administration';
    include(MENU_ROOT.'ksfv3/menus-admins.php');
}

// - menu DEV - //
if(ISDEV===1){
    $menuDevs=new gestMenus('devs');
    $menuDevs->metasPrefixes['title']='Agoria - Develloppement';
    include(MENU_ROOT.'ksfv3/menus-devs.php');
}


//  ==== html ==== //
?><!DOCTYPE html><html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="robots" content="index,follow">
<meta name="author" content="Pascal TOLEDO">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="generator" content="vim">
<meta name="identifier-url" content="http://legral.fr">
<meta name="date-creation-yyyymmdd" content="20140804">
<meta name="date-update-yyyymmdd"   content="2015115">
<meta name="reply-to" content="pascal.toledo@legral.fr">
<meta name="revisit-after" content="10 days">
<meta name="category" content="">
<meta name="publisher" content="legral.fr">
<meta name="copyright" content="pascal TOLEDO">

<?php
echo '<link rel="alternate" type="application/rss+xml"  href="?rss=agoria" title="agoria">';
if (defined('PERSONO'))echo '<link rel="alternate" type="application/rss+xml"  href="?rss=messagerie&amp;rssUUID='.$rssUUID.'" title="messagerie">';
 
//if(ISDEV)echo'<meta http-equiv="refresh" content="10">';

// - construction du/des menu(s) - //
$gestMenus->build();
define ('ARIANE_AGORIA',$gestMenus->arianeFULL);

if(ISMJ===1){
    $menuOrgas->build();
    define ('ARIANE_ORGA',$menuOrgas->arianeFULL);
}
?>
<!-- scripts -->
<script src="<?php echo SCRIPTPATH?>"> </script>

<!-- styles -->
<link rel="stylesheet" href="<?php echo STYLESPATH?>" media="all" />
<!--link rel="stylesheet" href="./styles/styles.css" media="all" /-->
<!--link rel="stylesheet" href="./styles/knacss/knacss.css" media="all" /-->
<!--link rel="stylesheet" href="./styles/ksfv3/ksfv3-styles.css" media="all" /-->
</head>

<body>
<div id="page">
<?php
// indiquer dans un cadre fixe le role actif si le joueur est un MJ
if(ISMJ===1){
    echo '<div id="infoBoxRole">';
    if(PERSONO>0) echo PER_PRENOM.' '.PER_NOM;
        //echo $perso->attrs->attrs['per_prenom'].' '.$perso->attrs->attrs['per_nom'];
    echo '</div>';
}
?>
<!-- header -->
<div id="header">

<div id="headerGauche"><!--gauche--></div>

<div id="logo"><img src="<?php echo LOGOAGORIA_SRC;?>" /></div>
<!--h1><a href="?<?php echo $gestMenus->menuDefaut?>">KSF v3</a></h1-->

<!--div id="headerDroit"><a href="?about=accueil">&agrave; propos de...</a></div-->
</div><!-- header -->

<!-- menu + ariane + contenu page + ariane -->
<?php
//echo '<div class="ariane print">'.$gestMenus->ariane->showAriane().'</div>'."\n";

if($dbksfV3->isConnect()<=0){
    echo '<div class="noteimportant">';
    echo 'Connexion impossible. '.$gestLib->erreurs['gestPDO']->getTexte().'<br>';

    $eTxt=$gestLib->erreurs['gestPDO']->getTexte('e');
    if($eTxt!=''){
        echo "exeption:$eTxt<br>";
    }
    echo '</div>';
    unset ($dbksfv3);
}

// - selection du personnage actif pour les ORGAS - // 
if(ISMJ===1){
   
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($persos->dbTable->sql->getSQL()).'</div>';

    $s='<select id="persoSelectNo" name="persoNo" onchange="this.options[this.selectedIndex].value && (window.location=\'?orgaPersoNo=\'+this.options[this.selectedIndex].value+\'&amp;ksfv3=perso-fiche\')">';
    $s.='<option disabled="disabled">personnages:</option>';
        foreach($persos->get() as $_perso_id => $_perso){
            $selected=($_perso_id==PERSONO)?' selected="selected" class="selected"':'';
            $s.='<option value="'.$_perso_id.'" title="'.$_perso['perPN'].'"'.$selected.'> '.$_perso['perPN'].'</option>'."\n";
        }
    $s.='</select>';

    echo "<div class='txtcenter'>Tous les personnages: $s</div><br>";

    unset($s);
}//if(ISMJ===1)

// - affiche le nom du perso actif - //
if(ISJOUEUR===1)echo '<div id="persoTitre">'.PER_PRENOM.' '.PER_NOM.'</div>';

// - affiche les menus contruient en commencant par le menu indiqué par le constructeur. Inclu la page appellee - //
// - affichage des menus - //

$gestMenus->showPage=0; //desactivation de l'affichage automatique 
if($psp===NULL){
//    $gestMenus->showPage=1;//aucun menu appelle afficher la page par defaut
}
echo $gestMenus->show();
if(ISJOUEUR===10){
    echo'<div class="txtcenter"><br>experimental:<audio controls preload="auto" loop autoplay src="http://mediatheques.legral.fr:8100" type="audio/ogg">';
    //echo'  <source src="http://mediatheques.legral.fr:8100" type="audio/ogg">';
    //echo'  <!--source src="http://mediatheques.legral.fr:8100/mp3" type="audio/mpeg"-->';
    //echo'Your browser does not support the audio element.';
    echo'</audio>';
    if(ISMJ===1)echo ' <a target="mpd" href="http://mediatheques.legral.fr/_/relaxx/">playlist</a>';
    echo '</div>';
}
if(ISMJ===1){
    $menuOrgas->showPage=0; //desactivation de l'affichage automatique 
    echo ''.$menuOrgas->show();
}

// - affichage des pages selectionnée des menus - //

// -- menu: agoria -- //
echo $gestMenus->showPageContent(1);

// -- menu: orgas -- //
if(ISMJ===1)echo $menuOrgas->showPageContent(1);

// -- ariane: affichage-- //
//echo "\n".'<div class="ariane">'.$gestMenus->ariane->showAriane().'</div>'."\n";
echo'<!-- menu + ariane + contenu page + ariane : FIN -->';

echo'<!-- section: dev -->';
if(ISDEV===1)include PAGESLOCALES_ROOT.'ksfv3/orgas/dev.php';
include PAGESLOCALES_ROOT.'footer.php';
?>
<!-- footer -->
</div><!-- //page -->
</body></html>
