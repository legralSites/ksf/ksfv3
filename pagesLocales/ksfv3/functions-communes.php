<?php
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_File"></div>';

// -- activation/desactivation des fonctions avancés -- //
if(isset($_GET['fonctionAvance']))$_SESSION['fonctionAvance'] = $_GET['fonctionAvance'];
define('FONCTIONAVANCE',(isset($_SESSION['fonctionAvance']))?$_SESSION['fonctionAvance']:0);


// ====================== //
// = fonctions communes = //
// ====================== //

// $per_etat: selection les perso dans l'etat spécifié (si -1 selectionne tous les persos quelques sois leurs états)
// $selectAll: selection tous les champs de la table (*)
function personnagesLoad($per_etat=PER_ETAT_RUN,$selectAll=0){
    // afficher les MJ et les perso de la ville du MJ, OU, QUE les personnages    return new gestTable("ksfv3",TBLPREFIXE.'personnages','per_id',
    $PN=',CONCAT (UCASE(per_nom)," ",per_prenom) AS perPN';
    $showJS=(ISMJ===1)?' OR per_id<1':' AND per_id>0';
    if($selectAll===1){
        $s='*'.$PN;
    }
    else{
        $s='per_id,per_etat,per_villeId';
        $s.=',per_isMes,per_isAct,per_isRec';
        //$s.=',per_Ech,per_Dec';
        $s.=$PN;
    }    

    $where=($per_etat===-1)?'per_villeId = '. PER_VILLEID.$showJS:'per_etat = '.$per_etat.' AND per_villeId = '. PER_VILLEID.$showJS;
    return new gestTable("ksfv3",TBLPREFIXE.'personnages','per_id',
        [ 'SELECT'  => $s
         ,'WHERE'   => $where
         ,'ORDERBY' => 'per_nom ASC,per_nom ASC'
         ,'clear'   => 0
     ]);
}


function logAdd($_persNo,$_domaine,$_actionNo,$_extra=''){
    global $dbksfV3;
    // (a faire) ajouter JOUID en plus de fromLogin
    $sql ='INSERT INTO `'.TBLPREFIXE.'logs` (`fromLogin`,`toPersoNo`,`domaine`,`actionNo`,`extra`) '."\n";
    $extra=addslashes($_extra);
    $sql.="VALUES ('".LOGIN."', $_persNo, '$_domaine', $_actionNo,'$extra');";
    $dbksfV3->sql->clear();$dbksfV3->sql->setOPERATION($sql);    $sql=$dbksfV3->query();    $dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
}

function getJoueurNom($jouId=JOUNO){
    global $dbksfV3;
    $dbksfV3->sql->setOPERATION("SELECT jou_nom FROM ".TBLPREFIXE."joueurs WHERE jou_id=$jouId;");$sql=$dbksfV3->query();$r=$dbksfV3->fetch();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    return $r['jou_nom'];
}

function getPersoNom($perId=PERSONO){
    global $dbksfV3;
    $dbksfV3->sql->setOPERATION('SELECT per_nom FROM '.TBLPREFIXE.'personnages WHERE per_id='.$perId.';');$sql=$dbksfV3->query();$r=$dbksfV3->fetch();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    return $r['per_nom'];
}

function getPersoNP($perId=PERSONO){
    global $dbksfV3;
    $dbksfV3->sql->setOPERATION('SELECT CONCAT(UCASE(per_nom)," ",per_prenom) AS perNP FROM '.TBLPREFIXE.'personnages WHERE per_id='.$perId.';');$sql=$dbksfV3->query();$r=$dbksfV3->fetch();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    return $r['perNP'];
}

function getPersoPN($perId=PERSONO){
    global $dbksfV3;
    $dbksfV3->sql->setOPERATION('SELECT CONCAT(per_prenom," ",UCASE(per_nom)) AS perPN FROM '.TBLPREFIXE.'personnages WHERE per_id='.$perId.';');$sql=$dbksfV3->query();$r=$dbksfV3->fetch();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    return $r['perPN'];
}

function getPersoMail($perId=PERSONO){
    global $dbksfV3;
    $dbksfV3->sql->setOPERATION('SELECT jou_mail FROM '.TBLPREFIXE.'joueurs WHERE jou_persoNo='.$perId.';');$sql=$dbksfV3->query();$r=$dbksfV3->fetch();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    return $r['jou_mail'];
}

function getPersoRssUUID($perId=PERSONO){
    global $dbksfV3;
    $dbksfV3->sql->setOPERATION('SELECT per_rssUUID FROM '.TBLPREFIXE.'personnages WHERE per_id='.$perId.';');$sql=$dbksfV3->query();$r=$dbksfV3->fetch();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    return $r['per_rssUUID'];
}

function getVilleNom($villeId){
    global $dbksfV3;
    $dbksfV3->sql->setOPERATION('SELECT vil_nom FROM '.TBLPREFIXE.'villes WHERE vil_id='.$villeId);$sql=$dbksfV3->query();$r=$dbksfV3->fetch();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    return $r['vil_nom'];
}

function getTalentNom($talId){
    global $dbksfV3;
    $dbksfV3->sql->setOPERATION('SELECT tal_nom FROM '.TBLPREFIXE.'talents WHERE tal_id='.$talId);$sql=$dbksfV3->query();$r=$dbksfV3->fetch();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    return $r['tal_nom'];
}

function getReseauNom($resId){
    global $dbksfV3;
    $dbksfV3->sql->setOPERATION('SELECT res_nom FROM '.TBLPREFIXE.'reseaux WHERE res_id='.$resId.';');$sql=$dbksfV3->query();$r=$dbksfV3->fetch();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    return $r['res_nom'];
}
// non utiliser
//function getCodeDMessage($dec_code){
//	global $dbksfV3;
//	$dbksfV3->sql->setOPERATION('SELECT dec_message FROM '.TBLPREFIXE.'decouvertes WHERE dec_code="'.$dec_code.'";');$sql=$dbksfV3->query();$r=$dbksfV3->fetch();$dbksfV3->queryClose();
//	if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
//	return $r['dec_message'];
//}



// ================= //
// = variablesInDB = //
// ================= //
// ca sert encore ca?
$varDB=new gestTable('ksfv3',TBLPREFIXE.'planning','pla_id',['SELECT' => 'pla_id'
	,'WHERE'=> '`pla_type` = 4 AND `pla_debut` <= NOW() AND   NOW() <= `pla_fin`'
]);

// ================= //
// = Edition du joueur = //
// ================= //
// - edition des donnes d'un personnage (login/nom/mail) - //
if(isset($_POST['jouDataEdit'])){
    //echo 'Sauvegarde des datas du joueur<br>';
    //$nom=addslashes($_POST['jouNomData']);
    $login=addslashes($_POST['jouLoginData']);
    $mail=addslashes($_POST['jouMailData']);

    //$sqlOp='UPDATE '.TBLPREFIXE."joueurs Set `jou_nom`= '$nom', jou_login= '$login', jou_mail='$mail' WHERE `jou_id`=".JOUID.';';
    $sqlOp='UPDATE '.TBLPREFIXE."joueurs Set jou_login= '$login', jou_mail='$mail' WHERE `jou_id`=".JOUID.';';
    $dbksfV3->sql->setOPERATION($sqlOp);$sql=$dbksfV3->query();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    logAdd(PERSONO,  'joueur',10,"login: $login, mail:$mail");
    unset($sqlOp,$note);
}

// - edition du pasw d'un personnage - //
if(isset($_POST['jouPaswEdit'])){
    //echo 'Sauvegarde du pasw du joueur<br>';
    $pwd=addslashes($_POST['jouPaswData']);
    $sqlOp='UPDATE '.TBLPREFIXE.'joueurs Set `jou_pwd`= "'.$pwd.'" WHERE`jou_id`='.JOUID.';';
    $dbksfV3->sql->setOPERATION($sqlOp);$sql=$dbksfV3->query();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">/* code sql de changement de mot de passe */</div>';
    logAdd(PERSONO,  'joueur',11,NULL);
    unset($sqlOp,$note);
}


// - edition de la note d'un personnage - //
if(isset($_POST['jouNoteEdit'])){
    //echo 'Sauvegarde de la note du joueur<br>';
    $note=addslashes($_POST['jouNoteData']);
    $sqlOp='UPDATE '.TBLPREFIXE.'joueurs Set `jou_note`= "'.$note.'" WHERE `jou_id`='.JOUID.';';
    $dbksfV3->sql->setOPERATION($sqlOp);$sql=$dbksfV3->query();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    //logAdd(PERSONO,  'joueur',220,$note);//Ne pas logguer cela est privé
    unset($sqlOp,$note);
}


// --------------------------------------------- //
// - met a jours les meta donnees d'un message - //
// --------------------------------------------- //
function msgSetMetaDonnees($msgId=NULL){
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_file">msgSetMetaDonnees</div>';
    global $dbksfV3;
//    if($msgId===NULL)$msgid=$_POST['msgNoEdit']; // modification d'un message via orgas-messagerie

    $msg_type=NULL;
    if(isset($_GET['msg_type']))$msg_type=$dbksfV3->sql->formatChamp($_GET['msg_type'],0,1);
    if(isset($_POST['msg_type']))$msg_type=$dbksfV3->sql->formatChamp($_POST['msg_type'],0,1);

    $sqlmsg_codeD='';
    if(ISMJ===1){
        if(isset($_GET['msg_codeD']))$msg_codeD=$_GET['msg_codeD'];
        if(isset($_POST['msg_codeD']))$msg_codeD=$_POST['msg_codeD'];
        $sqlmsg_codeD=', msg_codeD='.$dbksfV3->sql->formatChamp($msg_codeD);
    }

    $sql='UPDATE '.TBLPREFIXE."messageries SET msg_ts=msg_ts, msg_type=$msg_type$sqlmsg_codeD WHERE msg_id=$msgId";

    $dbksfV3->sql->clear();$dbksfV3->sql->setOPERATION($sql);$sql=$dbksfV3->query();$dbksfV3->queryClose();
    if(ISDEV===1) echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    if(ISMJ===1) logAdd(0,  'orga'    ,0,'modif du message no:'.$msgId.', type:'.$msg_type.', msg_codeD:'.$msg_codeD);
}


// =============================== //
// - chargement des données      - //
// -  et mise en var d'une table - //
// =============================== //

// $tblNom: nom de la table a charger
// $var: variable qui va recevoir les donnees
// $indexNom: nom du champ d e l'id
// $sqlClear: nettoie l'objet $dbksfV3->sql avec  $dbksfV3->sql->clear();
// $sqlAddon: utiliser $dbksfV3->sql avec  $sqlClear=0

function tableVarLoad($tblNom,&$vars,$indexNom,$sqlClear=1){
    global $gestLib,$dbksfV3,$debugContent;

    if($sqlClear===1)$dbksfV3->sql->clear();

    $dbksfV3->sql->setFROM($tblNom);
    $dbksfV3->sql->setOPERATION('SELECT * ');
    $sql=$dbksfV3->query();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    
    // --  mise en vars --- //
    $vars=array();
    while ($dbksfV3->fetch()){
        $id=$dbksfV3->lignes[$indexNom];
        $vars[$id]=array();
        foreach($dbksfV3->lignes as   $key => $val)$vars[$id][$key]=$val;
    }
    $dbksfV3->queryClose();
}

// --  tableVarShow: affichage(pour test) -- //
function tableVarShow($vars,$varNom){
    global $gestLib;
    $o='';
    $o.='<br><b>'.$varNom.': affichage</b><br>';
    $o.= $gestLib->libs['ksfv3']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,$vars,$vars);

    foreach($vars as   $key => $var){
        $o.=$gestLib->libs['ksfv3']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,'key',$key);
         $o.=$gestLib->libs['ksfv3']->debugShowVar(LEGRALERR::DEBUG,__LINE__,__METHOD__,'val',$var);
    }
    return $o.'<br>';
}


//tableVarLoad($tblNom,&$vars,$idNom);

// -- perTals : les talents d'UN personnage -- //
function perTalsLoad($perId){
    global $gestLib,$dbksfV3;
    if(!is_numeric($perId))return array();
    $dbksfV3->sql->setFROM(TBLPREFIXE.'perTalActif');
    $dbksfV3->sql->setWHERE("`perId`=$perId");
    $dbksfV3->sql->setJOIN('JOIN '.TBLPREFIXE.'talents ON talId = tal_id');
    $dbksfV3->sql->setOPERATION('SELECT * ');
    $sql=$dbksfV3->query();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    // ---  persoTalents : mise en var --- //
    $persoTalents=array();    // les id des talents d'UN perso
    while ($dbksfV3->fetch()){
        $talId=$dbksfV3->lignes['talId'];
        $persoTalents[$talId]=array();
        $persoTalents[$talId]['status']=$dbksfV3->lignes['status'];
        $persoTalents[$talId]['fromPerId']=$dbksfV3->lignes['fromPerId'];
        $persoTalents[$talId]['nom']=$dbksfV3->lignes['tal_nom'];
        $persoTalents[$talId]['description']=$dbksfV3->lignes['tal_description'];
    }
    $dbksfV3->queryClose();
    return $persoTalents;
}


// -- perTals : les reseaux d'UN personnage -- //
function perResLoad($perId){
    global $gestLib,$dbksfV3;
    if(!is_numeric($perId))return array();
    $dbksfV3->sql->setFROM(TBLPREFIXE.'perResActif');
    $dbksfV3->sql->setWHERE("`perId`=$perId");
    $dbksfV3->sql->setJOIN('JOIN '.TBLPREFIXE.'reseaux ON resId = res_id');
    $dbksfV3->sql->setOPERATION('SELECT * ');
    $sql=$dbksfV3->query();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

    // ---  persoTalents : mise en var --- //
    $persoReseaux=array();    // les id des talents d'UN perso
    while ($dbksfV3->fetch()){
        $resId=$dbksfV3->lignes['resId'];
        $persoReseaux[$resId]=array();
        $persoReseaux[$resId]['status']=$dbksfV3->lignes['status'];
        $persoReseaux[$resId]['fromPerId']=$dbksfV3->lignes['fromPerId'];
        $persoReseaux[$resId]['nom']=$dbksfV3->lignes['res_nom'];
        $persoReseaux[$resId]['description']=$dbksfV3->lignes['res_description'];
    }
    $dbksfV3->queryClose();
    return $persoReseaux;
}



// -- perPous : les informations des pouvoirs d'UN personnage -- //
function perPouLoad($perId){
    global $gestLib,$dbksfV3;
    if(!is_numeric($perId))return array();
    $dbksfV3->sql->setFROM(TBLPREFIXE.'perPouActif');
    $dbksfV3->sql->setWHERE("`perId`=$perId");
    $dbksfV3->sql->setJOIN('JOIN '.TBLPREFIXE.'pouvoirs ON pouId = pou_id');
    $dbksfV3->sql->setOPERATION('SELECT * ');
    $sql=$dbksfV3->query();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

    // ---  persoPouvoirs : mise en var --- //
    $persoPouvoirs=array();    // les id (et les infos sur les id) des pouvoirs d'UN perso
    while ($dbksfV3->fetch()){
        $pouId=$dbksfV3->lignes['pouId'];
        $persoPouvoirs[$pouId]=array();
        $persoPouvoirs[$pouId]['status']=$dbksfV3->lignes['status'];
        $persoPouvoirs[$pouId]['fromPerId']=$dbksfV3->lignes['fromPerId'];
        $persoPouvoirs[$pouId]['score']=$dbksfV3->lignes['score'];
        $persoPouvoirs[$pouId]['nomR']=$dbksfV3->lignes['pou_nom'];
        $persoPouvoirs[$pouId]['nomP']=$dbksfV3->lignes['nomP'];
        $persoPouvoirs[$pouId]['note']=$dbksfV3->lignes['note'];
        $persoPouvoirs[$pouId]['description']=$dbksfV3->lignes['pou_description'];
    }
    $dbksfV3->queryClose();
    return $persoPouvoirs;
}


// -- perDec : les decouverte acquis  d'UN personnage -- //
function perDecLoad($perId){
    global $dbksfV3;
    if(!is_numeric($perId))return array();
    $dbksfV3->sql->setFROM(TBLPREFIXE.'perDec');
    $dbksfV3->sql->setWHERE("`perId`=$perId");
    $dbksfV3->sql->setOPERATION('SELECT * ');
    $sql=$dbksfV3->query();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

    // ---  persoPouvoirs : mise en var --- //
    $perDec=array();    // les id (et les infos sur les id) des pouvoirs d'UN perso
    while ($dbksfV3->fetch()){
        $decId=$dbksfV3->lignes['decId'];
        $perDec[$decId]=$dbksfV3->lignes['decId'];
    }
    $dbksfV3->queryClose();
    return $perDec;
}
