<?php
global $gestLogins;
$itemsDescr= new gestTable('ksfv3',TBLPREFIXE.'itemsDescriptions','nom');
?>
<div id="accueil">
<h1 class="h1">Agoria: le jeu de r&ocirc;le Grandeur Nature</h1>
<?php echo '<div class="txtcenter">'.convertAccent(ln2br($itemsDescr->get('leJeu','diJ'))).'</div>';?>

<h1 class="h1">Les r&egrave;gles du Jeu</h1>
<p class="txtcenter">version <a href="<?php echo DOC_ROOT?>AGORIA/Regles-1.pdf">pdf</a>.</p>
</div>
<br>
<?php
echo '<div id="versionTag" class="txtcenter">';
echo 'Version du moteur de jeu:'.AGORIA_VERSION.'<br>';
echo 'Version du de l\'instance du jeu:'.AGORIA_INSTANCE_VERSION.'<br>';
echo '</div>';
