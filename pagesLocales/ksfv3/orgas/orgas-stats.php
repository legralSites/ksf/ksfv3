<h1 class="">LES STATS</h1>
<script src="../../agoria-engine/sid/lib/tiers/js/canvasjs/canvasjs-1.7.0.min.js"></script>


<script>
function showCountCar(TEXTAREAid,SPANidHTML){
    document.getElementById(SPANidHTML).innerHTML=TEXTAREAid.value.length;
}
</script>
<?php
global $dbksfV3;

// - Ajout (ou non) des MJ dans les stats  - //
$addMJ='';    // laisse le MJ de la ville
//$addMD=' OR per_id <1';    // ajoute tous les MJ (des autres villes)
$addMJ=' AND per_id >0';    // exclus tous les MJ

// - Ajout (ou non) des BetaTesteur (major DOME) dans les stats  - //
$addBT='';
$addBT=' AND per_prenom != "Major"';    // exclus tous les BT

// ========================= //
// - stat sur les messages - //
// ========================= //
// -- nb de message total -- //
$dbksfV3->sql->setOPERATION(
    'SELECT COUNT(*) AS TOTAL'
.    ' FROM '.TBLPREFIXE.'messageries'
.    ' JOIN '.TBLPREFIXE.'personnages ON per_id=msg_de_perId'
.    ' WHERE per_villeId='.PER_VILLEID.$addMJ.$addBT
);
$sql=$dbksfV3->query();
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

$msgTotal=0;
while ($dbksfV3->fetch())$msgTotal=$dbksfV3->lignes['TOTAL'];


// -- recherche du nombre de message par personnages -- //
$dbksfV3->sql->setOPERATION(
    'SELECT `msg_de_perId`, COUNT(*) AS NB'
    . ', CONCAT (per_prenom," ",UCASE(per_nom))AS perNP'
.    ' FROM '.TBLPREFIXE.'messageries'
.    ' JOIN '.TBLPREFIXE.'personnages ON per_id=msg_de_perId'
.    ' WHERE per_villeId='.PER_VILLEID.$addMJ.$addBT
.    ' GROUP BY `msg_de_perId`'
.    ' ORDER BY NB DESC'
);
$sql=$dbksfV3->query();
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

$msgNb=array();
while ($dbksfV3->fetch()){
    $deId=$dbksfV3->lignes['msg_de_perId'];
    $msgNb[$deId]=array();
    $msgNb[$deId]['perNP']=$dbksfV3->lignes['perNP'];
    $msgNb[$deId]['NB']=$dbksfV3->lignes['NB'];
}
$dbksfV3->queryClose();


// ====================== //
// - stat sur les carac - //
// ====================== //

// -- le min/avg/max et qut de chaque carac -- //
$dbksfV3->sql->setOPERATION(
    'SELECT '

    . ' MIN(per_richesseDispo) AS richesseDispoMIN,AVG(per_richesseDispo) AS richesseDispoAVG,MAX(per_richesseDispo) AS richesseDispoMAX,SUM(per_richesseDispo) AS richesseDispoSUM'
    . ',MIN(per_richesseMax) AS richesseMaxMIN,AVG(per_richesseMax) AS richesseMaxAVG,MAX(per_richesseMax) AS richesseMaxMAX,SUM(per_richesseMax) AS richesseMaxSUM'

    . ',MIN(per_procheDispo) AS procheDispoMIN,AVG(per_procheDispo) AS procheDispoAVG,MAX(per_procheDispo) AS procheDispoMAX,SUM(per_procheDispo) AS procheDispoSUM'
    . ',MIN(per_procheMax) AS procheMaxMIN,AVG(per_procheMax) AS procheMaxAVG,MAX(per_procheMax) AS procheMaxMAX,SUM(per_procheMax) AS procheMaxSUM'
    
.    ' FROM '.TBLPREFIXE.'personnages'
.    ' WHERE per_villeId='.PER_VILLEID.$addMJ.$addBT
);
$sql=$dbksfV3->query();
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.$sql.'</div>';


while ($dbksfV3->fetch()){
    $richesseDispoMIN=$dbksfV3->lignes['richesseDispoMIN'];$richesseDispoAVG=$dbksfV3->lignes['richesseDispoAVG'];$richesseDispoMAX=$dbksfV3->lignes['richesseDispoMAX'];$richesseDispoSUM=$dbksfV3->lignes['richesseDispoSUM'];
    $richesseMaxMIN=$dbksfV3->lignes['richesseMaxMIN'];$richesseMaxAVG=$dbksfV3->lignes['richesseMaxAVG'];$richesseMaxMAX=$dbksfV3->lignes['richesseMaxMAX'];$richesseMaxSUM=$dbksfV3->lignes['richesseMaxSUM'];

    $procheDispoMIN=$dbksfV3->lignes['procheDispoMIN'];$procheDispoAVG=$dbksfV3->lignes['procheDispoAVG'];$procheDispoMAX=$dbksfV3->lignes['procheDispoMAX'];$procheDispoSUM=$dbksfV3->lignes['procheDispoSUM'];
    $procheMaxMIN=$dbksfV3->lignes['procheMaxMIN'];$procheMaxAVG=$dbksfV3->lignes['procheMaxAVG'];$procheMaxMAX=$dbksfV3->lignes['procheMaxMAX'];$procheMaxSUM=$dbksfV3->lignes['procheMaxSUM'];
}


// -- liste des persos triées par ordre decroissant de la carac -- //
//
// --- per_richesseDispo --- //
$dbksfV3->sql->setOPERATION(
    'SELECT per_id'
    . ',CONCAT (per_prenom," ",UCASE(per_nom))AS perNP'
    . ',per_richesseDispo AS dispo'
.    ' FROM '.TBLPREFIXE.'personnages'
.    ' WHERE per_villeId='.PER_VILLEID.$addMJ.$addBT
//.    ' GROUP BY `msg_de_perId`'
.    ' ORDER BY per_richesseDispo DESC'
);

$sql=$dbksfV3->query();
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.$sql.'</div>';

$per_richesseDispoDesc=array();
while ($dbksfV3->fetch()){
    $id=$dbksfV3->lignes['per_id'];
    $per_richesseDispoDesc[$id]=array();
    $per_richesseDispoDesc[$id]['perNP']=$dbksfV3->lignes['perNP'];
    $per_richesseDispoDesc[$id]['dispo']=$dbksfV3->lignes['dispo'];
}
$dbksfV3->queryClose();

// --- per_procheDispo --- //
$dbksfV3->sql->setOPERATION(
    'SELECT per_id'
    . ',CONCAT (per_prenom," ",UCASE(per_nom))AS perNP'
    . ',per_procheDispo AS dispo'
.    ' FROM '.TBLPREFIXE.'personnages'
.    ' WHERE per_villeId='.PER_VILLEID.$addMJ.$addBT
//.    ' GROUP BY `msg_de_perId`'
.    ' ORDER BY per_procheDispo DESC'
);

$sql=$dbksfV3->query();
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.$sql.'</div>';

$per_procheDispoDesc=array();
while ($dbksfV3->fetch()){
    $id=$dbksfV3->lignes['per_id'];
    $per_procheDispoDesc[$id]=array();
    $per_procheDispoDesc[$id]['perNP']=$dbksfV3->lignes['perNP'];
    $per_procheDispoDesc[$id]['dispo']=$dbksfV3->lignes['dispo'];
}
$dbksfV3->queryClose();
unset($sql);


// ============= //
// - affichage - //
// ============= //
echo'<h2>Stats</h2>';

// - messagerie - //
//echo"<h2 class='pointeur' onclick='blockSwitch(\"stat_msgNb\");'>Les personnages les plus prolixes</h2>";
echo'<h2>Les personnages les plus prolixes</h2>';
echo'<div id="stat_msgNb" class="stat" style="display:block;">';
$o='';
foreach($msgNb as $_perso_id => $_msgNb){
    $perNP=$msgNb[$_perso_id]['perNP'];
    $_msgNb=$msgNb[$_perso_id]['NB'];
    $pct= floor($_msgNb/$msgTotal*100);
    $o.= "$perNP:$_msgNb soit $pct%<br>";
}
unset($_msgNb);
echo "<h3>Total des messages: $msgTotal</h3>";
echo "$o</div>";


// - caracs - //
echo'<h2>Les caracs</h2>';
echo'<div id="stat_caracs" class="stat">';

echo'<h3>Richesse</h3>';
echo "<div class='noteclassic'>Il reste actuellement $richesseDispoSUM sur les $richesseMaxSUM maximal</div>";
echo "disponible:min:$richesseDispoMIN / moyenne:$richesseDispoAVG / max:$richesseDispoMAX total: $richesseDispoSUM<br>";
echo'<div>';foreach($per_richesseDispoDesc as $_val)echo $_val['perNP'].':'.$_val['dispo'].' ';echo'</div>';

echo "maximal:min:$richesseMaxMIN / moyenne:$richesseMaxAVG / max:$richesseMaxMAX total: $richesseMaxSUM<br>";

echo'<div id="graphePersoRichesse" style="height: 300px; width: 100%;"></div>';


echo'<h3>Proche</h3>';
echo "<div class='noteclassic'>Il reste actuellement $procheDispoSUM sur les $procheMaxSUM maximal</div>";
echo "disponible:min:$procheDispoMIN / moyenne:$procheDispoAVG / max:$procheDispoMAX total: $procheDispoSUM<br>";
echo'<div>';foreach($per_procheDispoDesc as $_val)echo $_val['perNP'].':'.$_val['dispo'].' ';echo'</div>';
echo "maximal:min:$procheMaxMIN / moyenne:$procheMaxAVG / max:$procheMaxMAX total: $procheMaxSUM<br>";
echo '</div>';


/*
 * date sql
Specifier   Description
%a  Abbreviated weekday name (Sun..Sat)
%b  Abbreviated month name (Jan..Dec)
%c  Month, numeric (0..12)
%D  Day of the month with English suffix (0th, 1st, 2nd, 3rd, …)
%d  Day of the month, numeric (00..31)
%e  Day of the month, numeric (0..31)
%f  Microseconds (000000..999999)
%H  Hour (00..23)
%h  Hour (01..12)
%I  Hour (01..12)
%i  Minutes, numeric (00..59)
%j  Day of year (001..366)
%k  Hour (0..23)   ***************
%l  Hour (1..12)
%M  Month name (January..December)
%m  Month, numeric (00..12)
%p  AM or PM
%r  Time, 12-hour (hh:mm:ss followed by AM or PM)
%S  Seconds (00..59)
%s  Seconds (00..59)
%T  Time, 24-hour (hh:mm:ss)
%U  Week (00..53), where Sunday is the first day of the week
%u  Week (00..53), where Monday is the first day of the week
%V  Week (01..53), where Sunday is the first day of the week; used with %X
%v  Week (01..53), where Monday is the first day of the week; used with %x
%W  Weekday name (Sunday..Saturday)
%w  Day of the week (0=Sunday..6=Saturday)
%X  Year for the week where Sunday is the first day of the week, numeric, four digits; used with %V
%x  Year for the week, where Monday is the first day of the week, numeric, four digits; used with %v
%Y  Year, numeric, four digits
%y  Year, numeric (two digits)
%%  A literal “%” character
%x  x, for any “x” not listed above
 */
global $sqlData;//recupere le code sql de la requete (cumul des requetes
function getPageDureeDataPoints($p){
    global $dbksfV3,$sqlData;
    $o='';
    $dbksfV3->sql->setOPERATION(
        'SELECT loc_id,loc_ts,loc_page,loc_duree'
    .    ',YEAR(loc_ts) AS Y,MONTH(loc_ts)-1 AS M,DAY(loc_ts) AS D'
    .    ',HOUR(loc_ts) AS H,MINUTE(loc_ts) AS N,SECOND(loc_ts) AS S'
    .    ' FROM '.TBLPREFIXE.'logs_connect'
    .    ' WHERE loc_page="'.$p.'"'
    .    ' ORDER BY loc_ts DESC'
    );
    $sqlData.=$dbksfV3->query()."\n";
    while ($dbksfV3->fetch()){

        $loc_ts=$dbksfV3->lignes['loc_ts'];
        $loc_page=$dbksfV3->lignes['loc_page'];
        $loc_duree=$dbksfV3->lignes['loc_duree'];
        $Y=$dbksfV3->lignes['Y'];$M=$dbksfV3->lignes['M'];$D=$dbksfV3->lignes['D'];
        $H=$dbksfV3->lignes['H'];$N=$dbksfV3->lignes['N'];$S=$dbksfV3->lignes['S'];
        if($o!=='')$o.=',';
        $o.="{x:new Date($Y,$M,$D,$H,$N,$S),y:$loc_duree}";
        //$o.="\n";
    }
    $dbksfV3->queryClose();
    return $o;
}



function getPageDureeMonthDataPoints($p){
    global $dbksfV3,$sqlData;
    $o='';
    $dbksfV3->sql->setFROM(TBLPREFIXE.'logs_connect');
    $dbksfV3->sql->setOPERATION(
        'SELECT loc_id,loc_ts,loc_page,loc_duree'
    .    ',loc_jouId,jou_login'
    .    ',YEAR(loc_ts) AS Y,MONTH(loc_ts)-1 AS M,DAY(loc_ts) AS D'
    .    ',HOUR(loc_ts) AS H,MINUTE(loc_ts) AS N,SECOND(loc_ts) AS S');
    $dbksfV3->sql->setJOIN('JOIN ksfv3_joueurs ON jou_id=loc_jouId');
    $dbksfV3->sql->setWHERE('loc_page="'.$p.'" AND MONTH(loc_ts)=MONTH(NOW())');
    $dbksfV3->sql->setORDERBY('loc_ts DESC');

    $sqlData.=$dbksfV3->query()."\n";
    while ($dbksfV3->fetch()){

        $loc_ts=$dbksfV3->lignes['loc_ts'];
        $login=$dbksfV3->lignes['jou_login'];
        $loc_page=$dbksfV3->lignes['loc_page'];
        $loc_duree=$dbksfV3->lignes['loc_duree'];
        $Y=$dbksfV3->lignes['Y'];$M=$dbksfV3->lignes['M'];$D=$dbksfV3->lignes['D'];
        $H=$dbksfV3->lignes['H'];$N=$dbksfV3->lignes['N'];$S=$dbksfV3->lignes['S'];
        $l=$loc_duree<0.5?'':', indexLabel: "'.$login.'", indexLabelFontColor: "orange", indexLabelOrientation: "vertical"';//IndexLabel
        if($o!=='')$o.=',';
        $o.="{x:new Date($Y,$M,$D,$H,$N,$S),y:$loc_duree$l}";
        //$o.="\n";
    }
    $dbksfV3->queryClose();
    return $o;
}


// periode: all, month
function getPageData($p,$periode='all'){
    $o='{';
    $o.='type: "line"';
    $o.=',showInLegend: true';
    $o.=',name: "'.$p.'"';
    $o.=',legendText: "'.$p.'"';
    switch ($periode){
        case 'month':      $o.=',dataPoints: ['.getPageDureeMonthDataPoints($p).']}';break;
        case 'all':default:$o.=',dataPoints: ['.getPageDureeDataPoints($p).']}';
    }
    return $o;
}


//function getRichesse(){
//    $o='{';
//    $o.='type: "column"';
//    $o.=',showInLegend: true';
//    $o.=',name: "'.PERSONO.'"';
//    $o.=',legendText: "'.PERSONO.'"';
//    $o.=',dataPoints: ['.getPageRichesseDataPoints('perso-fiche').']}';
//    return $o;
//}
//$dbksfV3->sql->setFROM(TBLPREFIXE.'logs_connect');
?>

<h2>Temps de chargement</h2>
<h3>de tout le jeu</h3>
<div id="graphePageDuree" style="height: 500px; width: 100%;"></div>
<script>
var chart = new CanvasJS.Chart("graphePageDuree",{
    /*title:{
        text: "Temps de chargement de tout le jeu"
}*/
    axisX: {
//        valueFormatString: "D/M/Y H:mm:ss"
        valueFormatString: "D/M/Y"
        ,interval:7
//        ,intervalType: "month"
        ,intervalType: "day"
//        ,intervalType: "hour"
        ,labelAngle: -50
    }
    ,axisY:{
        includeZero: false
        ,maximum: 0.9
        //,valueFormatString: "D/M/Y H:mm:ss"
    }
    ,data: [
        <?php $sqlData='';echo getPageData('perso-fiche').','.getPageData('perso-messagerie').','.getPageData('perso-echanges').','.getPageData('perso-decouvertes') ;?>
    ]
});
    chart.render();
</script>
<?php if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sqlData).'</div>';?>



<h3>du mois en cours</h3>
<h4>Connexion la plus longue (toutes pages confondues)</h4>

<?php

// - connexion la plus longue (toutes pages confondus) - //
$dureeMIN=$dureeAVG=$dureeMAX=0;
$dbksfV3->sql->setFROM(TBLPREFIXE.'logs_connect');
$dbksfV3->sql->setOPERATION('SELECT jou_login AS login,loc_page,MIN(loc_duree) AS mini,AVG(loc_duree) AS moy,MAX(loc_duree) AS maxi');
$dbksfV3->sql->setJOIN('JOIN ksfv3_joueurs ON jou_id=loc_jouId');
$dbksfV3->sql->setWHERE('MONTH(loc_ts)=MONTH(NOW())');

$sql=$dbksfV3->query()."\n";
while ($dbksfV3->fetch()){
    $dureePage=$dbksfV3->lignes['loc_page'];
    $dureeLogin=$dbksfV3->lignes['login'];
    $dureeMIN=$dbksfV3->lignes['mini'];
    $dureeAVG=$dbksfV3->lignes['moy'];
    $dureeMAX=$dbksfV3->lignes['maxi'];
}
$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

echo "page: $dureePage. Min:$dureeMIN. Avg:$dureeAVG. Max:$dureeMAX.Login: $dureeLogin ";


// - MIN/AVG/MAX - //
echo'<h4>MIN/AVG/MAX</h4>';


global $pageDurees;
$pageDurees=array();
//$dbksfV3->sql->setJOIN('');

$pageNom='perso-fiche';

function getPageDureeStat($pageNom='total'){
    global $dbksfV3,$pageDurees;
    $dbksfV3->sql->setFROM(TBLPREFIXE.'logs_connect');
    $dbksfV3->sql->setOPERATION('SELECT MIN(loc_duree) AS mini,AVG(loc_duree) AS moy,MAX(loc_duree) AS maxi');
    if($pageNom==='total')$dbksfV3->sql->setWHERE('MONTH(loc_ts)=MONTH(NOW())');
    else $dbksfV3->sql->setWHERE('loc_page="perso-fiche" AND MONTH(loc_ts)=MONTH(NOW())');

    $pageDurees[$pageNom]=array();
    $sql=$dbksfV3->query()."\n";
    while ($dbksfV3->fetch()){
        $pageDurees[$pageNom]['MIN']=$dbksfV3->lignes['mini'];
        $pageDurees[$pageNom]['MOY']=$dbksfV3->lignes['moy'];
        $pageDurees[$pageNom]['MAX']=$dbksfV3->lignes['maxi'];
    }
    $dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    //if(ISDEV===1)echo gestLib_inspect('$pageDurees',$pageDurees);
    
}
$pageNom='total';getPageDureeStat();
if($pageDurees['total']['MAX']>5)$pageDurees['total']['MAX']=5;
echo "$pageNom: MIN:".round($pageDurees[$pageNom]['MIN'],3).' AVG:'.round($pageDurees[$pageNom]['MOY'],3).' MAX:'.round($pageDurees[$pageNom]['MAX'],3).'<br>';

$pageNom='perso-fiche';getPageDureeStat($pageNom);echo "$pageNom: MIN:".round($pageDurees[$pageNom]['MIN'],3).' AVG:'.round($pageDurees[$pageNom]['MOY'],3).' MAX:'.round($pageDurees[$pageNom]['MAX'],3).'<br>';
$pageNom='perso-messagerie';getPageDureeStat($pageNom);echo "$pageNom: MIN:".round($pageDurees[$pageNom]['MIN'],3).' AVG:'.round($pageDurees[$pageNom]['MOY'],3).' MAX:'.round($pageDurees[$pageNom]['MAX'],3).'<br>';
$pageNom='perso-echanges';getPageDureeStat($pageNom);echo "$pageNom: MIN:".round($pageDurees[$pageNom]['MIN'],3).' AVG:'.round($pageDurees[$pageNom]['MOY'],3).' MAX:'.round($pageDurees[$pageNom]['MAX'],3).'<br>';
$pageNom='perso-decouvertes';getPageDureeStat($pageNom);echo "$pageNom: MIN:".round($pageDurees[$pageNom]['MIN'],3).' AVG:'.round($pageDurees[$pageNom]['MOY'],3).' MAX:'.round($pageDurees[$pageNom]['MAX'],3).'<br>';
?>

<div id="graphePageDureeMonth" style="height: 500px; width: 100%;"></div>
<script>

var chart = new CanvasJS.Chart("graphePageDureeMonth",{
    axisX: {
        valueFormatString: "D/M/Y"
        ,interval:1
        ,intervalType: "day"
        ,labelAngle: -50
    }
    ,axisY:{
    includeZero: false
        ,maximum: <?php echo $pageDurees['total']['MAX']?>
        //,valueFormatString: "D/M/Y H:mm:ss"
    }
    ,data: [
        <?php $sqlData='';echo getPageData('perso-fiche','month').','.getPageData('perso-messagerie','month').','.getPageData('perso-echanges','month').','.getPageData('perso-decouvertes','month') ;?>
    ]
});
    chart.render();

</script>
<?php if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sqlData).'</div>';
