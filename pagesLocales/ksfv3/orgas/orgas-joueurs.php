<div class="orgas"><?php
$joueurs=new gestTable('ksfv3',TBLPREFIXE.'joueurs','jou_id',['SELECT' => 'jou_id,jou_ts,jou_nom,jou_mail,jou_isFONDATEUR,jou_isADMIN,jou_isMJ,jou_isJOUEUR,jou_isDEV,jou_villeId,jou_persoNo,jou_login','clear'=>0]);
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($joueurs->dbTable->sql->getSQL()).'</div>';
//if(ISDEV===1)echo $joueurs->tableau();

$personnages=new gestTable('ksfv3',TBLPREFIXE.'personnages','per_id',['SELECT' => 'per_id,per_nom,per_prenom, CONCAT (UCASE(per_nom)," ",per_prenom) AS perNP', 'ORDERBY' => 'per_nom ASC,per_nom ASC','clear'=>0]);

if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($personnages->dbTable->sql->getSQL()).'</div>';
//if(ISDEV===1)echo $personnages->tableau();

$villes= new gestTable('ksfv3',TBLPREFIXE.'villes','vil_id',[ "ORDERBY" => "`vil_nom` ASC",'clear'=>0]);
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($villes->dbTable->sql->getSQL()).'</div>';
//if(ISDEV===1)echo $villes->tableau();

$lPaire=0;
foreach($joueurs->get() as   $jou_id => $joueur){
    if($lPaire===1){$trClasse='trP';$lPaire=0;}else    {$trClasse='trI';$lPaire=1;}
    $id=$joueur['jou_id'];
    $ts=$joueur['jou_ts'];
    $lid="joueurLogin$id";    $login=$joueur['jou_login'];
    $nid="joueurNom$id";    $nom=$joueur['jou_nom'];
    $mid="joueurMail$id";    $mail=$joueur['jou_mail'];

    $isFONDATEUR=$joueur['jou_isFONDATEUR'];
    $isADMIN=$joueur['jou_isADMIN'];
    $isMJ=$joueur['jou_isMJ'];
    $isJOUEUR=$joueur['jou_isJOUEUR'];
    $isDEV=$joueur['jou_isDEV'];

    $villeId=$joueur['jou_villeId'];
    $villeNom=$villes->get($villeId,'vil_nom');

    $jou_persoNo=$joueur['jou_persoNo'];

    // - affichage - //
    echo "\n";
    echo "<a name='$jou_id'></a>";    
    echo '<form method="POST" action="?'.ARIANE_ORGA.'#'.$jou_id.'">';
    echo "<div class='$trClasse'>";

    echo '<input name="editJouNo" type="submit" value="sauver" />';
    echo '<input name="jouNo" type="hidden" value="'.$jou_id.'" />';

    echo "no:$jou_id";

    // -- login -- //
    echo     ' login:'.IMG_EDITNC.' onclick=" inlineSwitch(\''.$lid.'\')" />'.$login
    .    '<span id="'.$lid.'" style="display:none;">'
    .        '<input name="jouLogin" value="'.$login.'" />'
    .    '</span> '
    
    // -- le nom est pas utiliser -- //
    .    ' nom:'.IMG_EDITNC.' onclick="inlineSwitch(\''.$nid.'\')" />'.$nom
    .    '<span id="'.$nid.'" style="display:none;">'
    .        '<input name="jouNom" value="'.$nom.'" />'
    .    '</span>'

    // -- mail -- //
    .    ' mail:'.IMG_EDITNC.' onclick="inlineSwitch(\''.$mid.'\')" />'.$mail
    .    '<span id="'.$mid.'" style="display:none;">'
    .        '<input name="jouMail" value="'.$mail.'" />'
    .    '</span>';
    echo "\n";
    // -- domaines -- //
    $checked=$isFONDATEUR==='1'?' checked':'';echo '<input name="isFONDATEUR" type="checkbox" value="'.$isFONDATEUR.'"'.$checked.'>fondateur '."\n";
    $checked=$isADMIN==='1'?' checked':'';echo '<input name="isADMIN" type="checkbox" value="'.$isADMIN.'"'.$checked.'>admin '."\n";
    $checked=$isMJ==='1'?' checked':'';echo '<input name="isMJ" type="checkbox" value="'.$isMJ.'"'.$checked.'>MJ '."\n";
    $checked=$isDEV==='1'?' checked':'';echo '<input name="isDEV" type="checkbox" value="'.$isDEV.'"'.$checked.'>dev '."\n";
    $checked=$isJOUEUR==='1'?' checked':'';echo '<input name="isJOUEUR" type="checkbox" value="'.$isJOUEUR.'"'.$checked.'>joueur '."\n";

    // -- personnage associe a ce joueur -- //
    echo "<select name='jouPeNo'>";
    echo '<option value="" style="background-color:#6D6D6D;">perso gérer...</option>';
    $o='';
    foreach($personnages->get() as $per_id => $personnage){
        $selected=($jou_persoNo==$per_id)?' selected="selected" class="selected"':'';
        $o.='<option value="'.$per_id.'"'.$selected.'> '.$personnage['perNP'].'</option>';
    }
    echo $o.'</select></td>';
    echo '</div>';//class='$trClasse'>";
    echo '</form>';
}
?>
</div> <!-- orgas -->




