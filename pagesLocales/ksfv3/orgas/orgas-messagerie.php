<h1 class="">messageries</h1>

<script>
function showCountCar(TEXTAREAid,SPANidHTML,max){
    //count=document.getElementById(TEXTAREAidHTML).value.length;
    spanidCSS=document.getElementById(SPANidHTML);
    spanidCSS.innerHTML=TEXTAREAid.value.length;
    if(TEXTAREAid.value.length>max){
        spanidCSS.innerHTML+='<span style="font-weight:bold;color:red;">(MAX ATTEINT)</span>';
        TEXTAREAid.value=TEXTAREAid.value.substring(0,max);
    }
//    if(TEXTAREAid.value.length)idCSS.addClass
}
</script>
<?php
global $dbksfV3,$persos;

// ============================================ //
// - chargement des tables - //
// ============================================ //
// ->reload! 

$where='per_villeId = '. PER_VILLEID .' AND per_etat='.PER_ETAT_RUN;
if( ISMJ !== 1) $where.= ' AND per_id>0';  // pour les non MJ limité aux persos actifs en jeu 

$destinataires=&$persos;
//if(ISDEV===1)echo $destinataires->tableau();


// ========================= //
// - stat sur les messages - //
// ========================= //

// - Ajout (ou non) des MJ dans les stats  - //
$addMD='';
//$addMD=' OR per_id <1';

// -- nb de message total -- //
$dbksfV3->sql->setOPERATION(
    'SELECT COUNT(*) AS TOTAL'
.    ' FROM `'.TBLPREFIXE.'messageries`'
.    ' JOIN '.TBLPREFIXE.'personnages ON per_id=msg_de_perId'
.    ' WHERE per_villeId='.PER_VILLEID.$addMD
);
$sql=$dbksfV3->query();
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

$msgTotal=0;
while ($dbksfV3->fetch())$msgTotal=$dbksfV3->lignes['TOTAL'];
//echo gestLib_inspect('$msgTotal',$msgTotal);

// -- recherche du nombre de message par personnages -- //
$dbksfV3->sql->setOPERATION(
    'SELECT `msg_de_perId`, CONCAT (per_prenom," ",UCASE(per_nom))AS perNP, COUNT(*) AS NB'
.    ' FROM `'.TBLPREFIXE.'messageries`'
.    ' JOIN '.TBLPREFIXE.'personnages ON per_id=msg_de_perId'
.    ' WHERE per_villeId='.PER_VILLEID.$addMD
.    ' GROUP BY `msg_de_perId`'
.    ' ORDER BY NB DESC'
);
$sql=$dbksfV3->query();
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

$msgNb=array();
while ($dbksfV3->fetch()){
    $id=$dbksfV3->lignes['msg_de_perId'];
    $msgNb['perNP']=$dbksfV3->lignes['perNP'];
    $msgNb[$id]=$dbksfV3->lignes['NB'];
}
$dbksfV3->queryClose();


// ============================================ //
// - envoyer le message au(x) destinataire(s) - //
// ============================================ //
$forceForm=msgSend();


// ==================================== //
// = formulaire d'envoie ou de reponse= //
// ==================================== //
msgForm($destinataires);


// ================== //
// = phase d'A/R = //
// ================== //
if(PHASE_A_NB>0){if(PER_ISACT===0)echo '<div class="txtcenter">Vous êtes dans l\'incapacit&eacute; de faire des actions.</div>';else phaseForm(3);}
if(PHASE_R_NB>0){if(PER_ISREC===0)echo '<div class="txtcenter">Vous êtes dans l\'incapacit&eacute; de faire des recherches.</div>';else phaseForm(4);}


// ===================== //
// = Boite aux lettres = //
// ===================== //
?>
<h2><span class="bold pointeur" onclick="blockSwitch('mailDescrO')">Fonctionnement de la messagerie</span></h2>
<div id="mailDescrO" style="display:none" class="">
Les personnages peuvent :
<ul>
	<li>peuvent communiquer entre eux: tous le temps,de facon illimité.</li>
	<li>peuvent communiquer avec le personnage MJ:une fois par jours en phase d'action (quelque soit le nombre positif de phase d'action)</li>
	<li>peuvent répondre à un autre joueur</li>
	<li>NE peuvent PAS répondre au MJ</li>
	<li>NE peuvent PAS répondre à une lettre anonyme (utilisation du champs "nom vu par le destinataire")</li>
</ul>
</div>
<h2>Boite aux lettres</h2><a name="bal"></a>
<?php
// - gestion de la supression de messages - //
if(ISMJ===1){
    $msgId=msgEraseAsk();
    if($msgId > 0){
        echo '<div class="notewarning">'
        . 'Demande de suppression du message n°'.$msgId.'.<br>'
        . '<a href="?msgErase='.$msgId.'&amp;'.ARIANE_AGORIA.'#msgErase">Je confirme la suppression</a>.'
        . '</div>';
    }

    $msgId=msgErase();
    if ($msgId > 0){
        echo '<div class="noteimportant">'
        . 'Le message n°'.$msgId.' &agrave; bien &eacute;t&eacute; &eacute;ffac&eacute;.<br>'
        . '</div>';
    }
}//if(ISMJ===1)


// - filtres - //
msgFiltreForm();
$where='';
if(MSGFILTREN===1)$where.=' msg_type="" OR   ISNULL(msg_type) = 1 OR msg_type="N"';
if(MSGFILTREA===1){$and=$where===''?'':' OR ';$where.=$and.' msg_type="A"';}
if(MSGFILTRER===1){$and=$where===''?'':' OR ';$where.=$and.' msg_type="R"';}
if(MSGFILTREH===1){$and=$where===''?'':' OR ';$where.=$and.' msg_type="H"';}
if($where===''){
    echo '<div class="txtcenter">Aucun message.</div>';
}
else{

    $perDec=perDecLoad(PERSONO);

    //if(ISMJ!==1)
    $where='(msg_de_perId='.PERSONO. ' OR msg_pour_perId='.PERSONO.") AND ($where)";

    $messageries=new gestTable('ksfv3',TBLPREFIXE.'messageries','msg_id',
        [
            'SELECT' => 'msg_id'
                .    ',DATE_FORMAT(msg_ts,"%d/%m/%Y %H:%i:%s") AS tsf,DATE_FORMAT(msg_ts,"%j") AS tsj'
                .    ',msg_type'
                .    ',msg_login,msg_de_perId,msg_de_txt,msg_pour_perId'
                .    ',msg_codeD'
                .    ',msg_sujet,msg_message'
                .    ',per_villeId ,CONCAT(per_prenom," ",UCASE(per_nom))  AS de_perPN' // ville, prenom NOM de l'emmetteur
                .    ',dec_id,dec_message'
    //            .    "\n".','
        ,    'JOIN'   => 'JOIN '.TBLPREFIXE.'personnages ON msg_de_perId =  per_id'."\n" // pour recup villeId de l'emmetteur
                .    ' LEFT JOIN '.TBLPREFIXE.'decouvertes ON dec_code=msg_codeD'."\n"
        ,    'WHERE'  => $where."\n"
        ,    'ORDERBY'  => 'msg_ts DESC'."\n"
    //    ,    'LIMIT'  => 50 
        ,    'clear'  => 0
        ]);
    $sql=$messageries->dbTable->sql->getSQL();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    //if(ISDEV===1)echo $messageries->tableau();

    $tsjOld=0;
    
    foreach($messageries->get() as $msg){
        $emmeteurVilleId=$msg['per_villeId'];
        $destId         =$msg['msg_pour_perId'];
        $destVilleId    =$destinataires->get($destId,'per_villeId');// attention destinataires= persos de PER_VILLEID

        if($emmeteurVilleId!=PER_VILLEID AND $destVilleId!=PER_VILLEID)continue;//ce message ne concerne pas cette ville
        $msgInterVilleClass=($emmeteurVilleId!=$destVilleId)?' msgInterVille':'' ;//message interville?

        $id    = $msg['msg_id'];

        $ts    = $msg['tsf'];
        $tsj   = $msg['tsj'];// no du jours dans l'annee 01..365
        $tsjCSS= ($tsjOld!==$tsj)?' msg_tsNewJ':'';
        $tsjOld= $tsj;

        $msg_type = $msg['msg_type'];
        if ($msg_type===NULL OR $msg_type==='')$msg_type ='N';
        $typeCSS  = " msg_type$msg_type";

        $login      = $msg['msg_login'];
        $emmeteurId = $msg['msg_de_perId'];
        $emmeteurPN = $msg['de_perPN'];    // $emmeteurPN    =$destinataires->get($emmeteurId,'perPN');
        $emmeteurTxt= $msg['msg_de_txt'];
        $destPN     = $destinataires->get($destId,'perPN');

        $isMsgIn   = TRUE;// message entrant par defaut
        if ($emmeteurId === PERSONO)$isMsgIn = FALSE; // l'emmeteur est le perso donc c'est un msg sortant

        $codeD      = $msg['msg_codeD'];
        $dec_id     = $msg['dec_id'];
        $dec_message= ln2br($msg['dec_message']);

        $sujet      = $msg['msg_sujet'];
        $message    = ln2br($msg['msg_message']);

        echo '<a name="msg'.$id.'"></a>';
        echo "<div class='msg_type$typeCSS$msgInterVilleClass'>";
        echo '<form method="POST" action="?'.ARIANE_AGORIA.'#msg'.$id.'">';
        echo '<input type="hidden" name="msgNoEdit" value="'.$id.'" />';

        // - msg_infos - //
        if(ISMJ===1){
            echo '<div class="orgas txtcenter">';
            echo '<input type="submit" value="sauver" />';
            echo " msg no $id ";

            // -- type -- //
            echo " Type:";
            $typeChecked=$msg_type==='N'?' checked':'';
            $css=$typeChecked===''?'':'msg_type '.$typeCSS;
            echo ' <span class="'.$css.'"><input type="radio" name="msg_type" value="N"'.$typeChecked.' /><label for="">normal</label></span>';

            $typeChecked=$msg_type==='A'?' checked':'';
            $css=$typeChecked===''?'':'msg_type '.$typeCSS;
            echo ' <span class="'.$css.'"><input type="radio" name="msg_type" value="A"'.$typeChecked.' /><label for="">Action</label></span>';

            $typeChecked=$msg_type==='R'?' checked':'';
            $css=$typeChecked===''?'':'msg_type '.$typeCSS;
            echo ' <span class="'.$css.'"><input type="radio" name="msg_type" value="R"'.$typeChecked.' /><label for="">Recherche</label></span>';

            $typeChecked=$msg_type==='H'?' checked':'';
            $css=$typeChecked===''?'':'msg_type '.$typeCSS;
            echo ' <span class="'.$css.'"><input type="radio" name="msg_type" value="H"'.$typeChecked.' /><label for="">HRP</label></span>';
    
            // -- codeD -- //
            echo ' <span class="orgas">Code D:<input type="text" name="msg_codeD" value="'.$codeD.'" /></span>';

            // -- eraseAsk/erase -- //
            echo ' <a href="?msgEraseAsk='.$id.'&amp;'.ARIANE_AGORIA.'#msgEraseAsk" title="Supprimer ce message"><img alt="supprimer" src="'.IMG_ROOT.'erase-ask-16x16.png"></a>';
            //echo '<a href="?msgErase='.$id.'&amp;'.ARIANE_AGORIA.'#msgErase"><img alt="supprimer" src="'.IMG_ROOT.'erase-16x16.png"></a>';
            echo '</div>';
        }


        // - msg_datas - //
        echo "<span class='msg_ts$tsjCSS'>$ts</span><br>";
        //if(ISMJ===1)echo " <span class='orgas'>$login</span>";
        //echo "<a href='?orgaPersoNo=$emmeteurId&ksfv3=messagerie' class='msg_from'>DE: $emmeteurPN $emmeteurTxt</a>";
        //echo "<span class='msg_from'> DE: $emmeteurPN $emmeteurTxt</span>";
        //if(ISMJ===1)echo " <span class='orgas'>(ville:$emmeteurVilleId)</span>";

        // - afficher emmetteur ou destinataire - //
        if (ISMJ===1){
            echo ' <span class="orgas">'.$login.'</span>';
            if ($isMsgIn === TRUE)
                echo ' DE: <a href="?orgaPersoNo='.$emmeteurId.'&ksfv3=messagerie" class="msg_from">'.$emmeteurPN.' '.$emmeteurTxt.'</a>';
            else
                echo ' POUR: <a class="msg_to" href="?orgaPersoNo='.$destId.'&amp;msg_type='.$msg_type.'&amp;'.ARIANE_AGORIA.'">'.$destPN.'</a>';
            echo ' <span class="orgas">(ville:'.$destVilleId.')</span>';
        }
        else{// c'est un perso
            if ($isMsgIn === TRUE)
                echo 'DE: <span class="msg_from">'.$emmeteurPN.' '.$emmeteurTxt.'</span>';
            else
                echo 'POUR: <span class="msg_to">'.$destPN.'</span>';
        }

        echo '<br>';


        // - Ajout bouton de reponse/ajout - //
        // - si msg NE venant PAS d'un MJ et N'etant PAS aller vers un MJ? OU MJ - //
        if ($emmeteurId>0 AND $destId>0 OR ISMJ===1){

            if ($isMsgIn === TRUE){
                $href="send=1&msg_type=$msg_type&emmetteur=$destId&destId=$emmeteurId";
                $href.='&sujet='.urlencode('r&eacute;ponse de '.$sujet);
                echo '<span class="msg_repondre"><a href="?'.htmlentities($href).'&amp;'.ARIANE_AGORIA.'" title="repondre">R&eacute;pondre &agrave:</a> ';
            }
            else{
                $href="send=1&msg_type=$msg_type&emmetteur=$emmeteurId&destId=$destId";
                $href.='&sujet='.urlencode('Ajout &agrave; '.$sujet);
                echo '<span class="msg_repondre"><a href="?'.htmlentities($href).'&amp;'.ARIANE_AGORIA.'" title="ajouter">Ajouter &agrave:</a> ';
            }
        }
        
        echo '<span class="msg_sujet">'.$sujet.'</span>';

        echo '<div class="msg_message">'.$message.'</div></span>';
    
        if($codeD!=NULL){
            echo '<span class="msg_codeD"> <a href="?codeD='.$codeD.'&amp;ksfv3=perso-decouvertes#codeD'.$codeD.'" title="">'.$codeD.'</a></span>';
            if($dec_message!=NULL){
                //afficher le msg uniquement si acheté
                //if(ISMJ===1)echo gestLib_inspect('$perDec[$dec_id]',$perDec[$dec_id]);
                $o=isset($perDec[$dec_id])?$dec_message:"Vous n'avez pas acheté cette découverte.";
                echo '<div class="msg_codeDMsg">'.$o.'</div>';
                unset($o);
            }
            else
                echo '<div class="orgas">le code '.$codeD.' n\'est pas d&eacute;fini! <a href="?create=decouverte&orgas=orgas-decouvertes&#decouvertes" target="orgas">Cr&egrave;er une d&egrave;couverte?</a></div>';
        }
    echo '</form></div>'."\n";
    }//foreach($messageries->get() as $msg)
    
unset($messageries);
}//if($where==='') else
