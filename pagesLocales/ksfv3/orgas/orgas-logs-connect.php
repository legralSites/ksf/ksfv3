<h1 class="">logs des connexions</h1>
<?php
global $dbksfV3;


// ========================= //
// - stat sur les logs - //
// ========================= //
// - Ajout (ou non) des MJ dans les stats  - //
$addMD='';
//$addMD=' OR per_id <1';

// -- nb de message total -- //
$dbksfV3->sql->setOPERATION('SELECT COUNT(*) AS TOTAL FROM `'.TBLPREFIXE.'logs_connect`');
$sql=$dbksfV3->query();
if(ISDEV===1)echo '<div class="coding_code">'.ln2br($sql).'</a></div>';

$logTotal=0;
while ($dbksfV3->fetch())$logTotal=$dbksfV3->lignes['TOTAL'];
//if(ISDEV===1)echo gestLib_inspect('$msgTotal',$msgTotal);


// -- recherche du nombre de pages appellées -- //
$dbksfV3->sql->setOPERATION(
    'SELECT `loc_page`, COUNT(*) AS NB'
.    ' FROM '.TBLPREFIXE.'logs_connect'
.    ' GROUP BY loc_page'
.    ' ORDER BY NB DESC'
);
$sql=$dbksfV3->query();
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

$pageNb=array();
while ($dbksfV3->fetch()){
    $page=$dbksfV3->lignes['loc_page'];
    $pageNb[$page]=$dbksfV3->lignes['NB'];
}
$dbksfV3->queryClose();
//if(ISDEV===1)echo gestLib_inspect('msgNb',$msgNb);
?>
<h2>Stats</h2>
<h3 class="pointeur" onclick="blockSwitch('stat_pageNb');">Les pages les plus consultés</h3>
<div id="stat_pageNb" class="stat" style="display:none;">
<?php
$o='';
foreach($pageNb as $pageNom => $_pageNb){
//    $NP=$persos->get($_perso_id,'perNP');
    $pct= floor($_pageNb/$logTotal*100);
    $o.= "<span class='loc_menuPage'>$pageNom</span>:$_pageNb fois soit $pct%<br>";
}

echo "Total des consultations de pages: $logTotal<br>";
echo $o;
echo '</div>';

//$log_detail=(isset($_GET['log_detail']))?$_GET['log_detail']:0;
$log_detail=1;// detail forcer
$daysR=(isset($_GET['daysR']))?$_GET['daysR']:0;    // jours relatif (negatif)


// ============================================ //
// - chargement des tables - //
// ============================================ //
$where=NULL;
$day=date('z')+$daysR+1;
if($daysR<=0){
    $where='DATE_FORMAT(loc_ts,"%j") = '.$day;
    //$log_detail=1;
}


// -  afficher les logs de connexionx de tous les joueurs - //
$logs_connect=new gestTable('ksfv3',TBLPREFIXE.'logs_connect','loc_id',
    [     'SELECT'  => 'loc_id, loc_ts, loc_duree, loc_jouId, loc_IP, loc_menu, loc_page'
            .     ', jou_nom'
            .     ',`engine-version`,`instance-version`'
            .     ', loc_userAgent'
            .     ',CONCAT (per_prenom," ",UCASE(per_nom)) AS perPN'
            .     ',DATE_FORMAT(loc_ts,"%d/%m/%Y %H:%m:%s") AS tsf,DATE_FORMAT(loc_ts,"%j") AS tsj'
        ,'JOIN'=> ' LEFT JOIN '.TBLPREFIXE.'joueurs ON jou_id = loc_jouId'
                 .' LEFT JOIN '.TBLPREFIXE.'personnages ON per_id= jou_persoNo'
        ,'WHERE'   => $where
        ,'ORDERBY' => 'loc_ts ASC'
    ]);
//if(ISDEV===1)echo $logs_connect->tableau();

// -- tableau general pour l'organisateur -- //
?>

<a name="bal"></a>
Filtres:
log détaillé:
 <a href="?log_detail=1&amp;daysR=1&amp;<?php echo ARIANE_ORGA?>#log-connect_bottom">Tous les jours</a>: 
 <a href="?log_detail=1&amp;daysR=-7&amp;<?php echo ARIANE_ORGA?>#log-connect_bottom">-7</a>
 <a href="?log_detail=1&amp;daysR=-6&amp;<?php echo ARIANE_ORGA?>#log-connect_bottom">-6</a>
 <a href="?log_detail=1&amp;daysR=-5&amp;<?php echo ARIANE_ORGA?>#log-connect_bottom">-5</a>
 <a href="?log_detail=1&amp;daysR=-4&amp;<?php echo ARIANE_ORGA?>#log-connect_bottom">-4</a>
 <a href="?log_detail=1&amp;daysR=-3&amp;<?php echo ARIANE_ORGA?>#log-connect_bottom">-3</a>
 <a href="?log_detail=1&amp;daysR=-2&amp;<?php echo ARIANE_ORGA?>#log-connect_bottom">-2</a>
 <a href="?log_detail=1&amp;daysR=-1&amp;<?php echo ARIANE_ORGA?>#log-connect_bottom">hier</a>
 <a href="?log_detail=1&amp;daysR=0&amp;<?php echo ARIANE_ORGA?>#log-connect_bottom">aujourd'hui</a>
<br>
<?php
$tsjOld=0;
$loginOld='';
foreach($logs_connect->get() as $loc){
    $id        =$loc['loc_id'];

    $tsf       =$loc['tsf'];
    $tsj       =$loc['tsj'];// no du jours dans l'annee 01..366
    $tsjCSS=($tsjOld!==$tsj)?' msg_tsNewJ':'';
    $tsjOld=$tsj;

    //$type    =$loc['msg_type'];
    //$typeCSS =" msg_type$type";

    $loc_duree =$loc['loc_duree'];
    $loc_IP    =$loc['loc_IP'];

    $loc_engine_version  =$loc['engine-version'];
    $loc_instance_version=$loc['instance-version'];

    $loc_menu  =$loc['loc_menu'];
    $loc_page  =$loc['loc_page'];

    $loc_userAgent=$loc['loc_userAgent'];

    $login     =$loc['jou_nom'];
    if($login!=$loginOld){$loginChg=1;$loginOld=$login;}else{$loginChg=0;}
    $perPN=$loc['perPN'];

    if($tsjCSS!=='')echo"<br><br><div class='msg_ts$tsjCSS'>$tsf</div>";
    if($loginChg===1 OR $log_detail==1){
        echo '<div class="">';
        echo ' <span class"msg_ts">'.$tsf.'</span>';
        echo ' <span style="display:inline-block;width:8rem;">'.$loc_duree.'</span>';
        echo ' <span style="display:inline-block;width:8rem;">'.$loc_engine_version.'/'.$loc_instance_version.'</span>';
        echo ' <span class="msg_login" style="display:inline-block;width:10rem;">'.$login.'</span>';
        echo ' <span style="display:inline-block;width:10rem;">'.$perPN.'</span>';
        echo ' <a style="display:inline-block;width:10rem;" target="whois" href="http://www.whois.com/whois/'.$loc_IP.'">';
        echo '<span style="display:inline-block;width:10rem;">'.$loc_IP.'</span></a>';
    }
    $url= '?'.$loc_menu.'='.$loc_page;
    echo ' <a href="'.$url.'" class="loc_menuPage">'.$url.'</a>';
    echo '<br>';
    echo' <div style="position:absolute;right:0;margin-left:50px;font-style:italic;color:white;">'.$loc_userAgent.'</div>';
    echo '<br>';
    if($loginChg===1 OR $log_detail==1)echo '</div>';
    echo "\n";
}?>
<a name="log-connect_bottom"></a>
