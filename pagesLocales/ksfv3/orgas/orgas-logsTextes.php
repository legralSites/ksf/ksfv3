<?php
$logTextes= new gestTable('ksfv3',TBLPREFIXE.'logTextes','lox_id',[
        'WHERE' => 'lox_id >=0 AND lox_id<100'
    ,   'clear' => 0
    ]);
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($logTextes->dbTable->sql->getSQL()).'</div>';
//if(ISDEV===1)echo $logTextes->tableau();


// - renvoie les id disponible de la table entre min et max inclu - //
// lt=$logTextes
function loxCreateSelect(&$lt,$min,$max,$snom,$iSelected){
    $o="\n";
    for($i=$min;$i<=$max;$i++){
        //echo gestLib_inspect('$logTextes->lignes',$logTextes->lignes[$i],"",__CLASS__.':'.__FUNCTION__.':'.__LINE__,"br");
        if(!isset($lt->lignes[$i])){
            $selected=($i==$iSelected)?' selected=" selected"':'';
            $o.= "<option value='$i'$selected>$i </option>";
        }
    }
    return "<select name='$snom'>$o</select>";
}


function loxShowTableau(&$logTextes,$min,$max){
?>
<form method="POST" action="?<?php echo ARIANE_ORGA?>">
<table><caption>(<input type="submit" value="sauver" />)</caption>
<thead><tr><th>id <input type="submit" value="sauver" /><b>le no en 1er</b></th> <th>texte</th></thead></tr>
<tbody>
<?php
$Nb=0;
foreach($logTextes->get() as  $_lox){
    $Nb++;
    $id=$_lox['lox_id'];
    $iid="diI$id";
    $tid="diT$id";    $texte=$_lox['lox_texte'];

    echo    '<tr>';
    // !! change l'index !! //
    echo
        '<td>'.IMG_EDITNC.' onclick="inlineSwitch(\''.$iid.'\')" />'.$id
    .   '<span id="'.$iid.'" style="display:none;">'
    //.     '<input name="lox_id_Edit'.$id.'" type="text" value="'.$id.'" />'
    .       loxCreateSelect($logTextes,$min,$max,'lox_id_Edit'.$id,$id)
    .   '</span></td>';

    echo 
        '<td>'.IMG_EDITNC.' onclick="inlineSwitch(\''.$tid.'\')" />'.$texte
    .   '<span id="'.$tid.'" style="display:none;">'
    .       '<input name="loxTextEdit'.$id.'" type="text" value="'.$texte.'" />'
    .   '</span></td>';

    echo '</tr>';
}
unset($logTextes);
?>
</tbody></table>
<input name="loxNb" type="hidden" value="<?php echo $Nb?>" />
</form>
<?php }//function loxShowTableau(&$logTextes)
?>

<!--a href="?create=lox&amp;<?php echo ARIANE_ORGA?>">ajouter</a-->


<a name="logJ"></a><h1 class="h1">Joueurs (1..99)<a href="?create=loxJ&amp;<?php echo ARIANE_ORGA?>#logJ">ajouter</a></h1>
<?php $logTextes= new gestTable('ksfv3',TBLPREFIXE.'logTextes','lox_id',['WHERE' => 'lox_id >=0   AND lox_id<100']);loxShowTableau($logTextes,1,20);?>

<a name="logO"></a><h1 class="h1">Organisateurs (100..199)<a href="?create=loxO&amp;<?php echo ARIANE_ORGA?>#logO">ajouter</a></h1>
<?php $logTextes= new gestTable('ksfv3',TBLPREFIXE.'logTextes','lox_id',['WHERE' => 'lox_id >=100 AND lox_id<200']);loxShowTableau($logTextes,100,150);?>

<a name="logP"></a><h1 class="h1">personnages (200..299)<a href="?create=loxP&amp;<?php echo ARIANE_ORGA?>#logP">ajouter</a></h1>
<?php $logTextes= new gestTable('ksfv3',TBLPREFIXE.'logTextes','lox_id',['WHERE' => 'lox_id >=200 AND lox_id<300']);loxShowTableau($logTextes,200,250);?>

<a name="logA"></a><h1 class="h1">autres (>=300)<a href="?create=loxA&amp;<?php echo ARIANE_ORGA?>#logA">ajouter</a></h1>
<?php $logTextes= new gestTable('ksfv3',TBLPREFIXE.'logTextes','lox_id',['WHERE' => 'lox_id >=300']);loxShowTableau($logTextes,300,320);
