<?php
$logs= new gestTable('ksfv3',TBLPREFIXE.'logs','log_id',[
     'SELECT'=>'log_id,domaine,fromLogin,toPersoNo,per_prenom,actionNo,lox_texte,extra'
    .     ',CONCAT (per_prenom," ",UCASE(per_nom)) AS perPN'
    .     ',DATE_FORMAT(ts,"%d/%m/%Y %H:%m:%s") AS tsf,DATE_FORMAT(ts,"%j") AS tsj'
    ,'JOIN'=>' INNER JOIN '.TBLPREFIXE.'logTextes ON actionNo = lox_id'
    .    ' INNER JOIN '.TBLPREFIXE.'personnages ON toPersoNo = per_id'
    ,'ORDERBY'=>'ts DESC'
]);
$sql=$logs->dbTable->sql->getSQL();
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
//if(ISDEV===1)echo $logs->tableau();
?>
<a href="#joeursJournauxBottom">Aller en bas de la liste</a>
<div class="" style="max-height:25em;overflow:auto;">
<?php
$tsjOld=0;
foreach($logs->get() as  $_log){
    //if(ISDEV===1)echo $gestLib->debugShowVarOrigine('ksfv3',LEGRALERR::DEBUG,NULL,__CLASS__.':'.__METHOD__.':'.__LINE__,'$_item',$_item);
    //$ts=$_log['ts'];
    $tsf        =$_log['tsf'];
    $tsj        =$_log['tsj'];// no du jours dans l'annee 01..366
    $tsjCSS=($tsjOld!==$tsj)?' msg_tsNewJ':'';
    $tsjOld=$tsj;
    $domaine=$_log['domaine'];
    $fromLogin=(ISORGA===1)?$_log['fromLogin']:'';
    $toPersoNo=$_log['toPersoNo'];
    $perPN=$_log['perPN'];
    $actionNo=$_log['actionNo'];
    $texte=$_log['lox_texte'];
    $extra=$_log['extra'];
    echo "<div><span class='msg_ts$tsjCSS'>$tsf</span> de <span class='orgas msg_login'>$fromLogin</span>:<span class=''>$perPN</span>  [$domaine] pour <b>$perPN</b> $texte <i>$extra</i></div>";
}
unset($logs);
?><a name="joeursJournauxBottom"></a></div>





