<?php
global $menuOrgas,$persos;
?>

<h1>Gestion des DECOUVERTES</h1>

<h2 class="h2"><a href="?create=decouverte&amp;<?php echo $menuOrgas->arianeFULL?>#decouvertes">Ajouter une DECOUVERTE</a></h2>
<div class="notewarning">Il ne peut y avoir 2 codes identiques.</div>
<a name="decouvertes"></a>
<?php
$decouvertes= new gestTable('ksfv3',TBLPREFIXE.'decouvertes','dec_id',[
    'WHERE' => 'dec_villeId = '.VILLEID.' AND dec_jouId='.JOUNO
    ,'ORDERBY'=>'dec_id DESC'
    ,'clear'=>0
]);
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($decouvertes->dbTable->sql->getSQL()).'</div>';
/*
$persos=new gestTable("ksfv3",TBLPREFIXE.'personnages','per_id',[
    'SELECT'=> 'per_id, CONCAT(UCASE(per_nom)," ",per_prenom) AS perNP'
    ,'WHERE' => 'per_villeId = '.VILLEID
    ,'ORDERBY' => 'per_nom ASC,per_prenom ASC'
    ,'clear'=>0
]);
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($persos->dbTable->sql->getSQL()).'</div>';
 */
//if(ISDEV===1)echo $decouvertes->tableau();
$talents=  new gestTable('ksfv3',TBLPREFIXE.'talents', 'tal_id',['clear'=>0]);
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($talents->dbTable->sql->getSQL()).'</div>';
$reseaux=  new gestTable('ksfv3',TBLPREFIXE.'reseaux', 'res_id',['clear'=>0]);
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($reseaux->dbTable->sql->getSQL()).'</div>';

$villeNom=getVilleNom(VILLEID);
$Nb=0;
$ligneCSS='';
//$dpCall='';
//
echo '<div class="notewarning">La condition d&eacute;sactiv&eacute; ne fonctionne pas. L achat sera fait meme si le personnage &agrave; le talent ou le reseau d activ&eacute;!</div>';
foreach($decouvertes->get() as $r){
    $Nb++;

    $id=$r['dec_id'];
    $ligneCSS=$ligneCSS==='ligneP'?'ligneI':'ligneP';
    $jouId=$r['dec_jouId'];
    $villeId=$r['dec_villeId'];
    $nom=$r['dec_nom'];
    $description=$r['dec_description'];
    $code=$r['dec_code'];
    $AD=$r['dec_AD'];
    //echo "AD:'$AD'<br>";
    $msg=$r['dec_message'];
    $dateFin=$r['cdt_dateFin'];

    echo'<a name="decouverte'.$id.'"></a><div class="rechercheSet '.$ligneCSS.'"><form method="POST" action="?'.$menuOrgas->arianeFULL.'#decouverte'.$id.'">';
    echo"<input type='submit'  name='decouverteSet' value='sauver' />";
    echo"Découverte No $id";
    echo" <input type='hidden'  name='decouverteNo' value='$id' />";
    echo"nom:<input name='dec_nom' value='$nom' />";
    echo" code:<input name='dec_code' value='$code' />";
    echo' cr&eacute;ateur: '.getJoueurNom($jouId);

    echo" ville: $villeNom";

    // -- Date limite -- //
    //echo"Fin:<input id='cdt_dateFin$id' name='cdt_dateFin' style='cursor:help;width:120px;'   length='20' maxlength='19' value='$dateFin' title='Cette recherche sera impossible &agrave; partir de cette date format(YYYY/MM/JJ hh:mm:ss)'/>";
    //$dpCall.='$( "#cdt_dateFin'.$id.'" ).datepicker({inline: true,dateFormat: "yy-mm-dd" ,autoSize: true ,buttonText: "Choose",closeText: "Fermer"});';

    // -- AutoDestruction -- //
//    $checked=$AD==1?' checked="checked"':'';
//    echo' <span class="" style="cursor:help;" title="Ce message ne sera plus accessible après la 1ere lecture (par quiconque!)" />'
//        .    "<input type='checkbox' name='dec_AD' value='$AD'$checked />Auto Destruction"
//        .    '</span>';

    echo'<br>';

    // -- Description -- //
    echo'Description: <input style="display:block;width:500px;" name="dec_description" value="'.$description.'" title="description" class="RDescription"  length="60" placeholder="description" />';


    // = conditions = //

    // -- destinataire -- //
    $cdt=$r["cdt_persoNo"];
    if($cdt!==NULL)$cdt=(int)$cdt;    // transtypé $cdt en numeric
    $s='Personnage: <select name="cdt_persoNo")">';
    //$s.='<option disabled="disabled">personnages:</option>';
    $selected=($cdt===NULL)?' selected class="selected"':'';
    $s.="<option value=NULL$selected>Tout le monde</option>";
    foreach($persos->get() as $_perso_id => $_perso){
        $selected=($cdt===$_perso_id)?' selected class="selected"':'';
        $PN=$_perso['perPN'];
        $s.='<option value="'.$_perso_id.'" title="'.$PN.'"'.$selected.'> '.$PN.'</option>'."\n";
    }
    $s.='</select>';
    echo $s;

    //echo gestLib_inspect('$cdt',$cdt);
    //echo gestLib_inspect('$_perso_id',$_perso_id);

    // -- richesses -- //
    $cdt=$r["cdt_richessePrix"];
    echo'<div class="Rrichesse">';
    echo'<span class="RTitre">Richesse</span>';
    echo "<select name='cdt_richessePrix'>";
    $selected=$cdt===NULL?' selected class="selected"':''; echo"<option value=NULL $selected title='quelque soi sa richesse'/>n/a</option>";
    $selected=$cdt==='0'?' selected  class="selected"':''; echo"<option value='0'$selected title='une recherche de fauché!' />0</option>";
    $selected=$cdt==='1'?' selected  class="selected"':''; echo"<option value='1'$selected />1</option>";
    $selected=$cdt==='2'?' selected  class="selected"':''; echo"<option value='2'$selected />2</option>";
    $selected=$cdt==='3'?' selected  class="selected"':''; echo"<option value='3'$selected />3</option>";
    $selected=$cdt==='4'?' selected  class="selected"':''; echo"<option value='4'$selected />4</option>";
    $selected=$cdt==='5'?' selected  class="selected"':''; echo"<option value='5'$selected />5</option>";
    echo'</select>';
    echo'</div>';

    // -- proches -- //
    $cdt=$r["cdt_prochePrix"];
    echo'<div class="Rproche">';
    echo'<span class="RTitre">Proche</span>';
    echo "<select name='cdt_prochePrix'>";
    $selected=$cdt===NULL?' selected class="selected"':''; echo"<option value=NULL $selected style='cursor:help;' title='quelque soi son niveau en proche'/>n/a</option>";
    $selected=$cdt==='0'?' selected  class="selected"':''; echo"<option value='0'$selected style='cursor:help;' title='Résérvé au solitaire!' />0</option>";
    $selected=$cdt==='1'?' selected  class="selected"':''; echo"<option value='1'$selected style='cursor:help;' />1</option>";
    $selected=$cdt==='2'?' selected  class="selected"':''; echo"<option value='2'$selected style='cursor:help;' />2</option>";
    $selected=$cdt==='3'?' selected  class="selected"':''; echo"<option value='3'$selected style='cursor:help;' />3</option>";
    $selected=$cdt==='4'?' selected  class="selected"':''; echo"<option value='4'$selected style='cursor:help;' />4</option>";
    $selected=$cdt==='5'?' selected  class="selected"':''; echo"<option value='5'$selected style='cursor:help;' />5</option>";
    echo'</select>';
    echo'</div>';



    // -- talents -- //
    echo'<div class="Rtalents">';
    echo'<div class="RtalTitre">Talents</div>';
    for($i=1;$i<=TALNB;$i++){
        $cdt=$r["cdt_tal$i"];
        echo '<span class="RNom">&nbsp;'.$talents->get($i,'tal_nom').'</span>:';
        echo "<select name='cdt_tal$i'>";
        $selected=$cdt===NULL?' selected class="selected"':''; echo"<option value=NULL $selected title=''/>n/a</option>";
        $selected=$cdt==='-1'?' selected  class="selected"':''; echo"<option value='-1'$selected style='cursor:help;' title='NE doit PAS &ecirc;tre activ&eacute;'/>D&eacute;sactiv&eacute;</option>";
        $selected=$cdt==='0'?' selected  class="selected"':''; echo"<option value='0'$selected style='cursor:help;' title='DOIT &ecirc;tre activ&eacute;' />activ&eacute;</option>";
        $selected=$cdt==='1'?' selected  class="selected"':''; echo"<option value='1'$selected style='cursor:help;' title='coût:1' />1</option>";
        $selected=$cdt==='2'?' selected  class="selected"':''; echo"<option value='2'$selected style='cursor:help;' title='coût:2' />2</option>";
        $selected=$cdt==='3'?' selected  class="selected"':''; echo"<option value='3'$selected style='cursor:help;' title='coût:3' />3</option>";
        $selected=$cdt==='4'?' selected  class="selected"':''; echo"<option value='4'$selected style='cursor:help;' title='coût:4' />4</option>";
        $selected=$cdt==='5'?' selected  class="selected"':''; echo"<option value='5'$selected style='cursor:help;' title='coût:5' />5</option>";
        echo'</select>';
    }
    echo'</div>';

    // -- reseaux -- //
    echo'<div class="Rreseaux">';
    echo'<div class="RresTitre">R&eacute;seaux</div>';

    for($i=1;$i<=RESNB;$i++){
        $cdt=$r["cdt_res$i"];
        echo '<span class="RNom">&nbsp;'.$reseaux->get($i,'res_nom').'</span>:';
        echo "<select name='cdt_res$i'>";
        $selected=$cdt===NULL?' selected class="selected"':''; echo"<option value=NULL $selected title=''/>n/a</option>";
        $selected=$cdt==='-1'?' selected class="selected"':''; echo"<option value='-1'$selected style='cursor:help;' title='NE doit PAS &ecirc;tre activ&eacute;'/>D&eacute;sactiv&eacute;</option>";
        $selected=$cdt==='0'?' selected  class="selected"':''; echo"<option value='0'$selected style='cursor:help;' title='DOIT &ecirc;tre activ&eacute;' />activ&eacute;</option>";
        $selected=$cdt==='1'?' selected  class="selected"':''; echo"<option value='1'$selected style='cursor:help;' title='coût:1' />1</option>";
        $selected=$cdt==='2'?' selected  class="selected"':''; echo"<option value='2'$selected style='cursor:help;' title='coût:2' />2</option>";
        $selected=$cdt==='3'?' selected  class="selected"':''; echo"<option value='3'$selected style='cursor:help;' title='coût:3' />3</option>";
        $selected=$cdt==='4'?' selected  class="selected"':''; echo"<option value='4'$selected style='cursor:help;' title='coût:4' />4</option>";
        $selected=$cdt==='5'?' selected  class="selected"':''; echo"<option value='5'$selected style='cursor:help;' title='coût:5' />5</option>";
        echo'</select>';
    }
    echo'</div>';

    echo'<div class="RpouTitre">Message découvert par le personnage:</div>';
    echo"<textarea name='dec_msg' class=''>$msg</textarea>";
    echo'</form></div><br>';
}
