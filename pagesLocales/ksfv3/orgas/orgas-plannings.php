<h1>plannings</h1>

<div class="noteimportant">
<div class="txtcenter"><b>Concernant les phases de Recherche et d'Action:</b></div>
La gestion des phases simultanné est géré.<br>
Vous pouvez avoir plusieurs phase du même type qui se chevauche.<br>
Tant que le compteur n'a pas été resété: la quantité de phase d'un meme type se cumule. C'est a dire que si un joueur n'a pas fait ses phases il pourra les utiliser..<br>
Le joueur aura autant de recherche ou d'action qu'il y a de phase (de ce type) en cours.<br>
Les phases non jouées quand il n' y a plus de phase en cours sont perdus.<br>
Si la description Joueur est non vide alors elle sera afficher en sujet sinon ce sera la date de début d'édition de la réponse.(Le mJ recoit la date de reception)<br>
</div>

<a name="Reset"></a>
<h2>r&eacute;initialisation (ATTENTION PAS DE CONFIRMATION)</h2>

<?php
global $dbksfV3;
if (isset($_GET['reset']))switch ($_GET['reset']){
    case 'scenario':
        $dbksfV3->sql->clear();
        $sql='/* reinit de la richesse et de proche */';
        $sql.='    UPDATE `'.TBLPREFIXE.'personnages` SET ';
        $sql.='    `per_richesseDispo` = `per_richesseMax`, `per_procheDispo`   = `per_procheMax`';
        $sql.=' WHERE per_villeId = '.VILLEID.';';
        //$dbksfV3->sql->setOPERATION("CALL resetScenario(VILLEID)");
        $dbksfV3->sql->setOPERATION($sql);$sql=$dbksfV3->query();$dbksfV3->queryClose();
        if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
        echo '<div class="noteimportant">Le scénario de la ville no '.VILLEID.' à été resetté.</div>';
        break;

    case 'talentsReseauxDispo':
        resetTalRes(VILLEID);
        echo '<div class="noteimportant">Les disponibilit&eacute;s de tous les talents/reseaux ont &eacute;t&eacute; res&eacute;tt&eacute;s (pour les personnages de la ville no '.VILLEID.').</div>';
        break;

    case 'phaseCpt':
        $dbksfV3->sql->clear();
        //$sql='/* suppression des compteurs de phase des personnages de la ville */'."\n";
        $sql='    DELETE FROM `'.TBLPREFIXE.'phasesCpt` WHERE villeId= '.VILLEID.';';
        $dbksfV3->sql->setOPERATION($sql);$sql=$dbksfV3->query();$dbksfV3->queryClose();
        if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
        echo '<div class="noteimportant">Les compteurs de phase des personnages de la ville no '.VILLEID.' ont été supprimés.</div>';
        break;
    
    // - personnages - //
    // -- journaux -- //
    case 'perLog':
        // - rechercher la ville de toPersoNo et la mettre en conditon WHERE - //
        
//        $dbksfV3->sql->clear();
//        $sql='/* suppression des journaux des personnages de la ville */';
        //$sql.='    DELETE FROM `'.TBLPREFIXE.'logs` WHERE villeId= '.VILLEID.';';
//        $sql.='    DELETE FROM `'.TBLPREFIXE.'logs` ;';

//        $dbksfV3->sql->setOPERATION($sql);
//        $sql=$dbksfV3->query();$dbksfV3->queryClose();
//        if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
//        echo '<div class="noteimportant">Les journaux des joueurs de <b>TOUTES LES VILLES</b> ont été supprimés.</div>';
        break;

    // -- messages -- //
    case 'perMes':
        //$dbksfV3->sql->clear();
        //$sql='/* suppression des messages des personnages de la ville */';
        //$sql.='    DELETE FROM `'.TBLPREFIXE.'messageries` WHERE villeId= '.VILLEID.';';

        //$dbksfV3->sql->setOPERATION($sql);$sql=$dbksfV3->query();$dbksfV3->queryClose();
        //if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
        //echo '<div class="noteimportant">Les messages des joueurs de la ville no '.VILLEID.' ont été supprimés.</div>';
        //echo '<div class="notewarning">FONCTION DESACTIVE CAR TROP DANGEUREUSE !!!</div>';
        break;
}

?>

<h3>Reset du <a href="?reset=scenario&orgas=orgas-plannings#Reset">scénario</a> de la ville N°<?php echo VILLEID?> (ville inscrit sur votre <a href="?ksfv3=orgas&ksfv3=profils">profil joueur</a>):</h3>
<ul>
    <li>richesseDispo=richesseMax</li>
    <li>procheDispo=procheMax</li>
</ul>


<h3>Reset des phases de la ville</h3>
<ul>
    <li>reset TALENTS/RESEAUX <a href="?reset=talentsReseauxDispo&ksfv3=orgas&orgas=orgas-plannings#Reset">disponibilité des TALENTS/RESEAUX</a> des personnages de la ville N°<?php echo VILLEID?> (ville inscrit sur votre <a href="?ksfv3=profils">profil joueur</a>):<ul>
        <li>Tous les talentDispo = reseaux= 0</li>
        <li>Si talent Actif alors talentDispo = 1</li>
        <li>Si reseau Actif alors reseauDispo = 1</li>
    </ul></li>

    <li>Reset du <a href="?reset=phaseCpt&orgas=orgas-plannings#Reset">compteur de phase</a>:<ul>
        <li>le nombre de phase d'ACTION éffectué par le joueur revient à 0.</li>
        <li>le nombre de phase de RECHERCHE éffectué par le joueur revient à 0.</li>
    </ul></li>
</ul>
<br>

<!--
<div class="notewarning">!!! ATTENTION Les reset suivants sont a utiliser avec PRECAUTION !!!</div>
<h3>Reset des information des personnages de <b>TOUTES</b> les villes!</h3>
<ul>
    <li>Reset des <a href="?reset=perLog&ksfv3=orgas&orgas=orgas-plannings#Reset">journaux des personnages</a></li>
    <li>Reset des <a href="?reset=perMes&ksfv3=orgas&orgas=orgas-plannings#Reset">messages des personnages</a></li>


</ul>
<br>
<link  href="lib/tiers/js/jquery-ui-1.11.4.datePicker/jquery-ui.css" rel="stylesheet">
<script src="lib/tiers/js/jquery-ui-1.11.4.datePicker/external/jquery/jquery.js"></script>
<script src="lib/tiers/js/jquery-ui-1.11.4.datePicker/jquery-ui.js"></script>

<link  href="lib/tiers/js/jquery-ui-1.11.4.datePicker/jquery-ui.css" rel="stylesheet">
<script src="lib/tiers/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon-v1.5.3.js"></script>
-->
<?php
echo    '<div class="noteclassic"><b>PHASES EN COURS DANS LA VILLE</b><br>PHASE ACTION:'.PHASE_A_NB.'<br>PHASE RECHERCHE:'.PHASE_R_NB.'</div>';

$select= '*'
.    ',  (pla_type=1 OR `pla_debut`="0000-00-00 00:00:00")AS NoP'
.    ',  `pla_debut` >  NOW() AS a_venir'
.    ', (`pla_debut` <= NOW() AND   NOW() <= `pla_fin`) AS en_cours'
.    ', (`pla_fin` < NOW() AND `pla_debut`!= "0000-00-00 00:00:00") AS terminer '
.    ',DATE_FORMAT(`pla_debut`,"%Y-%H-%m %H:%i:%s") AS dtf'
;

$plannings=new gestTable('ksfv3',TBLPREFIXE.'planning','pla_id'
    ,[
        'SELECT'=> "$select"
        ,'WHERE' => 'pla_villeId = '.VILLEID
        ,'JOIN'=>'JOIN '.TBLPREFIXE.'planningType ON '.TBLPREFIXE.'planning.pla_type = '.TBLPREFIXE.'planningType.plt_id'
        ,'ORDERBY'=>'pla_debut'
    ]
);
//echo $plannings->tableau();

$planningTypes=new gestTable('ksfv3',TBLPREFIXE.'planningType','plt_id');
//echo $planningTypes->tableau();

// periodeNom= alias sql: 'NoP', 'a_venir', ,'en_cours', 'terminer'
function planningShow(&$p,&$pt,$periodeNom){
    //global $planningTypes;
    $display=($periodeNom==='terminer')?' style="display:none;"':'';
    $Nb=0;
    $lPaire=0;
    $out='';

    $out.='<a name="planning'.$periodeNom.'"></a>';
    $out.='<div id="form'.$periodeNom.'"'.$display.'><form method="POST" action="?'.ARIANE_ORGA.'#planning'.$periodeNom.'">';
    $out.='<div class="txtleft"><input type="submit" value="sauver" /></div>';
    //echo $p->tableau();

    foreach($p->get() as  $pla_id => $planning){    

        if($planning[$periodeNom]!=1)continue;
        $Nb++;
        if($lPaire===1){$trClasse='trP';$lPaire=0;}else    {$trClasse='trI';$lPaire=1;}

        $type       =$planning['pla_type'];
        $typeNom    =$planning['plt_nom'];
        //$typeDescription=$planning['plt_description'];

        $deOid="deO$pla_id";    $deO  =$planning['pla_deO'];
        $deJid="deJ$pla_id";    $deJ  =$planning['pla_deJ'];
        $aid="deb$pla_id";    $debut=$planning['pla_debut'];
        $fid="fin$pla_id";    $fin  =$planning['pla_fin'];

        $out.= '<div class="'.$trClasse.'">';

        // -- pla_id -- //
        //echo '<td style="display:none;">'.$pla_id.'</td>';

        // -- pla_type -- //
        $out.= "<select name='plaTypeEdit$pla_id'>";

        $o='';
        foreach($pt->get() as $_plt_id => $_type){
            $selected=($type==$_plt_id)?' selected="selected" class="selected"':'';
            $o.='<option value="'.$_plt_id.'" title="'.$_type['plt_description'].'"'.$selected.'> '.$_type['plt_nom'].'</option>';
        }
        $out.= $o.'</select>';

        // -- pla_debut  -- //
        $out.= '&nbsp;<input id="'.$aid.'" name="plaDebuEdit'.$pla_id.'" value="'.$debut.'"   length="22" maxlength="22" style="width:150px;"/>';

        // pla_fin -- //
        $out.= '&nbsp;<input id="'.$fid.'" name="plaFin_Edit'.$pla_id.'" value="'.$fin.'"    length="22" maxlength="22" style="width:150px;" />';

        $out.= '<br>';

        // -- description -- //
        $out.= 'O: '.
            IMG_EDITNC.' onclick=" inlineSwitch(\''.$deOid.'\')" />'.$deO
        .    '<span id="'.$deOid.'" style="display:none;"><input name="plaDeO_Edit'.$pla_id.'" length="50" maxlength="255" value="'.$deO.'" /></span>';

        $out.= '<br>';
        $out.= 'J: '.
            IMG_EDITNC.' onclick=" inlineSwitch(\''.$deJid.'\')" />'.$deJ
        .    '<span id="'.$deJid.'" style="display:none;"><input name="plaDeJ_Edit'.$pla_id.'" length="50" maxlength="255" value="'.$deJ.'" /></span>';
            
        $out.="</div>\n";
        //echo $out;
    }//foreach
  
$out.='<input name="plaNb" type="hidden" value="'.$Nb.'" />';
$out.='</form></div>';
return $out;
}//function planningShow()
?>
<a name="phaseAdd"></a>
<h2 class="h2">Ajouter une phase
<?php
echo '     <a href="?create=phaseNP&amp;'.ARIANE_ORGA.'#phaseAdd">Non programmé</a>';
echo ', de <a href="?create=phaseS&amp;'.ARIANE_ORGA.'#phaseAdd">SCENARIO</a>';
echo ', d\'<a href="?create=phaseA&amp;'.ARIANE_ORGA.'#phaseAdd">ACTION</a>';
echo ', de <a href="?create=phaseR&amp;'.ARIANE_ORGA.'#phaseAdd">RECHERCHE</a>';
echo ',    <a href="?create=phaseSP&amp;'.ARIANE_ORGA.'#phaseAdd">SPECIAL</a>';

//echo '<a href="?create=phase&amp;'.ARIANE_ORGA.'"></a>';
?>    
</h2>


<!-- planning non programmé -->
<h2 class="h2">plannings Non programmés ou nouvellement crées</h2>
<div>Les événements non programmés apparaissent aussi dans leurs plannings respectifs (à venir,en cours, terminés)</div>
<?php echo planningShow($plannings,$planningTypes,'NoP');?>


<!-- planning a venir -->
<h2 class="h2">plannings à venir</h2>
<?php echo planningShow($plannings,$planningTypes,'a_venir');?>


<!-- planning en cours -->
<h2 class="h2">plannings en cours</h2>
<?php echo planningShow($plannings,$planningTypes,'en_cours');?>


<!-- planning terminer -->
<h2 class="h2">plannings terminés <span class="pointeur" onclick=" inlineSwitch('formterminer');" />(Afficher/Cacher)</span></h2>
<?php echo planningShow($plannings,$planningTypes,'terminer');
