<?php
global $dbksfV3;

$reseaux= new gestTable('ksfv3',TBLPREFIXE.'reseaux','res_id');
//if(ISDEV===1)echo $reseaux->tableau();
?>
<a id="reseaux"></a>

<form method="POST" action="?<?php echo ARIANE_ORGA?>#reseaux">
<table><caption>reseaux (<a href="?create=reseau&amp;<?php echo ARIANE_ORGA?>#reseaux">ajouter</a> - <input type="submit" value="sauver" />)</caption>
<thead><tr><th style="display:none;">id</th><th>nom</th> <th>description</th> </thead></tr>
<tbody>
<?php
$Nb=0;
foreach($reseaux->get() as  $_reseau){
    $Nb++;
    $id=$_reseau['res_id'];
    $nid="reseauNom$id";        $nom=$_reseau['res_nom'];
    $did="reseauDescription$id";    $description=$_reseau['res_description'];

    echo '<tr><td style="display:none;">'.$id.'</td>'

    .    '<td>'.IMG_EDITNC.' onclick="inlineSwitch(\''.$nid.'\')" />'.$nom
    .    '<span id="'.$nid.'" style="display:none;">'
    .        '<input name="resNameEdit'.$id.'" type="text"  value="'.$nom.'" />'
    .    '</span></td>'

    .    '<td>'.IMG_EDITNC.' onclick="inlineSwitch(\''.$did.'\')" />'.$description
    .    '<span id="'.$did.'" style="display:none;">'
    .        '<input name="resDescEdit'.$id.'" type="text" value="'.$description.'" />'
    .    '</span></td>'

    .    '</tr>';
}
echo '</tbody></table>';
echo '<input name="resNb" type="hidden" value="'.$Nb.'" />';
echo '</form>';
unset($reseaux);


// - affichage des talents - //
$talents= new gestTable('ksfv3',TBLPREFIXE.'talents','tal_id');
//echo $talents->tableau();
//?>
<a id="talents"></a>

<form method="POST" action="?<?php echo ARIANE_ORGA?>#talents">
<table><caption>talents (<a href="?create=talent&amp;<?php echo ARIANE_ORGA?>#talents">ajouter</a> - <input type="submit" value="sauver" />)</caption>
<thead><tr><th style="display:none;">id</th><th style="min-width:1em;max-width:10em;">nom</th> <th>description</th> </thead></tr>
<tbody>
<?php
$Nb=0;
foreach($talents->get() as $talent){

    $Nb++;
    $id=$talent['tal_id'];
    $nid="talentNom$id";        $nom=$talent['tal_nom'];
    $did="talentDescription$id";    $description=$talent['tal_description'];


    echo '<tr><td style="display:none;">'.$id.'</td>'

    .    '<td style="min-width:1em;max-width:10em;">'.IMG_EDITNC.' onclick="inlineSwitch(\''.$nid.'\')" />'.$nom
    .    '<span id="'.$nid.'" style="display:none;">'
    .        '<input name="talNameEdit'.$id.'" type="text"  value="'.$nom.'" />'
    .    '</span></td>'

    .    '<td>'.IMG_EDITNC.' onclick="inlineSwitch(\''.$did.'\')" />'.$description
    .    '<span id="'.$did.'" style="display:none;">'
    .        '<input name="talDescEdit'.$id.'" type="text" value="'.$description.'" />'
    .    '</span></td>'

    .    '</tr>';
}
echo '</tbody></table>';
echo '<input name="talNb" type="hidden" value="'.$Nb.'" />';
echo '</form>';
unset($talents);


// - affichage des pouvoirs - //
$pouvoirs= new gestTable('ksfv3',TBLPREFIXE.'pouvoirs','pou_id');
//if (ISDEV===1)echo $pouvoirs->tableau();
?>
<a id="pouvoirs"></a>

<form method="POST" action="?<?php echo ARIANE_ORGA?>#pouvoirs">
<table><caption>pouvoirs (<a href="?create=pouvoir&amp;<?php echo ARIANE_ORGA?>#pouvoirs">ajouter</a> - <input type="submit" value="sauver" />)</caption>
<thead><tr><th style="display:none;">id</th><th>nom</th> <th>description</th></thead></tr>
<tbody>
<?php
$Nb=0;
foreach($pouvoirs->get() as $pouvoir){
    $Nb++;
    $id=$pouvoir['pou_id'];
    $nid="pouvoirNom$id";        $nom=$pouvoir['pou_nom'];
    $did="pouvoirDescription$id";    $description=$pouvoir['pou_description'];


    echo '<tr><td style="display:none;">'.$id.'</td>'

    .    '<td>'.IMG_EDITNC.' onclick="inlineSwitch(\''.$nid.'\')" />'.$nom
    .    '<span id="'.$nid.'" style="display:none;">'
    .        '<input name="pouNameEdit'.$id.'" type="text"  value="'.$nom.'" />'
    .    '</span></td>'

    .    '<td>'.IMG_EDITNC.' onclick="inlineSwitch(\''.$did.'\')" />'.$description
    .    '<span id="'.$did.'" style="display:none;">'
    .        '<textarea style="width:100%; min-height:2em;" name="pouDescEdit'.$id.'" type="text" value="'.$description.'">'.$description.' </textarea>'
    
    .    '</span></td>'

    .    '</tr>';
}
echo '</tbody></table>';
echo '<input name="pouNb" type="hidden" value="'.$Nb.'" />';
echo '</form>';
unset($pouvoirs);
