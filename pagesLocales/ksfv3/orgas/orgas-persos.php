<?php
global $per_etatTxt;
// - chargement des personnages - // 
// attention var $persos local
$persos=personnagesLoad(-1,1);

//$persos=new gestTable("ksfv3",TBLPREFIXE.'personnages','per_id',['ORDERBY' => 'per_id ASC', 'WHERE' => 'per_villeId = '.VILLEID,'clear'=>0]);
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($persos->dbTable->sql->getSQL()).'</div>';

// ************* //
// - affichage - //
// ************* //
?>
<div class="txtcenter">
personnages (<a href="?create=perso&amp;<?php echo ARIANE_ORGA?>">Ajouter</a>)<!--<br>
Vous ne voyez que les personnages affili&eacute;s &agrave; votre ville.-->
</div>
<?php
$lPaire=0;

foreach($persos->get() as   $per_id => $perso){
    $o='';
    if($lPaire===1){$trClasse='trP';$lPaire=0;}else{$trClasse='trI';$lPaire=1;}

    $nid='nom'.$per_id;    $nom=$perso['per_nom'];
    $pid='prenom'.$per_id; $prenom=$perso['per_prenom'];
    
    $per_etat=(int)$perso['per_etat']; //(0:inactif, 1:en jeu, 2:pause,3:mort)
//    $eid='etat'.$per_id;   //selection de l'etat du perso (0:inactif, 1:en jeu, 2:pause,3:mort)

    // - autorisation - //
    $isMes=$perso['per_isMes']==1?1:0;
    $isAct=$perso['per_isAct']==1?1:0;
    $isRec=$perso['per_isRec']==1?1:0;
    $isEch=$perso['per_isEch']==1?1:0;
    $isDec=$perso['per_isDec']==1?1:0;


    // - richesse - //
    $richessDispoVal=$perso['per_richesseDispo'];
    $richessDispoDec=($richessDispoVal>0)?  '<a href="?per_id='.$per_id.'&amp;edit=richesseDispoDec&amp;'.ARIANE_ORGA.'#'.$per_id.'"> - </a>':'';
    $richessDispoInc=($richessDispoVal<100)?'<a href="?per_id='.$per_id.'&amp;edit=richesseDispoInc&amp;'.ARIANE_ORGA.'#'.$per_id.'"> + </a>':'';

    $richessMaxVal    =$perso['per_richesseMax'];
    $richessMaxDec    =($richessMaxVal  >0)?'<a href="?per_id='.$per_id.'&amp;edit=richesseMaxDec&amp;'.ARIANE_ORGA.'#'.$per_id.'"> - </a>':'';
    $richessMaxInc    =($richessMaxVal<100)?'<a href="?per_id='.$per_id.'&amp;edit=richesseMaxInc&amp;'.ARIANE_ORGA.'#'.$per_id.'"> + </a>':'';

    // - proche - //
    $procheDispoVal    =$perso['per_procheDispo'];
    $procheDispoDec    =($procheDispoVal  >0)?'<a href="?per_id='.$per_id.'&amp;edit=procheDispoDec&amp;'.ARIANE_ORGA.'#'.$per_id.'"> - </a>':'';
    $procheDispoInc    =($procheDispoVal<100)?'<a href="?per_id='.$per_id.'&amp;edit=procheDispoInc&amp;'.ARIANE_ORGA.'#'.$per_id.'"> + </a>':'';

    $procheMaxVal    =$perso['per_procheMax'];
    $procheMaxDec    =($procheMaxVal   >0)?'<a href="?per_id='.$per_id.'&amp;edit=procheMaxDec&amp;'.ARIANE_ORGA.'#'.$per_id.'"> - </a>':'';
    $procheMaxInc    =($procheMaxVal    <100)?'<a href="?per_id='.$per_id.'&amp;edit=procheMaxInc&amp;'.ARIANE_ORGA.'#'.$per_id.'"> + </a>':'';

    // - talent - //
    $talentDispo=array();
    $talentDispo[1]=$perso['per_talent1Dispo'];    $talentDispo[2]=$perso['per_talent2Dispo'];    $talentDispo[3]=$perso['per_talent3Dispo'];
    $talentDispo[4]=$perso['per_talent4Dispo'];    $talentDispo[5]=$perso['per_talent5Dispo'];    $talentDispo[6]=$perso['per_talent6Dispo'];

    // - reseau - //
    $reseauDispo=array();
    $reseauDispo[1]=$perso['per_reseau1Dispo'];    $reseauDispo[2]=$perso['per_reseau2Dispo'];    $reseauDispo[3]=$perso['per_reseau3Dispo'];
    $reseauDispo[4]=$perso['per_reseau4Dispo'];    $reseauDispo[5]=$perso['per_reseau5Dispo'];    $reseauDispo[6]=$perso['per_reseau6Dispo'];

    // - affichage - //

    echo "<a name='$per_id'></a>";
    echo '<form method="POST" name="Fedit" action="?'.ARIANE_ORGA.'#'.$per_id.'">';
    echo "<div class='$trClasse per_etat$per_etat'>";

    echo '<input name="editPerNo" type="submit" value="sauver" />';
    echo '<input name="perNo" type="hidden" value="'.$per_id.'" />';
    
    // -- per_id -- //
    echo "Personnage n $per_id: ";
    
    // -- Nom Prenom -- //
    echo IMG_EDITNC.' onclick=" inlineSwitch(\''.$pid.'\')" />'.$prenom
    .    '<span id="'.$pid.'" style="display:none;">'
    .        '<input name="perPrenEdit" value="'.$prenom.'" />'
    .    '</span> '
    
    .    IMG_EDITNC.' onclick="inlineSwitch(\''.$nid.'\')" />'.$nom
    .    '<span id="'.$nid.'" style="display:none;">'
    .        '<input name="perNameEdit" value="'.$nom.'" />'
    .    '</span>';

    // -- Etat -- //
    //echo ' <span>Etat: '.$per_etat.' '.$per_etatTxt[$per_etat].'</span>';

    echo ' <select name="per_etat">';
    echo '<option disabled>&eacute;tat:</option>';
    echo selectOptionEtat($per_etat);
    echo '</select>';

    echo ' <a href="?orgaPersoNo='.$per_id.'&amp;ksfv3=perso-fiche">&eacute;diter la fiche.</a>';

    echo '<br>';

    echo '</form>';

    // - autorisation - //
    echo '<div>';
    echo'Autorisations:';
    $titleOui='Autoriser';
    $titleNon='Interdire';
    if($isMes===0){$oui='autorisationOf';$non='autorisationOn';}else{$oui='autorisationOn';$non='autorisationOf';}
    echo" Messagerie: <a href='?per_id=$per_id.&amp;edit=aut_isMesOn&amp;".ARIANE_ORGA."#$per_id' class='$oui' title='$titleOui'>Oui</a>|<a href='?per_id=$per_id.&amp;edit=aut_isMesOff&amp;".ARIANE_ORGA."#$per_id' class='$non' title='$titleNon'>Non</a>";
    if($isAct===0){$oui='autorisationOf';$non='autorisationOn';}else{$oui='autorisationOn';$non='autorisationOf';}
    echo" Action: <a href='?per_id=$per_id.&amp;edit=aut_isActOn&amp;".ARIANE_ORGA."#$per_id' class='$oui' title='$titleOui'>Oui</a>|<a href='?per_id=$per_id.&amp;edit=aut_isActOff&amp;".ARIANE_ORGA."#$per_id' class='$non' title='$titleNon'>Non</a>";
    if($isRec===0){$oui='autorisationOf';$non='autorisationOn';}else{$oui='autorisationOn';$non='autorisationOf';}
    echo" Recherche: <a href='?per_id=$per_id.&amp;edit=aut_isRecOn&amp;".ARIANE_ORGA."#$per_id' class='$oui' title='$titleOui'>Oui</a>|<a href='?per_id=$per_id.&amp;edit=aut_isRecOff&amp;".ARIANE_ORGA."#$per_id' class='$non' title='$titleNon'>Non</a>";
    if($isEch===0){$oui='autorisationOf';$non='autorisationOn';}else{$oui='autorisationOn';$non='autorisationOf';}
    echo" Echange: <a href='?per_id=$per_id.&amp;edit=aut_isEchOn&amp;".ARIANE_ORGA."#$per_id' class='$oui' title='$titleOui'>Oui</a>|<a href='?per_id=$per_id.&amp;edit=aut_isEchOff&amp;".ARIANE_ORGA."#$per_id' class='$non' title='$titleNon'>Non</a>";
    if($isDec===0){$oui='autorisationOf';$non='autorisationOn';}else{$oui='autorisationOn';$non='autorisationOf';}
    echo" Découverte: <a href='?per_id=$per_id.&amp;edit=aut_isDecOn&amp;".ARIANE_ORGA."#$per_id' class='$oui' title='$titleOui'>Oui</a>|<a href='?per_id=$per_id.&amp;edit=aut_isDecOff&amp;".ARIANE_ORGA."#$per_id' class='$non' title='$titleNon'>Non</a>";
    echo'</div>';

    // -- richesse -- //
    echo    '<div>Richesse: '
    .    "<span>$richessDispoDec $richessDispoVal $richessDispoInc</span> / "
    .    "<span>$richessMaxDec $richessMaxVal $richessMaxInc</span>"
    .    '';

    // -- proche -- //
    echo    'Proche: '
    .    "<span>$procheDispoDec $procheDispoVal $procheDispoInc</span> / "
    .    "<span>$procheMaxDec $procheMaxVal $procheMaxInc</span>"
    .    '</div>';
    
    // -- talents -- //
    echo '<div>Talents: ';
    $perTals=perTalsLoad($per_id);
    foreach($perTals as $talId => $perTal){echo $perTal['nom'].':'.$talentDispo[$talId].' ';}
    echo '</div>';
    
    // -- reseaux -- //
    echo '<div>R&eacute;seaux: ';
    $perRes=perResLoad($per_id);
    foreach($perRes as $resId =>$perRes ){echo $perRes['nom'].':'.$reseauDispo[$resId].' ';}
    echo '</div>';

    // -- pouvoirs -- //
    echo '<div>Pouvoirs: ';
    $perPous=perPouLoad($per_id);
    foreach($perPous as $perPou){
        $nomR =$perPou['nomR'];
        $nomP =$perPou['nomP'];
        $score=$perPou['score'];
        $note =$perPou['note'];
        echo "<b>$nomR</b>:$score $nomP <span style='overflow:auto;font-style : italic;'>$note</span>";
    }
    echo'</div>';
    echo'</div>';//<div class='$trClasse'>
}
