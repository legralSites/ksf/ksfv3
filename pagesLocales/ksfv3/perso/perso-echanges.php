<a name="echanges"></a>

<h1>&Eacute;CHANGER</h1>
<?php
global $gestLib,$dbksfV3,$persos;


// ============================================ //
// -  init des variables - //
// ============================================ //
if (PHASE_A_NB>0){
    echo '<div class="noteimportant">Les &eacute;changes sont interdites lors des phases d\'actions!</div>';
}
// - autoriser? - //
elseif(PER_ISECH === 0){
    echo '<div class="notewarning">Vous &ecirc;tes dans l\'impossibilit&eacute; de faire des &eacute;changes.</div>';
}
else{
    // - chargement du perso actif - //
    $perso=new gestLigne($dbksfV3,TBLPREFIXE.'personnages',PERSONO,'per_id');

    // - chargement  - //
    $talents=  new gestTable('ksfv3',TBLPREFIXE.'talents', 'tal_id');
    $reseaux=  new gestTable('ksfv3',TBLPREFIXE.'reseaux', 'res_id');

    $richesse=$proche=0;


if(isset($_POST['transactionAsk'])){

    $errNo=0;
    $logTxt='';

    // - on remplit le sac d'offrandes - //
    $fromId=PERSONO;
    $emprunteurId=$_POST['toId'];
    $sql=''; // les requetes sql cumulées

    // - lecture du POST - //

    $richesse=isset($_POST['richesse'])?$_POST['richesse']:0;
    $proche=isset($_POST['proche'])?$_POST['proche']:0;

    // - chargement du beneficiaire - //
    //$benef=new gestLigne($dbksfV3,TBLPREFIXE.'personnages',$emprunteurId,'per_id');

    // - RICHESSE - //
    if($richesse>0){
        // soustraction de la valeur au preteur //
        $sql.='UPDATE `'.TBLPREFIXE."personnages` SET per_richesseDispo = per_richesseDispo - $richesse WHERE per_id = $fromId;";

        // addition de la valeur a l'emprunteur //
        $sql.='UPDATE `'.TBLPREFIXE."personnages` SET per_richesseDispo = per_richesseDispo + $richesse WHERE per_id = $emprunteurId;";
        $logTxt.="richesse: $richesse ";
    }

    // - PROCHE - //
    if($proche>0){
        // soustraction de la valeur au preteur //
        $sql.='UPDATE `'.TBLPREFIXE."personnages` SET per_procheDispo = per_procheDispo - $proche WHERE    per_id = $fromId;";

        // addition de la valeur a l'emprunteur //
        $sql.='UPDATE `'.TBLPREFIXE."personnages` SET per_procheDispo = per_procheDispo + $proche WHERE    per_id = $emprunteurId;";
        $logTxt.="proche: $proche ";
    }

    // - TALENTS - //
    $disponibilites=array();
    $disponibilites['type']='talent';
    for($i=1;$i<=TALNB;$i++) $disponibilites[$i]=(isset($_POST["tal$i"]) AND $_POST["tal$i"]>0)?$_POST["tal$i"]:0;

    // -- ajout de la commande sql , dasn $sql, de la transation Talent/reseau -- //
    $errNo=transactionTalRes($perso,$emprunteurId,$disponibilites,$sql,$logTxt);
    if($errNo>0){
        echo "<div class='notewarning'>Erreur No: $errNo lors de la transaction!</div>";
        $errNo=0;
    }

    // - RESEAUX - //
    $disponibilites=array();
    $disponibilites['type']='reseau';
    for($i=1;$i<=TALNB;$i++) $disponibilites[$i]=(isset($_POST["res$i"]) AND $_POST["res$i"]>0)?$_POST["res$i"]:0;

    $errNo=transactionTalRes($perso,$emprunteurId,$disponibilites,$sql,$logTxt);
    if($errNo>0){
        echo "<div class='notewarning'>Erreur No: $errNo lors de la transaction!</div>";
        $errNo=0;
    }

    // - execution sql - //
    if($sql===''){
        echo '<div class="noteclassic">Votre offre est vide:</div>';
    }
    else{
        $dbksfV3->sql->clear();
        $dbksfV3->sql->setOPERATION($sql);
        $sql=$dbksfV3->query();
        $dbksfV3->queryClose();
        if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.$sql.'</div>';

        $benefNom=getPersoPN($emprunteurId);
        logAdd(PERSONO,  'perso'    ,0,"Vous avez offert &agrave; $benefNom: $logTxt");
        logAdd($emprunteurId,  'perso'    ,0,'Vous avez recut de '.getPersoNom().": $logTxt");

        // -- log -- //
        echo "<div class='noteclassic'>Vous avez offert à $benefNom:$logTxt</div>";
    }
}//if(isset($_POST['transactionAsk'])){



// ============================================ //
// - (re)chargement des tables - //
// ============================================ //
$perso=new gestLigne($dbksfV3,TBLPREFIXE.'personnages',PERSONO,'per_id');
if(ISDEV===1)echo '<div class="coding_code">$perso='.$perso->legralPDO->sql->getSQL().'</a></div>';
//echo $perso->tableau();

$perTals=perTalsLoad(PERSONO);
$perRes =perResLoad (PERSONO);


if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($persos->dbTable->sql->getSQL()).'</div>';
//if(ISDEV===1)    echo $persos->tableau();


// ======================== //
// - formulaire d'echange - //
// ======================== //
?>
<form METHOD="POST" ACTION="?<?php echo ARIANE_AGORIA?>">
<div class="echangeForm">
<?php

// - Beneficiaire - //

$o='<select class="echangeBeneficiaire" size="5" name="toId" required="required">';

foreach($persos->get() as $_dest_id => $_perso){
    if($_dest_id<=0)continue;    // exclure les MJ
    $selected='';
    $PN=$_perso['perPN'];
    $o.="<option value='$_dest_id' title='$PN'.$selected>$PN</option>\n";
}
echo $o.'</select>';
unset($o);
?>
<div id="echangeRPRT">
<!--h2>Richesses Et Proches</h2--><?php
// - afficher richesse et Proche - //
$limit=$perso->get('per_richesseDispo');

if($limit>0){
?>
<span class="Rrichesse">
<span class="RTitre">Richesse</span>
<select name="richesse" class="echangeSelect">
<option  selected="selected" value="0" />0</option>
<?php
for($i=1;$i<=$limit;$i++) echo"<option value='$i' />$i</option>";
?>
</select>
</span><br>
<?php }//if($limit>0){


$limit=$perso->get('per_procheDispo');

if($limit>0){
?><span class="Rproche">
<span class="RTitre">Proche</span>
<select name="proche" class="echangeSelect">
<option  selected="selected" value="0" />0</option>
<?php
for($i=1;$i<=$limit;$i++) echo"<option value='$i' />$i</option>";
?>    
</select>
</span>
<br><br>

<?php }//if($limit>0){

// - talents - //
for($i=1;$i<=TALNB;$i++){
    $dispo=$perso->get('per_talent'.$i.'Dispo');
    if($dispo>0){
        echo $talents->get($i,'tal_nom').':';
        echo"<select name='tal$i'>";
        for ($d=0;$d<=$dispo;$d++)echo "<option value='$d' />$d</option>";
        echo'</select><br>';
    }
}
echo '<br>';

// - Reseaux - //
for($i=1;$i<=RESNB;$i++){
    $dispo=$perso->get('per_reseau'.$i.'Dispo');
    if($dispo>0){
        echo $reseaux->get($i,'res_nom').':';
        echo"<select name='res$i'>";
        for ($d=0;$d<=$dispo;$d++)echo "<option value='$d' />$d</option>";
        echo'</select><br>';
    }
}
?>
</div><!--div id="echangeRPRT"-->


<input class="transactionAsk" type="submit" name="transactionAsk" value="Confirmé" />
</div><!--div class="echangeForm"-->
</form>
<?php
}// if(PER_ISECH === 0) else{
