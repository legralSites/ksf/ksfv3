<a name="decouvertes"></a>

<h1>VOS D&Eacute;COUVERTES</h1>
<?php

global $dbksfV3;
// - (re)chargement du personnage actif - //
$perso=new gestLigne($dbksfV3,TBLPREFIXE.'personnages',PERSONO,'per_id');
//if (ISDEV==1)$perso->tableau();

// - autoriser? - //
$isDec=$perso->get('per_isDec')==1?1:0;
if($isDec===0){
    echo '<div class="notewarning">Vous êtes dans l\'impossibilité de faire des découvertes.</div>';
}
else{
    $codeR=(isset($_GET['codeD'])AND !empty($_GET['codeD']) )?$_GET['codeD']:NULL;// c'est bien codeD (viens de *messagerie)
    if(isset($_POST['codeR'])AND !empty($_POST['codeR']))$codeR=$_POST['codeR'];
?>
<a name="recherche"></a><h2 class="<h2">RECHERCHER</h2>

<div class="txtcenter">
<form name="FRecherche" method="POST" action="?<?php echo ARIANE_AGORIA?>#recherche">
    <input  class="txtcenter" name="codeR" value="<?php echo $codeR?>" /><br>
    <input type="submit" name="sendCode" value="rechercher!" />
</form>
</div>
<?php
    $talents= new gestTable('ksfv3',TBLPREFIXE.'talents','tal_id',['clear'=>0]);
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($talents->dbTable->sql->getSQL()).'</div>';
    //if(ISDEV===1)echo $talents->tableau();

    $reseaux= new gestTable('ksfv3',TBLPREFIXE.'reseaux','res_id',['clear'=>0]);
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($reseaux->dbTable->sql->getSQL()).'</div>';
    //if(ISDEV===1)echo $reseaux->tableau();

    // -- Chargement de la liste de decouverte deja acquise -- //
    $perDec=perDecLoad(PERSONO);
    //if(ISDEV===1)echo gestLib_inspect('$perDec',$perDec);

if($codeR!==NULL){
    // - Chargement des artefacts - //

    //$gestLib->libs['gestTables']->setErr(LEGRALERR::ALL);

    $decouverte=new gestLigne($dbksfV3,TBLPREFIXE.'decouvertes',$codeR,'dec_code');
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">/*$decouverte*/'.ln2br($decouverte->legralPDO->sql->getSQL()).'</div>';
    //if (ISDEV==1)echo $decouverte->tableau();

    // - Le code correspond t-il a une recherche? - //
    if(empty($decouverte->get())){
        echo '<div class="noteclassic">Vous passer des heures à rechercher sans résultat!</div>';
    }
    else{

        // -- le personnage est il le destinataire de cette découverte-- //
        $destId=$decouverte->get('cdt_persoNo');

        if($destId!=NULL AND $destId!=PERSONO){//non
        echo '<div class="noteclassic">Cette découverte ne vous est pas destinée!</div>';
        }
        else{
            // -- Chargement de la liste de decouverte deja acquise -- //
            //$perDec=perDecLoad(PERSONO);

            // -- Le personnage possede t'il deja cette decouverte? -- //
            if(isset($perDec[$decouverte->get('dec_id')])){
                echo '<h2>Rappel du message découvert:</h2>';
                echo '<div class="noteclassic">'.$decouverte->get('dec_message').'</diV>';
            }
        
            // -- Le personnage NE possede PAS cette decouverte -- //
            else{
                echo '<div class="noteclassic">Votre recherche à été fructueuse !</div>';
                echo '<div id="decCondAchat">';
                $cdtManquante=0;
                $cdtTxt='';

                // -- verifications des conditions -- //

                // --- richesseDispo --- //
                //echo '<h2>Richesse neccessaires:</h2>';
                $richessePrix=$decouverte->get('cdt_richessePrix');
                if(empty($richessePrix))$richessePrix=0;
                if($richessePrix>0){
                    $dispo=$perso->get('per_richesseDispo');
                    $manque=$richessePrix-$dispo;
                    $m='';
                    if($manque>0){echo CDTFAIL_IMG;$m="manque:$manque";}else{echo CDTOK_IMG;}
                    if($manque>$dispo)$cdtManquante++;    // n'a pas les moyens de payer
                    echo " Richesse: $dispo/$richessePrix. $m <br>";
                }

                // --- procheDispo --- //
                //echo '<h2>Proche neccessaires:</h2>';
                $prochePrix=$decouverte->get('cdt_prochePrix');
                if(empty($prochePrix))$prochePrix=0;
                if($prochePrix>0){
                    $dispo=$perso->get('per_procheDispo');
                    $manque=$prochePrix-$dispo;
                    $m='';
                    if($manque>0){echo CDTFAIL_IMG;$m="manque:$manque";}else{echo CDTOK_IMG;}
                    if($manque>$dispo)$cdtManquante++;    // n'a pas les moyens de payer
                    echo " Proche: $dispo / $prochePrix. $m<br>";
                }
                unset($manque);

                $o='';
                // --- talents --- //
                //$o.='<h2>Talents neccessaires:</h2>';

                $talentsPrix=array();//valeur d'achat par talent
                for($i=1;$i<=TALNB;$i++){
                    $prix=$talentsPrix[$i]=$decouverte->get('cdt_tal'.$i);

                    // - indifferent? - //
                    if($prix==NULL) continue;

                    // - conditionner: - //
                    $talNom=$talents->get($i,'tal_nom');
                    $prix=$talentsPrix[$i]=$decouverte->get('cdt_tal'.$i);
                    $dispo=$perso->get('per_talent'.$i.'Dispo');

                    // - ce talent doit-il etre désactivé?- //
                    if($prix==-1 AND isset($perTal[$i])){
                        $cdtManquante++;
                        $o.=CDTFAIL_IMG." Votre talent $talNom vous empeche d'acquierir cette connaissance &agrave; tout jamais!";
                        $talentsPrix[$i]=NULL;unset($talentsPrix[$i]);
                    }
                    if($prix>=0){
                        if($dispo<$prix){
                            $cdtManquante++;
                            $manque=$prix-$dispo;
                            $o.=CDTFAIL_IMG. "$talNom: $prix. Il vous manque: $manque";
                            $talentsPrix[$i]=NULL;unset($talentsPrix[$i]);
                        }
                        else{$o.=CDTOK_IMG. "$talNom: $prix";}
                    }
                $o.= '<br>';
                }


                // --- reseaux --- //
                //$o.='<h2>R&eacute;seaux neccessaires:</h2>';

                $reseauxPrix=array();//valeur d'achat par reseau
                for($i=1;$i<=RESNB;$i++){
                    $prix=$reseauxPrix[$i]=$decouverte->get('cdt_res'.$i);

                    // - indifferent? - //
                    if($prix==NULL) continue;

                    // - conditionner: - //
                    $resNom=$reseaux->get($i,'res_nom');
                    $prix=$reseauxPrix[$i]=$decouverte->get('cdt_res'.$i);
                    $dispo=$perso->get('per_reseau'.$i.'Dispo');

                    // - ce talent doit-il etre désactivé?- //
                    if($prix==-1 AND isset($perTal[$i])){
                        $cdtManquante++;
                        $o.=CDTFAIL_IMG." Votre talent $talNom vous empeche d'acquierir cette connaissance à tout jamais!<br>";

                        $reseauxPrix[$i]=NULL;unset($reseauxPrix[$i]);
                    }
                    if($prix>=0){
                        if($dispo<$prix){
                            $cdtManquante++;
                            $manque=$prix-$dispo;
                            $o.=CDTFAIL_IMG. "$resNom: $prix. Il vous manque: $manque<br>";
                            $reseauxPrix[$i]=NULL;unset($reseauxPrix[$i]);
                        }
                        else{$o.=CDTOK_IMG. "$resNom: $prix<br>";}
                    }
                $o.= '<br>';
                }
                $o.='<br>';
                echo $o;unset($o);

                // -- Si il ne manque pas de condition-- //
                if($cdtManquante===0){

                    $tal1Prix=isset($talentsPrix[1])?$talentsPrix[1]:0;
                    $tal2Prix=isset($talentsPrix[2])?$talentsPrix[2]:0;
                    $tal3Prix=isset($talentsPrix[3])?$talentsPrix[3]:0;
                    $tal4Prix=isset($talentsPrix[4])?$talentsPrix[4]:0;
                    $tal5Prix=isset($talentsPrix[5])?$talentsPrix[5]:0;
                    $tal6Prix=isset($talentsPrix[6])?$talentsPrix[6]:0;

                    $res1Prix=isset($reseauxPrix[1])?$reseauxPrix[1]:0;
                    $res2Prix=isset($reseauxPrix[2])?$reseauxPrix[2]:0;
                    $res3Prix=isset($reseauxPrix[3])?$reseauxPrix[3]:0;
                    $res4Prix=isset($reseauxPrix[4])?$reseauxPrix[4]:0;
                    $res5Prix=isset($reseauxPrix[5])?$reseauxPrix[5]:0;
                    $res6Prix=isset($reseauxPrix[6])?$reseauxPrix[6]:0;

                    // --- reception de confirmation d'achat en richesse et/ou proche --- //
                    if(isset($_POST['acheter'])){
                        echo'<h2 class="h2">achat:</h2>';

                        // ---- AJOUT de la decouverte dans la liste des decouvertes du personnaga ---- //
                        $decId=$decouverte->get('dec_id');
                        $dbksfV3->sql->clear();
                        $sql='';
                        $sql.='UPDATE '.TBLPREFIXE.'personnages ';
                        $sql.='SET ';
                        $sql.="\n";
                        $sql.=" per_richesseDispo = per_richesseDispo -$richessePrix,";
                        $sql.=" per_procheDispo = per_procheDispo - $prochePrix,";
                        $sql.=" per_talent1Dispo= per_talent1Dispo-$tal1Prix,";
                        $sql.=" per_talent2Dispo= per_talent2Dispo-$tal2Prix,";
                        $sql.=" per_talent3Dispo= per_talent3Dispo-$tal3Prix,";
                        $sql.=" per_talent4Dispo= per_talent4Dispo-$tal4Prix,";
                        $sql.=" per_talent5Dispo= per_talent5Dispo-$tal5Prix,";
                        $sql.=" per_talent6Dispo= per_talent6Dispo-$tal6Prix,";
                        $sql.="\n";
                       $sql.=" per_reseau1Dispo= per_reseau1Dispo-$res1Prix,";
                        $sql.=" per_reseau2Dispo= per_reseau2Dispo-$res2Prix,";
                        $sql.=" per_reseau3Dispo= per_reseau3Dispo-$res3Prix,";
                        $sql.=" per_reseau4Dispo= per_reseau4Dispo-$res4Prix,";
                        $sql.=" per_reseau5Dispo= per_reseau5Dispo-$res5Prix,";
                        $sql.=" per_reseau6Dispo= per_reseau6Dispo-$res6Prix";
                        $sql.="\n";
                        $sql.=' WHERE per_id = '.PERSONO.";\n";
                        $sql.='INSERT INTO '.TBLPREFIXE.'perDec (perId, decId ) VALUES ( '.PERSONO.", $decId);";
                        $dbksfV3->sql->setOperation($sql);
                        $sql=$dbksfV3->query();
                        if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
                        
                        // ----  log ---- //
                        // ecrire dans le journal l'achat de la decouverte
                        logAdd(PERSONO,  'perso'    ,218,$decouverte->get('dec_code'));

                        // --- rechargement des numero de decouverte du personnage (pour affichage du bas)--- //
                        $perDec=perDecLoad(PERSONO);
                        }
                    else{    // --- sinon on fait un form de confirmation d'achat en richesse et/ou proche --- //
                        echo '<form method="POST" action="?'.ARIANE_AGORIA.'">';
                        echo '<div class="noteclassic">Voulez vous payer:<br>';
    
                        // - richesse/proche - //
                        echo "$richessePrix en RICHESSE et $prochePrix en PROCHE<br>";

                        // - talents- //
                        $o='';
                        //if(ISDEV)$o.= gestLib_inspect('$talentsPrix',$talentsPrix);
                        foreach($talentsPrix as $talId => $prix){
                            if($prix>0){
                                $nom=$talents->get($talId,'tal_nom');
                                $o.= "$nom:$prix.<br>";
                            }
                        }

                        // - Reseaux- //
                        $o.='';
                        //if(ISDEV)$o.= gestLib_inspect('$reseauxPrix',$reseauxPrix);
                        foreach($reseauxPrix as $resId => $prix){
                            if($prix>0){
                                $nom=$reseaux->get($resId,'res_nom');
                                $o.= "$nom:$prix.<br>";
                            }
                        }
                        echo "$o";
                        echo '<br>';
                        echo '<input type="hidden" name="codeR" value="'.$codeR.'" />';
                        echo '<input type="submit" name="acheter" value="Acheter" />';
                        echo'</div></form>';
                    }
                }
                echo'</div>';//<div id="decCondAchat">
            }//else{// -- Le personnage NE possede PAS cette decouverte -- //
        }//if($dest===NULL OR $dest=PERSONO){    
    }//if(empty($decouverte->get()))
}//if($codeR!==NULL)
}//if($isDec===0)
?>

<h2 class="pointeur" onclick="blockSwitch('decFindBox');">CE QUE VOUS AVEZ D&Eacute;J&Agrave; D&Eacute;COUVERT</h2>
<!--div id="decFindBox" style="display:<?php echo $codeR===NULL?'none':'block'?>;"-->
<div id="decFindBox" style="display:block">
<?php
/*
 * A REFAIRE */
// - Afficher les decouvertes - //
$decouvertes=  new gestTable('ksfv3',TBLPREFIXE.'decouvertes', 'dec_id',[
//    'SELECT' =>'*,'
//    .    'DATE_FORMAT(ts,"%d/%m/%Y %H:%m:%s") AS tsf'
//    ,'JOIN'   => 'INNER JOIN '.TBLPREFIXE.'perDec ON cdt_persoNo = perId'
//    ,'WHERE'  =>'perId='.PERSONO
//    ,'ORDERBY' =>'ts DESC'
    'clear'  => 0
    ]
);
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($decouvertes->dbTable->sql->getSQL()).'</div>';
//if(ISDEV===1)echo $decouvertes->tableau();


foreach($decouvertes->get() AS $dec_id => $decFind){
    if(isset($perDec[$dec_id])){
        //echo gestLib_inspect('$decFind',$decFind);
        //$ts=$decFind['ts'];
        //$tsf=$decFind['tsf'];
        $code=$decFind['dec_code'];
         $msg=ln2br($decFind['dec_message']);
        $coutTxt='';
        $cdt=$decFind['cdt_richessePrix'];if($cdt>0)$coutTxt.=" Richesse: $cdt.";
        $cdt=$decFind['cdt_prochePrix'];  if($cdt>0)$coutTxt.=" Proche: $cdt.";
        $selectCSS=$code==$codeR?' decFind_select':'';
        for($i=1;$i<=TALNB;$i++){$cdt=$decFind['cdt_tal'.$i];if($cdt>0)$coutTxt.=$talents->get($i,'tal_nom').": $cdt.";}
        for($i=1;$i<=RESNB;$i++){$cdt=$decFind['cdt_res'.$i];if($cdt>0)$coutTxt.=$reseaux->get($i,'res_nom').": $cdt.";}
        if($coutTxt=='')$coutTxt='gratuit!';

        echo "<a name='codeD$code'></a>";
        echo "<div class='decFind$selectCSS'>";
        //echo "<div><span class='decFindCodeD'>$code</span>. <!--D&eacute;couvert le: <span class=''>$ts</span>.-->Co&ucirc;t: $coutTxt</div>";
        echo "<div><span class='decFindCodeD'>$code</span>. Co&ucirc;t: $coutTxt</div>";
        echo "<div class='decFindMsg'>$msg</div>";
        echo '</div>';
    }
}
echo '</div>';
