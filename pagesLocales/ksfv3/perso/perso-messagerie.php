<?php
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_File"></div>';
?>
<script>
function showCountCar(TEXTAREAid,SPANidHTML,max){
	//count=document.getElementById(TEXTAREAidHTML).value.length;
	spanidCSS=document.getElementById(SPANidHTML);
	spanidCSS.innerHTML=TEXTAREAid.value.length;
	if(TEXTAREAid.value.length>max){
		spanidCSS.innerHTML+='<span style="font-weight:bold;color:red;">(MAX ATTEINT)</span>';
		TEXTAREAid.value=TEXTAREAid.value.substring(0,max);
	}
//	if(TEXTAREAid.value.length)idCSS.addClass
}
</script>
<?php
global $dbksfV3;


// ============================================ //
// - chargement des tables - //
// ============================================ //

// ajouter la possibilité aux MJ de se parlent entre eux (per_id<1)
$showJS=(ISMJ===1)?' OR per_id<1':' AND per_id>0';// afficher les MJ et les perso de la ville du MJ, OU, QUE les personnages
$persos=new gestTable("ksfv3",TBLPREFIXE.'personnages','per_id',
    [    'SELECT'   => 'per_id,per_isMes,per_isAct,per_isRec,CONCAT (UCASE(per_nom)," ",per_prenom) AS perPN'
        ,'WHERE'    => 'per_etat =1 AND per_villeId = '. PER_VILLEID.$showJS
        ,'ORDERBY'  => 'per_nom ASC,per_nom ASC'
        ,'clear'    => 0
    ]);
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($persos->dbTable->sql->getSQL()).'</div>';
//if(ISDEV===1)	echo $persos->tableau();

// - autoriser? - //
$isMes=$persos->get(PERSONO,'per_isMes')==1?1:0;
$isAct=$persos->get(PERSONO,'per_isAct')==1?1:0;
$isRec=$persos->get(PERSONO,'per_isRec')==1?1:0;


// ============================================ //
// - envoyer le message au(x) destinataire(s) - //
// ============================================ //
$forceForm=msgSend();


// ===================================== //
// = formulaire d'envoie ou de reponse = //
// ===================================== //
msgForm($persos);


// =============== //
// = phase d'A/R = //
// = formulaires = //
// =============== //
function formPhase($phaseType){
	$tag='Action';	if ($phaseType == 4)$tag='Recherche';
	$TAG='ACTION';	if ($phaseType == 4)$TAG='RECHERCHE';
	$phaseNb=PHASE_A_NB;	if ($phaseType == 4)$phaseNb=PHASE_R_NB;
	$carMAX=ACTIONCARMAX;	if ($phaseType == 4)$carMAX=RECHERCHECARMAX;

	// - chargement de toutes les phases (A ou R) en cours - //
	$phases=new gestTable('ksfv3',TBLPREFIXE.'planning','pla_id'
	,[
		'SELECT'=> '*'
		,'WHERE' => 'pla_villeId='.PER_VILLEID.' AND pla_type ='.$phaseType.' AND `pla_debut` <= NOW() AND  NOW() <= `pla_fin` '
//		,'JOIN'=>'JOIN '.TBLPREFIXE.'planningType ON '.TBLPREFIXE.'planning.pla_type = '.TBLPREFIXE.'planningType.plt_id'
		,'ORDERBY'=>'pla_debut'
		,'clear'  => 0
	]
);
	if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($phases->dbTable->sql->getSQL()).'</div>';
	//echo $phases->tableau();

	echo"<a name='$tag'></a><h2>$TAG</h2>";

	// - deja effectuer ? - //
	$phaseCpt=perso_phaseCpt($phaseType);//Nb de phase (selon type )fait PAR le personnage

	$dispo=$phaseNb - $phaseCpt;

	if($dispo<1)    {echo "<div class=''>Vous n'avez plus de $tag disponible.</div>";}
	elseif($dispo<2){echo "Il vous reste $dispo $tag<br>";}
	else		{echo "Il vous reste $dispo actions sur $phaseNb $tag".'s<br>';}

	if($dispo>0){	
		echo "Quelle est votre $tag?<br>";

		echo '<form method="POST" action="?'.ARIANE_AGORIA.'#'.$tag.'">';

		//<!--destinataire(s): MJ<br-->
		echo'<input type="hidden" name="MJID" value="'.MJID.'" />';

		// - Choix de l'action - //
		echo'<input type="hidden" name="phaseType" value="'.$phaseType.'" />';
		echo 'Sujet: ';

//		if(PHASE_A_NB==1){
//			$deJ=$phase['pla_deJ'];
//			if(empty($deJ))$deJ='ACTION du '.date('d/m/Y H:i');// si pas de description pour les joueurs alors on met la date et heure du jours
//		}
//		else
//		if(PHASE_A_NB>1){
			echo '<select name="pha_id">';
			foreach($phases->get() as $pha_id => $phase){
				$deJ=$phase['pla_deJ'];
				if(empty($deJ))$deJ="$TAG du ".date('d/m/Y H:i');// si pas de description pour les joueurs alors on met la date et heure du jours
				echo "<option value='$pha_id'>$deJ</option>";
				//echo "$deJ<br>";
			}
			echo '</select><br>';
//		}

//<!--
//		sujet: <input      name="sujet"   required="required" size="32"  maxlenght="32" readonly="readonly" style="background-color:transparent;font-weight:bold"
//			value="ACTION DU echo date('D  d/m/Y H:i');"/><br>
//-->
		echo 'message (<span id="msgAPhase_Counter">0</span>/'.$carMAX.')(<a href="http://www.compteurs.info/caracteres.html" target="compteCar">compteur externe de caractères</a>) ';
		$span="'msgAPhase_Counter'";
		echo '<textarea onkeyup="showCountCar(this,'.$span.','.$carMAX.');"  name="message" required="required" class="msg_typeA" maxlenght="'.$carMAX.'"></textarea>';

		echo'<input type="submit" name="sendPhaseAR" value="Go!" title="Envoyer votre message. Attention ne peut être annulé!" />';
		echo'</form>';

	}//if($dispo>0){

//}//if(PHASE_A_NB>0){
}// function formPhase($phaseType)



// ================== //
// = phase d'A/R = //
// ================== //
if(PHASE_A_NB>0){if(PER_ISACT===0)echo '<div class="notewarning">Vous êtes dans l\'incapacité de faire des actions.</div>';else formPhase(3);}
if(PHASE_R_NB>0){if(PER_ISREC===0)echo '<div class="notewarning">Vous êtes dans l\'incapacité de faire des recherches.</div>';else formPhase(4);}


// ============== //
// = messagerie = //
// =  (PERSO)   = //
// ============== //
msgFiltreForm('ksfv3=perso-messagerie');
$where='';
if(MSGFILTREN===1)$where.=' msg_type=""';
if(MSGFILTREA===1){$and=$where===''?'':' OR ';$where.=$and.' msg_type="A"';}
if(MSGFILTRER===1){$and=$where===''?'':' OR ';$where.=$and.' msg_type="R"';}
if(MSGFILTREH===1){$and=$where===''?'':' OR ';$where.=$and.' msg_type="H"';}

if($where===''){
	echo '<div class="txtcenter">Aucun message.</div>';
}
else{
$where='(msg_de_perId='.PERSONO. ' OR msg_pour_perId='.PERSONO.") AND ($where)";

$perDec=perDecLoad(PERSONO);

// --- chargement des mails --- //
$mailsPerso=new gestTable('ksfv3',TBLPREFIXE.'messageries','msg_id',
	[
		'SELECT' => 'msg_id'
		.	',DATE_FORMAT(msg_ts,"%d/%m/%Y %H:%i:%s") AS tsf,DATE_FORMAT(msg_ts,"%j") AS tsj'
		.	',msg_type,msg_de_perId,msg_de_txt,msg_pour_perId, CONCAT(per_prenom," ",per_nom) AS dest_perPN'
		.	',msg_codeD,msg_sujet,msg_message'
		.	',dec_id,dec_message'
	,	'JOIN'   => 'INNER JOIN '.TBLPREFIXE.'personnages ON msg_pour_perId =  per_id'
		.	' LEFT JOIN '.TBLPREFIXE.'decouvertes ON dec_code=msg_codeD'
	,	'WHERE'  => $where
	,	'ORDERBY'  => 'msg_ts DESC'
//	,	'LIMIT'  => 50 
	,	'clear'  => 0
]);
//if(ISDEV===1)echo '<div class="coding_code">$mailsPerso='.$mailsPerso->dbTable->sql->getSQL().'</a></div>';
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($mailsPerso->dbTable->sql->getSQL()).'</div>';
//if(ISDEV===1)echo gestLib_inspect('$mailsPerso->dbTable->sql',$mailsPerso->dbTable->sql);
//if(ISDEV===1)echo $mailsPerso->tableau();

$o=(ISMJ===1)?' <span class="orgas"><a href="?orgas=orgas-messagerie">(Aller à la messagerie orgas)</a></span><br>':'';
echo "<h2>MESSAGERIE$o</h2>";
echo'<div id="mailPerso">';
$tsjOld=0;
foreach($mailsPerso->get() as $msg_id => $msg){
	$id=$msg['msg_id'];

	$ts          =$msg['tsf'];
	$tsj	     =$msg['tsj'];// no du jours dans l'annee 01..366
	$tsjCSS=($tsjOld!==$tsj)?' msg_tsNewJ':'';
	$tsjOld=$tsj;

	$type        =$msg['msg_type'];
	$msgCSS      ="msg_type$type";

	$codeD	     =$msg['msg_codeD'];
	$dec_id      =$msg['dec_id'];
	$dec_message =ln2br($msg['dec_message']);

	$emmeteurId  =$msg['msg_de_perId'];
	$emmeteurPN  =$persos->get($emmeteurId,'perPN');
	//$emmeteurTxt =$msg['msg_de_txt'];
	//$from        =$emmeteurTxt!=''?$emmeteurTxt:$emmeteurPN;
	$from        =$emmeteurId>0?$emmeteurPN:'MJ';
 
	$destId	     =$msg['msg_pour_perId'];
	$destPN      =$msg['dest_perPN'];
	$sujet       =$msg['msg_sujet'];
	$message     =ln2br($msg['msg_message']);

	echo "<div class='$msgCSS'>";

	echo"<span class='msg_ts$tsjCSS'>$ts</span>";


	$repondre='';
	if($emmeteurId!==PERSONO){
		echo " DE: <span class='msg_from'>$from</span> ";
		$href ='send=1&destId='.$emmeteurId;
		$href.='&sujet='.urlencode('réponse de '.$sujet);
		$repondre='<a href="?'.htmlentities($href).'&amp;'.ARIANE_AGORIA.'" title="répondre">@</a>';
	}
	if($destId!==PERSONO){
		echo " POUR:<span class='msg_to'>$destPN</span> ";
		$href ='send=1&destId='.$destId;
		$href.='&sujet='.urlencode('ajout à '.$sujet);
		$repondre='<a href="?'.htmlentities($href).'&amp;'.ARIANE_AGORIA.'" title="complété avec un nouveau message">@</a>';
	}


//	if( PERSONO>0 AND  ($emmeteurId<=0 OR $destId<=0 OR $emmeteurTxt!=''))$repondre='';// LES PERSOS (non MJ) ne peuvent repondrent au MJ! ni a une lettre anonyme
	if( PERSONO>0 AND  ($emmeteurId<=0 OR $destId<=0))$repondre='';// LES PERSOS (non MJ) ne peuvent repondrent au MJ! ni a une lettre anonyme
	echo "<span class='msg_sujet'>$sujet</span>";
	echo "<div class='msg_message'>$repondre$message";
	if($codeD!=NULL){
		echo '<div class="msg_codeD"><a href="?codeD='.$codeD.'&amp;ksfv3=perso-decouvertes#codeD'.$codeD.'" title="">'.$codeD.'</a></div>';
		if($dec_message!=NULL){
			//afficher le msg uniquement si acheté
			$o='';
			if(isset($perDec[$dec_id])){
				$o=$dec_message;
			}
			else{
				$o="Vous n'avez pas acheté cette découverte.";
			}
			echo '<div class="msg_codeDMsg">'.$o.'</div>';
			unset($o);
		}
	}

	echo '</div>';//<div class='msg_message'>

	echo '</div>';//<div class='$msgCSS'>
}

echo'</div><!--div id="mailPerso" -->';
}//if($where==='') else
//}//if($isMes===0){