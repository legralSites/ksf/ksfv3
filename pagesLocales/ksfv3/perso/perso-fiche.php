<?php
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_File"></div>';

if(defined('PERSONO')){

global $perso;
global $dbksfV3;


$villes= new gestTable('ksfv3',TBLPREFIXE.'villes','vil_id',[ "ORDERBY" => "`vil_nom` ASC",'clear'=>0]);
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($villes->dbTable->sql->getSQL()).'</div>';
//echo $villes->tableau();

// - chargement des descriptions pour les joueurs des items - //
$itemsDescr= new gestTable('ksfv3',TBLPREFIXE.'itemsDescriptions','nom',[ "SELECT" => "`nom`,`diJ`", "ORDERBY" => "`nom` ASC",'clear'=>0]);
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($itemsDescr->dbTable->sql->getSQL()).'</div>';
//echo $itemsDescr->tableau();


$perId=(int)$perso->get('per_id');
$perBGFileName=$perso->get('per_BGFileName');
//$persoNom=$perso->get('per_nom');    $persoPrenom=$perso->get('per_prenom');

$perEtat=(int)$perso->get('per_etat');

// - civilite - //
//$perDateN=$perso->get('per_dateN');    $perSexe=$perso->get('per_sexe');
//$perAdresse=$perso->get('per_adresse');    $perVilleId=$perso->get('per_villeId');
//$perOsmNode=$perso->get('per_osm_node');
//$perMsgCode=$perso->get('per_msg_code');//droit d'accée en écriture à la messagerie (pour les autres perso)

// - note - //
$perNote=$perso->get('per_note');

// - HTML - //
echo '<div class="txtcenter">';
//if(ISMJ===1)echo '<div class="orgas">Aller dans la liste <a href="?ksfv3=orgas&orgas=orgas-persos">personnages</a></div>';
echo '<h1>'.strtoupper(PER_PRENOM.' '.PER_NOM).'</h1>';
if(!empty($perBGFileName))echo'    <a href="'.BG_ROOT.$perBGFileName.'"><img src="'.TOPSECRET_SRC.'" /></a>';
echo'</div>';
?>
<!--
<div id="perso_civilite">
<?php
/*
if(ISMJ===1){
    echo '<div class="orgas"><form method="POST" action="?'.ARIANE_AGORIA.'#civilite">';
    echo '<input type="submit" name="perCiviliteEdit" value="sauver" /><br>';
    echo "n&eacute;(e) le <input name='perDateN' value='$perDateN'/><br>";
    echo "sexe: <input name='perSexe' value='$perSexe'/> (1 caractère: M/F/T/A/?) <br>";
    //echo 'adresse: <input name="perAdresse" value="'.str_replace('"','&quot;',$perAdresse).'"/><br>';
    echo 'adresse: <input name="perAdresse" value="'.htmlentities($perAdresse).'"/>';
//    echo 'ville:<input name="perVille" value="'.htmlentities($perVille).'"/><br>';
    echo '<select name="perVilleId">';
    $o='';
    foreach ($i=$villes->get() as $_id => $_ville){
        $selected=($_id==$perVilleId)?' selected="selected" class="selected"':'';
        $o.='<option value="'.$_id.'" title="'.$_ville['vil_description'].'"'.$selected.'> '.$_ville['vil_nom'].'</option>'."\n";
    }
    echo $o.'</select><br>';
    echo "osm-node: <input name='perOsmNode' value='$perOsmNode'/><br>";
    echo "msg-code: <input name='perMsgCode' value='$perMsgCode'/><br>";
    echo'</form></div>';
}
if($perDateN!==NULL)echo "n&eacute;(e) le $perDateN<br>";
if($perSexe!==NULL)echo "sexe: $perSexe<br>";
if($perAdresse!==NULL)echo "Adresse: $perAdresse";
//if($perVilleId!==NULL)echo ' '.$villes->get($perVilleId,'vil_nom');
if($perOsmNode!==NULL)echo ' <a href="http://www.openstreetmap.org/node/'.$perOsmNode.'" target="osm">carte</a>';
echo "<br>";
//echo "code messagerie: $perMsgCode <span class='itemDescription'>".$itemsDescr->get('msgCode','diJ').'</span>';
 */
?>
</div>
-->

<!--
<?php

// ========== //
// = PHASES = //
// ========== //

// - Phase ACTION - //
if(PER_ISACT===0){
    echo '<div class="notewarning">Vous $etes dans l\'impossibilité de faire une action</div>';
}
elseif(PHASE_A_NB==1){
    echo '<div class="phaseActionBox"><a name="action"></a><h2 class="<h2">Phase d\'ACTION</h2>';
    $actionNb=perso_phaseCpt(3);
    //echo gestLib_inspect('(PHASE_A_NB==1):$actionNb',$actionNb);
    if($actionNb<=0){echo 'Pour faire votre ACTION aller sur votre <a href="?ksfv3=perso-messagerie#Action">messagerie</a>.';}
    else        {echo 'Vous avez d&eacute;j&agrave; fait votre ACTION.';}
    echo '</div>';
}
elseif(PHASE_A_NB>1){
    echo '<div class="phaseActionBox"><a name="action"></a><h2 class="<h2">Phase d\'ACTION</h2>';
    $actionNb=perso_phaseCpt(3);
    $actionDispo=PHASE_A_NB-$actionNb;

    switch($actionDispo){
        case 0: echo 'Vous avez d&eacute;j&agrave; fait vos '.PHASE_A_NB.' ACTIONS.';break;
        case 1: echo 'Pour faire votre ACTION aller sur votre <a href="?ksfv3=perso-messagerie#Action">messagerie</a>.';break;
        default: //$actionDispo>1
            echo 'Pour faire vos '.$actionDispo.' ACTIONS aller sur votre <a href="?ksfv3=perso-messagerie#Action">messagerie</a>.';
    }
}



// - Phase RECHERCHE - //
if(PER_ISREC===0){
    echo '<div class="notewarning">Vous $etes dans l\'impossibilité de faire une recherche.</div>';
}
elseif(PHASE_R_NB>0){
    echo'<div class="phaseRechercheBox"><a name="recherche"></a><h2 class="<h2">Phase de RECHERCHE</h2>';
    echo'Vous pouvez faire des <a href="?ksfv3=perso-recherches">recherches</a></div>';
}
?>
--><!-- // A/R  -->

<?php

// - richesse - //
$richesseDispoVal    =$perso->get('per_richesseDispo');
$richesseDispoDec    =($richesseDispoVal      >0)  ?'<a href="?per_id='.PERSONO.'&amp;edit=richesseDispoDec&amp;'.  ARIANE_AGORIA.'#orgas-richesseProche"> - </a>':'';
$richesseDispoInc    =($richesseDispoVal      <100)?'<a href="?per_id='.PERSONO.'&amp;edit=richesseDispoInc&amp;'.  ARIANE_AGORIA.'#orgas-richesseProche"> + </a>':'';

$richesseMaxVal        =$perso->get('per_richesseMax');
$richesseMaxDec        =($richesseMaxVal      >0)  ?'<a href="?per_id='.PERSONO.'&amp;edit=richesseMaxDec&amp;'.  ARIANE_AGORIA.'#orgas-richesseProche"> - </a>':'';
$richesseMaxInc        =($richesseMaxVal      <100)?'<a href="?per_id='.PERSONO.'&amp;edit=richesseMaxInc&amp;'.  ARIANE_AGORIA.'#orgas-richesseProche"> + </a>':'';
 

// - proche - //
$procheDispoVal    =$perso->get('per_procheDispo');
$procheDispoDec    =($procheDispoVal      >0)  ?'<a href="?per_id='.PERSONO.'&amp;edit=procheDispoDec&amp;'.  ARIANE_AGORIA.'#orgas-richesseProche"> - </a>':'';
$procheDispoInc    =($procheDispoVal      <100)?'<a href="?per_id='.PERSONO.'&amp;edit=procheDispoInc&amp;'.  ARIANE_AGORIA.'#orgas-richesseProche"> + </a>':'';

$procheMaxVal    =$perso->get('per_procheMax');
$procheMaxDec    =($procheMaxVal      >0)  ?'<a href="?per_id='.PERSONO.'&amp;edit=procheMaxDec&amp;'.  ARIANE_AGORIA.'#orgas-richesseProche"> - </a>':'';
$procheMaxInc    =($procheMaxVal      <100)?'<a href="?per_id='.PERSONO.'&amp;edit=procheMaxInc&amp;'.  ARIANE_AGORIA.'#orgas-richesseProche"> + </a>':'';


//echo '<span class="itemDescription">'.$itemsDescr->get('talent','diJ').'</span><br>';
$talents=new gestTable("ksfv3",TBLPREFIXE.'talents','tal_id');
$perTals=perTalsLoad(PERSONO);
$perTalNb    =count($perTals);
define('TAL_UNSETABLE',(ISORGA==1 AND $perTalNb > 0));    // les talents sont désactivables ?// reponse TRUE|FALSE


//echo '<span class="itemDescription">'.$itemsDescr->get('reseau','diJ').'</span><br>';
$reseaux=new gestTable("ksfv3",TBLPREFIXE.'reseaux','res_id');
$perRes=perResLoad(PERSONO);
$perResNb    =count($perRes);
define('RES_UNSETABLE',(ISORGA==1 AND $perResNb > 0));    // les reseaux sont désactivables ?// reponse TRUE|FALSE
?>


<!-- * vue perso * -->

<?php
// =========== //
// = POUVOIRS  //
// =========== //
$perPou=perPouLoad(PERSONO);
$perPouNb=count($perPou);


if($perPouNb>0 OR ISMJ===1){
    $pouvoirs=new gestTable("ksfv3",TBLPREFIXE.'pouvoirs','pou_id');
    //echo'<a id="pouvoirs"></a><h1 class="h1">POUVOIR</h1>';

    // -- vue personnage -- //
    echo'<input type="hidden" name="perPouEdit" value="1" />';
    echo'<span class="itemDescription">'.$itemsDescr->get('XP','diJ').'</span><br>';

    $o='';
    foreach($pouvoirs->get() as  $pouvoir){
        $pou_id=$pouvoir['pou_id'];
        if(isset($perPou[$pou_id])){
            $pou_id=$pouvoir['pou_id'];
            //echo'<div class="pouvoirBox">';
            $o.='<form method="POST" action="?'.ARIANE_AGORIA.'#pouvoirs">';
            $o.='<input type="hidden" name="perPouEdit" value="1" />';

            $nomP =$perPou[$pou_id]['nomP'];
            $score=$perPou[$pou_id]['score'];
            $note =$perPou[$pou_id]['note'];
    
            $o.='<input style="display:none" type="submit" value="" />';
            $o.='<input type="hidden" name="pouId'.$pou_id.'"  value="'.$pou_id.'" />';

            $o.='<div class="pouvoirBox">';
                // -- nomP -- //
                $nomAfficher=empty($nomP)?'Quel nom allez-vous donner &agrave; cette sensation?':$nomP;
                $o.=    '<span class="caracTitre">&nbsp;'.$nomAfficher.'</span>';
                $o.=    '&nbsp;'.IMG_EDITNC.' onclick=" inlineSwitch(\'pouNameEdit'.$pou_id.'\')" />';
                $o.=    '<div id="pouNameEdit'.$pou_id.'" style="display:none;">'
                        .'<br><input style="margin-left:30px" size="'.POUNOM_SIZE.'" maxlenght='.POUNOM_SIZE.' name="perPouNameEdit'.$pou_id.'" title="Nom de ce pouvoir('.POUNOM_SIZE.' car. max)" value="'.$nomP.'" />'
                    .'</div>'
                    ;
                // -- niveau -- //
                if($score=='0'){
                    $scoreNiveau='<span title="niveau:0">0</span>';
                }
                else{
                    $scoreNiveau='';
                    for ($i=0;$i<$score;$i++)$scoreNiveau.='<img alt="niveauIMG" title="niveau:'.$score.'" src="'.IMGPOUVOIR_SRC.'" />';
                }
                $o.=" <div class='pouvoirNiveau'>$scoreNiveau</div>";

                // -- description -- //
                $o.='<div class="itemDescriptionPouvoir">'.$pouvoir['pou_description'].'</div>';
            $o.='</div><!--div class="pouvoirBox"-->';

            $o.='</form>'."\n";
        }//if(isset($perPou[$pou_id]))
    }//foreach($pouvoirs->get() as $pouvoir)
    echo "$o";
    echo'<br>';
}//if($perPouNb>0 OR ISMJ===1){
?>


<!--class="h1">CARACTERISTIQUES</h1-->
<br>
<div class="caracsBox">

    <!-- div pour forcer la largeur -->
    <!--div class="caracsBox"></div-->

    <!-- Talents -->
    <div class="caracBox">

        <div class="caracTitre">Talents</div>
        <div class="caracTalRes">
            <?php
            $o='';
            foreach($talents->get() as $talent){
                $id=$talent['tal_id'];

                $dispo=$perso->get('per_talent'.$id.'Dispo');

                $nom=$talent['tal_nom'];
                $descr=$talent['tal_description'];

                // - 1er colonne Actif/desactif - //
                if(isset($perTals[$id])){$title='Activé';$imgSrc=LOGOACTIF_SRC;}
                else            {$title='Inactif';$imgSrc=LOGOINACTIF_SRC;}
                $o.="<img class='itemActif' src='$imgSrc' title='$title' />";

                // - 2eme colonne vide ou icone si carac>0 - //
                if($dispo<0) {$title="Disponible: $dispo";$imgSrc=LOGOBUG_SRC;}
                if($dispo==0){$title="Disponible: $dispo";$imgSrc=LOGOVIDE_SRC;}
                if($dispo>0) {$title="Disponible: $dispo";$imgSrc=LOGODISPONIBLE2_SRC;}
                $o.="<img class='itemActif' src='$imgSrc' title='$title' />";

                // - 3eme colonne: nom title:description - //
                $o.="<span title=\"$descr\".>$nom</span><br>";
                //                $o.='</span><br>';
            }
            echo $o;
            ?>
        </div>
    </div>


    <div class="caracBox">

        <!-- Richesse -->
        <div class="caracTitre">Richesse</div>
        <div class="caracRicPro">
            <?php
            //echo    "$richesseDispoVal /  $richesseMaxVal<br>"; 
            $max='';
            $_title="richesse max:$richesseMaxVal. ". $itemsDescr->get('richesseMax','diJ');
            if ($richesseMaxVal==0)$max.='<img alt="richesse max" title="'.$_title.'" src="'.LOGOINACTIF_SRC.'" />';
            for ($i=0;$i<$richesseMaxVal;$i++)$max.='<img alt="richesse max" title="'.$_title.'" src="'.LOGOACTIF_SRC.'" />';
            echo $max.'<br>';

            $dispo='';
            $_title="richesse disponible:$richesseDispoVal. ". $itemsDescr->get('richesseDispo','diJ');
            for ($i=0;$i<$richesseDispoVal;$i++)$dispo.='<img alt="richesse disponible" title="'.$_title.'" src="'.LOGODISPONIBLE2_SRC.'" />';
            echo $dispo.'<br>';

            ?>
        </div><!--div class="caracRicPro"-->

        <!-- Proche -->
        <div class="caracTitre">Proches</div>
        <div class="caracRicPro">
            <?php
            //echo    "$procheDispoVal /  $procheMaxVal"
            //.    '<span class="itemDescription">'.$itemsDescr->get('proche','diJ').'</span>'
            //.     '<br>';
            $max='';
            $_title="proche max:$procheMaxVal. ". $itemsDescr->get('procheMax','diJ');
            if ($procheMaxVal==0)$max.='<img alt="Proches max" title="'.$_title.'" src="'.LOGOINACTIF_SRC.'" />';
            for ($i=0;$i<$procheMaxVal;$i++)$max.='<img alt="proche max" title="'.$_title.'" src="'.LOGOACTIF_SRC.'" />';
            echo $max.'<br>';

            $dispo='';
            $_title="proche disponible:$procheDispoVal. ". $itemsDescr->get('procheDispo','diJ');
            for ($i=0;$i<$procheDispoVal;$i++)$dispo.='<img alt="proche disponible" title="'.$_title.'" src="'.LOGODISPONIBLE2_SRC.'" />';
            echo $dispo.'<br>';
            ?>
        </div><!--div class="caracRicPro"-->


    </div><!--div class="caracBox"-->

    <!-- Reseaux -->
    <div class="caracBox">
        <div class="caracTitre">R&eacute;seaux</div>
        <span class="caracTalRes">
            <?php
            $o='';
            foreach($reseaux->get() as $reseau){
                $id=$reseau['res_id'];

                $dispo=$perso->get('per_reseau'.$id.'Dispo');
                $nom=$reseau['res_nom'];
                $descr=$reseau['res_description'];

                // - 1er colonne Actif/desactif - //
                if (isset($perRes[$id])){$title='Activé'; $imgSrc=LOGOACTIF_SRC;}
                else            {$title='Inactif';$imgSrc=LOGOINACTIF_SRC;}
                $o.="<img class='itemActif' src='$imgSrc' title='$title' />";

                // - 2eme colonne vide ou icone si carac>0 - //
                if($dispo<0) {$title="Disponible: $dispo";$imgSrc=LOGOBUG_SRC;}
                if($dispo==0){$title="Disponible: $dispo";$imgSrc=LOGOVIDE_SRC;}
                if($dispo>0) {$title="Disponible: $dispo";$imgSrc=LOGODISPONIBLE2_SRC;}
                $o.="<img class='itemActif' src='$imgSrc' title='$title' />";

                // - 3eme colonne: nom title:description - //
                $o.="<span title=\"$descr\".>$nom</span><br>";
            }
            echo $o;
            ?>
        </span>

    </div><!--div class="caracBox"-->

</div><!--div class="caracsBox"-->

<div class="clear"></div>
<?php


// ====================== //
// = Note du personnage = //
// ====================== //
?>
<!-- Note du personnage -->
<a name="Note"></a>
<form method="POST" action="?<?php echo ARIANE_AGORIA?>#Note">
<h1 class="pointeur" onclick="blockSwitch('perso-note');">CARNET DE NOTES</h1>

<input type="hidden" name="perNoteEdit" value="1" />
<div class="centrer w300p" style="width:60px"><input type="submit" value="sauver" /></div>
<textarea name="perNoteData" id="perNoteData" class="pouvoirBox blocNote" onkeyup="IDhautAuto(this.id);"><?php echo $perNote;?></textarea>
</form><br>
<br>
<?php


// =========== //
// = journal = //
// =========== //
if (JOURNAL_SHOW===1){
    echo'<h1 class="pointeur" onclick="blockSwitch(\'perso-journal\');" >JOURNAL</h1>';

    $addOrga=PERSONO<1?'(domaine="perso" OR domaine="orga")':'domaine="perso"';

    $logs= new gestTable('ksfv3',TBLPREFIXE.'logs','log_id',
        [
        'SELECT'=>
            'log_id,ts,domaine,fromLogin,toPersoNo,actionNo,extra'
        .    ',lox_id,lox_texte'
        .    ',per_prenom,per_nom'
        .    ',DATE_FORMAT(ts,"%d/%m/%Y %H:%i:%s") AS tsf,DATE_FORMAT(ts,"%j") AS tsj'

        ,'JOIN'=>
            ' LEFT JOIN '.TBLPREFIXE.'logTextes ON '.TBLPREFIXE.'logs.actionNo =   '.TBLPREFIXE.'logTextes.lox_id'
        .    ' LEFT JOIN '.TBLPREFIXE.'personnages ON '.TBLPREFIXE.'logs.toPersoNo = '.TBLPREFIXE.'personnages.per_id'
        ,'WHERE' => 'toPersoNo = '.PERSONO.' AND '.$addOrga
        ,'ORDERBY'=>'ts DESC'
        ,'clear'=> 0
        ]
    );
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($logs->dbTable->sql->getSQL()).'</div>';
    //if(ISDEV===1)echo '---'.$logs->tableau();
    ?>
<div id="perso-journal" style="display:block;">
<a href="#bottom">Aller en bas de la liste</a>
<div class="" style="max-height:25em;overflow:auto;">
<?php
    $tsjOld=0;
    foreach($logs->get() as  $_log){
        $tsf=$_log['tsf'];
        $tsj        =$_log['tsj'];// no du jours dans l'annee 01..366
        $tsjCSS=($tsjOld!==$tsj)?' msg_tsNewJ':'';
        $tsjOld=$tsj;

        $domaine=$_log['domaine'];
        $orgaCSS=$domaine=='orga'?'orgas':'';

        // - log - //
        $fromLogin=$_log['fromLogin'];
        $toPersoNo=$_log['toPersoNo'];
        $per_pn=$_log['per_prenom'].''.$_log['per_nom'];

        $actionNo=$_log['actionNo'];
        $texte=$_log['lox_texte'];
        $extra=$_log['extra'];
    
        // - affichage - //
        //echo "<div>$ts $fromLogin [$domaine] $per_pn $texte $extra</div>";

        // ajouter formPersoNo dans la db
        $fl=(ISORGA===1)?" <span class='orgas'>[$fromLogin]</span> ":' ';
        //echo "<div>$ts de $fl <!-- $fromPersoNo -->$texte $extra</div>";
        echo "<div class='$orgaCSS'>";
        echo "<span class='$tsjCSS'>$tsf</span>";
        echo "<span class=''>$fl</span>";
        echo "<span class=''>$texte $extra</span>";
        echo '</div>';
    }
    unset($logs);
    echo'<a name="bottom"></a></div>';
    echo'</div><!--div id="perso-journal"-->';
}//if (JOURNAL_SHOW===1)


// ========= //
// = ORGAS =  //
// ========= //
if (ISMJ===1){
    $per_id=PERSONO;?>
    <br><a name="fichePerso-orgas"></a>
    <div class="per_etat'.$perEtat.' orgas txtcenter">
    <h1 class="h1">ACCEE ORGANISATEUR<br>Personnage NO <?php echo PERSONO.' '.PER_PRENOM.' '.PER_NOM?></h1>

    <form id="FpersoEdit" method="POST" name="Fedit" action="?<?php echo ARIANE_AGORIA?>#fichePerso-orgas">
        <input name="editPerNo" type="submit" value="sauver" />
        <input name="perNo" type="hidden" value="<?php echo PERSONO?>" />
        <br>

        <label for="perPrenEdit"></label><input id="perPrenEdit" name="perPrenEdit" placeholder="prénom" value="<?php echo PER_PRENOM?>"/><br>
        <label for="perNameEdit"></label><input id="perNameEdit" name="perNameEdit" placeholder="nom"  value="<?php echo PER_NOM?>"/><br>

    <!-- // -- Nom Prenom -- //

    echo IMG_EDITNC.' onclick=" inlineSwitch(\'persoPrenom\')" />'.$persoPrenom
    .    '<span id="persoPrenom" style="display:none;">'
    .        '<input name="perPrenEdit" value="'.$persoPrenom.'" />'
    .    '</span> '
    
    .    IMG_EDITNC.' onclick="inlineSwitch(\'persoNom\')" />'.$persoNom
    .    '<span id="persoNom" style="display:none;">'
    .        '<input name="perNameEdit" value="'.$persoNom.'" />'
    .    '</span>';

    // - etat - // -->
        <select name="per_etat">
            <option disabled>&eacute;tat:</option>
            <?php echo selectOptionEtat($perEtat);?>
        </select>

    </form>

    <!-- // - autorisation - // -->
    <a id="orgas-Autorisations"></a><h1 class="h1">Autorisations</h1>
    <div>
    <?php
    $titleOui='Autoriser';
    $titleNon='Interdire';

    if(PER_ISMES===0){$oui='autorisationOf';$non='autorisationOn';}else{$oui='autorisationOn';$non='autorisationOf';}
    echo' Messagerie:';
    echo " <a href='?per_id=$per_id.&amp;edit=aut_isMesOn&amp;".ARIANE_AGORIA."#orgas-Autorisations' class='$oui' title='$titleOui'>Oui</a>";
    echo "|<a href='?per_id=$per_id.&amp;edit=aut_isMesOff&amp;".ARIANE_AGORIA."#orgas-Autorisations' class='$non' title='$titleNon'>Non</a>";

    if(PER_ISACT===0){$oui='autorisationOf';$non='autorisationOn';}else{$oui='autorisationOn';$non='autorisationOf';}
    echo' Action:';
    echo " <a href='?per_id=$per_id.&amp;edit=aut_isActOn&amp;".ARIANE_AGORIA."#orgas-Autorisations' class='$oui' title='$titleOui'>Oui</a>";
    echo "|<a href='?per_id=$per_id.&amp;edit=aut_isActOff&amp;".ARIANE_AGORIA."#orgas-Autorisations' class='$non' title='$titleNon'>Non</a>";

    if(PER_ISREC===0){$oui='autorisationOf';$non='autorisationOn';}else{$oui='autorisationOn';$non='autorisationOf';}
    echo' Recherche:';
    echo " <a href='?per_id=$per_id.&amp;edit=aut_isRecOn&amp;".ARIANE_AGORIA."#orgas-Autorisations' class='$oui' title='$titleOui'>Oui</a>";
    echo "|<a href='?per_id=$per_id.&amp;edit=aut_isRecOff&amp;".ARIANE_AGORIA."#orgas-Autorisations' class='$non' title='$titleNon'>Non</a>";

    if(PER_ISECH===0){$oui='autorisationOf';$non='autorisationOn';}else{$oui='autorisationOn';$non='autorisationOf';}
    echo' Echange:';
    echo " <a href='?per_id=$per_id.&amp;edit=aut_isEchOn&amp;".ARIANE_AGORIA."#orgas-Autorisations' class='$oui' title='$titleOui'>Oui</a>";
    echo "|<a href='?per_id=$per_id.&amp;edit=aut_isEchOff&amp;".ARIANE_AGORIA."#orgas-Autorisations' class='$non' title='$titleNon'>Non</a>";

    if(PER_ISDEC===0){$oui='autorisationOf';$non='autorisationOn';}else{$oui='autorisationOn';$non='autorisationOf';}
    echo ' D&eacute;couverte:';
    echo " <a href='?per_id=$per_id.&amp;edit=aut_isDecOn&amp;".ARIANE_AGORIA."#orgas-Autorisations' class='$oui' title='$titleOui'>Oui</a>";
    echo "|<a href='?per_id=$per_id.&amp;edit=aut_isDecOff&amp;".ARIANE_AGORIA."#orgas-Autorisations' class='$non' title='$titleNon'>Non</a>";
?>
    </div>


<a id="orgas-richesseProche"></a><h1 class="h1">Richesse/Proche</h1>
<?php
echo '<div class="">Faite un refresh (F5) pour r&eacute;p&eacute;ter la dernière commande.<br>Corrollaire: ATTENTION:Un refresh répéte la dernière opération!<br> En vrai il y a un bug: Seul le 1er clic d\'une commnde est prise en compte, les autres ne fonctionne pas.</div>';
echo    "<div class=''>Richesse: dispo: $richesseDispoDec $richesseDispoVal $richesseDispoInc / max: $richesseMaxDec $richesseMaxVal $richesseMaxInc</div>";
echo    "<div class=''>Proche: dispo: $procheDispoDec $procheDispoVal $procheDispoInc / max: $procheMaxDec $procheMaxVal $procheMaxInc</div>";
?>
<a id="talents"></a><h1 class="h1">talents</h1>
<div class="flex-container"><div class="w300p">
<h2 class="h2">Talents activés</h2>
<form method="POST" action="?<?php echo ARIANE_AGORIA?>#talents">
<?php
if (TAL_UNSETABLE===TRUE) echo '<input type="submit" value="désactiver" /> (le perso garde ses points disponible)';

$o='';
foreach($perTals as $tal_id => $perTal){
    $o.='<li style="list-style-type:none;">';
    if(TAL_UNSETABLE===TRUE)$o.=' <input type="radio" name="unsetTalentNo" value="'.$tal_id.'" />';// désactiver
    $o.=$perTal['nom'];
    $o.=' <span class="itemDescription">'. $perTal['description'].'</span>';
$o.='</li>';
}
echo "<ul>$o</ul></form>";
unset($tal_id);
?>
</div><div class="flex-item-fluid">
<h2 class="h2">Talents non activés</h2>
<form method="POST" action="?<?php echo ARIANE_AGORIA?>#talents">
<?php
echo '<input type="submit" value="activer" />  - Attention ne peut être changé!';

$o='';
foreach($talents->get() as $talent){
    $id=$talent['tal_id'];
    if(isset($perTals[$id]))continue; //deja actif
    $o.='<li style="list-style-type:none;">';
    //if ($talDispo > 0)
               $o.=' <input type="radio" name="setTalentNo" value="'.$id.'" />';
    $o.=$talent['tal_nom'];
    $o.=' <span class="itemDescription">'.$talents->get($id,'tal_description').'</span>';
    $o.='</li>';
}
echo "<ul>$o</ul></form>";
unset($talents,$perTals,$talMax);
?>
</div><!-- talentNon activé --></div><!-- talents -->


<a id="reseaux"></a><h1 class="h1">reseaux</h1>
<div class="flex-container">

<div class="w300p"><h2 class="h2">Réseaux activés</h2>
<form method="POST" action="?<?php echo ARIANE_AGORIA?>#reseaux">
<?php
if (RES_UNSETABLE===TRUE) echo '<input type="submit" value="désactiver" />';
$o='';

foreach($perRes as $res_id =>$perTal){
    $o.='<li>';
    if(RES_UNSETABLE===TRUE)$o.=' <input type="radio" name="unsetReseauNo" value="'.$res_id.'" />';// désactiver
    $o.=$perTal['nom'].' <span class="itemDescription">'. $perTal['description'].'</span>';
    $o.='</li>';
}
echo "<ul>$o</ul></form>";
?>
</div><div class="flex-item-fluid">
<h2 class="h2">Resaux non activés</h2>
<form method="POST" action="?<?php echo ARIANE_AGORIA?>#reseaux">
<?php
echo '<input type="submit" value="activer" />  - Attention ne peut être changé!';

$o='';
foreach($reseaux->get() as $reseau){
    $id=$reseau['res_id'];
    if( isset($perRes[$id]))continue; //deja actif
    $o.='<li style="list-style-type:none;">';
    //if ($resDispo > 0) 
        $o.=' <input type="radio" name="setReseauNo" value="'.$id.'" />';
    $o.=$reseau['res_nom'];
    $o.=' <span class="itemDescription">'.$reseaux->get($id,'res_description').'</span>';
    $o.='</li>';
}

echo "<ul>$o</ul></form>";
unset($reseaux,$perRes,$resMax);
?>
</div><!-- Resaux Non activé --></div><!-- Reseaux -->


<!--
// =========== //
// = POUVOIRS  //
// =========== //
-->
<a name="orgas-pouvoirs"></a><h1 class="h1">POUVOIR</h1>

<div class="">ATTENTION d&eacute;sactiver un pouvoir d&eacute;truit aussi le niveau et les textes personnalis&eacute;s associ&eacute;s!<br>Il n'y &agrave; pas de confirmation!</div>
<div id="pouvoirsEdit">
<?php

$o='';
foreach($pouvoirs->get() as $pouvoir){
    $pou_id=$pouvoir['pou_id'];
    $o.='<form method="POST" name="forgPouEdit'.$pou_id.'" action="?'.ARIANE_AGORIA.'#orgas-pouvoirs" class="">';
    $o.='<input type="hidden" name="orgPouEdit" value="'.$pou_id.'" />';

    $nomR=$pouvoirs->get($pou_id,'pou_nom');

    $o.='<span class="pouvoirLabel">'.$nomR.': </span>';

    if(isset($perPou[$pou_id])){$o.=' <input type="submit" name="unsetPouvoir" value="désactiver" />';}
    else  $o.=' <input type="submit" name="setPouvoir" value="activer" />';
    if(isset($perPou[$pou_id])){
        $nomP =$perPou[$pou_id]['nomP'];
        $score=$perPou[$pou_id]['score'];
        $note =$perPou[$pou_id]['note'];
    

        // -- score -- //
        $o.=' score:';
        if($score>0)$o.='<a href="?per_id='.PERSONO.'&amp;pouNo='.$pou_id.'&amp;edit=scoreDec&amp;'.ARIANE_AGORIA.'#orgas-pouvoirs" title="Décrementer le score de ce pouvoir"> - </a>';
        $o.=" $score ";
        $o.='<a href="?per_id='.PERSONO.'&amp;pouNo='.$pou_id.'&amp;edit=scoreInc&amp;'.ARIANE_AGORIA.'#orgas-pouvoirs" title="Incrementer le score de ce pouvoir"> + </a>';
    }//if(isset($perPou[$pou_id]))
    //$o.='<span class="itemDescription">'.$pouvoir['pou_description'].'</span>';
    $o.='</form>'."\n";
}//foreach($pouvoirs->get() as $pouvoir)

echo "$o";
echo '</div><!--<div id="pouvoirsEdit"-->';
echo '</div><!--div class="orgas"-->';
} //if (ISMJ==1)


}//if(defined(PERSONO))
else{
    if(ISORGA==TRUE){
        echo '<div class="notewarning">Pas de personnage séléctionné.<br>Pour en séléctionné un aller dans la liste <a href="?ksfv3=orgas&orgas=orgas-persos">personnages</a></div>';
    }
    if(ISJOUEUR==TRUE){
        echo '<div class="notewarning">Vous n\'avez pas de personnage attribué. Contacter l\'admin ou un MJ</div>';
    }
}
