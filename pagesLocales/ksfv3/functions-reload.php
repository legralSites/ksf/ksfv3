<?php
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_File"></div>';


// fonctions de chargement globales.
// Ce fichier doit etre appelllé apres les autres fichiers include de mise a jours (function-communes.php,functions-orgas.php


// ============================= //
// = rechagement du personnage = //
// ============================= //
$perso=new gestLigne($dbksfV3,TBLPREFIXE.'personnages',PERSONO,'per_id',['clear'=>0]);
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">/* chargement du personnage */'.ln2br($perso->legralPDO->sql->getSQL()).'</div>';
//if(ISDEV===1)echo $perso->tableau();

// - calcul  de la ville du perso - //
define('PER_VILLEID',$perso->get('per_villeId'));
define('MJID',-PER_VILLEID+1);

// - Nom Prenom- //
define('PER_NOM',$perso->get('per_nom'));
define('PER_PRENOM',$perso->get('per_prenom'));

// - Autorisations - //
define('PER_ISACT',$perso->get('per_isAct')==='1'?1:0);
define('PER_ISREC',$perso->get('per_isRec')==='1'?1:0);
define('PER_ISMES',$perso->get('per_isMes')==='1'?1:0);
define('PER_ISECH',$perso->get('per_isEch')==='1'?1:0);
define('PER_ISDEC',$perso->get('per_isDec')==='1'?1:0);

// - calcul  de la ville du perso - //
define('PER_VILLEID',$perso->get('per_villeId'));
define('MJID',-PER_VILLEID+1);

// = rechagement des personnages de la ville du perso = //
$persos=personnagesLoad();


// ====================================================== //
// = recherche du nombre des phases (A/R)               = //
// = Neccessite functions-orgas pour prise en compt maj = //
// ====================================================== //
$dbksfV3->sql->clear();
$sql='/* compte le nombre de phase d Action simultanné */'."\n";;
$sql.='SELECT COUNT(*) AS actionNb FROM  '.TBLPREFIXE.'planning WHERE pla_type = 3 AND pla_debut <= NOW() AND   NOW() <= pla_fin AND pla_villeId='.PER_VILLEID.' ;';
$dbksfV3->sql->setOPERATION($sql);
$sql=$dbksfV3->query();
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
while ($dbksfV3->fetch()){define ('PHASE_A_NB',$talId=$dbksfV3->lignes['actionNb']);}
//echo 'PHASE_A_NB'.PHASE_A_NB.'<br>';

$dbksfV3->sql->clear();
$sql='/* compte le nombre de phase de RECHERCHE simultanné */'."\n";
$sql.='SELECT COUNT(*) AS Nb FROM  '.TBLPREFIXE.'planning WHERE pla_type = 4 AND pla_debut <= NOW() AND   NOW() <= pla_fin AND pla_villeId='.PER_VILLEID.';';
$dbksfV3->sql->setOPERATION($sql);
$sql=$dbksfV3->query();
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
while ($dbksfV3->fetch()){define ('PHASE_R_NB',$talId=$dbksfV3->lignes['Nb']);}
//echo 'PHASE_R_NB'.PHASE_R_NB.'<br>';


// ===================== //
// = run               = //
// ===================== //
if(ISMJ===1)msgEdit();
