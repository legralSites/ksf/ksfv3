<?php
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_File"></div>';

// functions pour les organisateurs
// if (ISORGA)
global $gestLib,$dbksfV3;

// ========================= //
// - creations des donnees - //
// ========================= //

// - application des commandes - //

if(isset($_GET['create'])){
    switch($_GET['create']){

        case 'joueur':
            logAdd(0,  'orga'    ,110);
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'joueurs` (`jou_login`,jou_pwd,jou_villeId,jou_isJOUEUR) VALUES ("newJoueur",UUID(),'.VILLEID.',1);');
            break;

        // - phases- //
        case 'phaseNP':
            logAdd(0,  'orga'    ,111);
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'planning` (`pla_type`,pla_villeId) VALUES (1,'.VILLEID.')');
            break;

        case 'phaseS':
            logAdd(0,  'orga'    ,112);
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'planning` (`pla_type`,pla_villeId) VALUES (2,'.VILLEID.')');
            break;

        case 'phaseA':
            logAdd(0,  'orga'    ,113);
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'planning` (`pla_type`,pla_villeId) VALUES (3,'.VILLEID.')');
            break;

        case 'phaseR':
            logAdd(0,  'orga'    ,114);
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'planning` (`pla_type`,pla_villeId) VALUES (4,'.VILLEID.')');
            break;

        case 'phaseSP':
            logAdd(0,  'orga'    ,115);
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'planning` (`pla_type`,pla_villeId) VALUES (5,'.VILLEID.')');
            break;

        // - personnage - //
        case 'perso':
            logAdd(0,  'orga'    ,116,'ville N°'.VILLEID);
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'personnages` (`per_nom`,per_villeId) VALUES ("",'.VILLEID.')');
            break;

        case 'talent':
            logAdd(0,  'orga'    ,117);
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'talents` (`tal_nom`) VALUES ("")');
            break;
    
        case 'reseau':
            logAdd(0,  'orga'    ,118);
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'reseaux` (`res_nom`) VALUES ("")');
            break;

        case 'pouvoir':
            logAdd(0,  'orga'    ,119);
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'pouvoirs` (`pou_nom`) VALUES ("")');
            break;
    
        case 'decouverte':
            logAdd(0,  'orga'    ,120);
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'decouvertes` (dec_jouId,dec_villeId) VALUES ('.JOUNO.','.VILLEID.');');
            break;

    
        // - item description - //
        case 'itD':
            logAdd(0,  'orga'    ,0,"Ajout d'une description d'une item");
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'itemsDescriptions` (`nom`) VALUES ("0nouveau")');
            break;
    
        // - lox: logTextes  - //
        case 'loxJ'://Joueur
            logAdd(0,  'orga'    ,0,"création d'un logTexte loxJ");
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'logTextes` (`lox_id`) VALUES (99);');
            //}
            break;
        case 'loxO'://Orga
            //if(isset($_GET['lox_id'])){
                //$log_id=$_GET['lox_id'];
                logAdd(0,  'orga'    ,0,"création d'un logTexte loxO");
                $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'logTextes` (`lox_id`) VALUES (199);');
            //}
            break;
        case 'loxP'://personnage
            //if(isset($_GET['lox_id'])){INSERT INTO `'.TBLPREFIXE.'decouvertes` (dec_jouId,dec_villeId) VALUES (1,1);
                //$log_id=$_GET['lox_id'];
                logAdd(0,  'orga'    ,0,"création d'un logTexte loxP");
                $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'logTextes` (`lox_id`) VALUES (299);');
            //}
        case 'loxA'://autre
            //if(isset($_GET['lox_id'])){
                //$log_id=$_GET['lox_id'];
                logAdd(0,  'orga'    ,0,"création d'un logTexte loxA");
                $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'logTextes` (`lox_id`) VALUES (399);');
            //}
            break;
        case 'bug':
            //logAdd(0,  'orga'    ,120);
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'bugs` (bug_genre) VALUES (1);');
            break;
        case 'toDo':
            //logAdd(0,  'orga'    ,120);
            $dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'bugs` (bug_genre) VALUES (2);');
            break;
    }
    $sql=$dbksfV3->query();    $dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
}//if(isset($_GET['create']))


// ============= //
// = logTextes = //
// ============= //
if( (isset($_POST['loxNb']) AND $_POST['loxNb']>0 )){
    //œecho __FILE__.' edition de lox'; 
    $dbksfV3->sql->clear();
    $sqlOp='';
    foreach($_POST as $key => $value){
        $lout='';
        $sql='';

        $cmd=substr($key,0,11);    // on recupere la cmd sans la refId
        $id= substr($key,11,strlen($key));    //on recupere id en enlevant de  la cmd (tailleFixe)
        switch($cmd){
            //case 'lox_id_Edit':$sql='lox_id='.$value;break;
            case 'loxTextEdit':$sql='lox_texte="'.addslashes($value).'"';$lout=addslashes($value);break;

        }
        if($sql!=""){
            $sqlOp.='UPDATE '.TBLPREFIXE."logTextes Set $sql WHERE lox_id=$id;\n";
            //logAdd(PERSONO,  'orga'    ,0,"Modification du logTexte $id par <i>$lout</i>");
        }
    }
    $dbksfV3->sql->setOPERATION($sqlOp); $sql=$dbksfV3->query();     $dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
}//if(isset($_POST['bugNb'])){


// =========================== //
// = reset des               = //
//  * talentDispoN           = //
//  * reseauxDispoN          = //
//      des personnnagess    = //
//       (fonction orga)     = //
//   talentDispoN=0
//   talentDispo=1 si talentActif
// =========================== //
function resetTalRes($villeId){
    global $dbksfV3;

    $sqlRT='';

    // - mise à 0 des talents et reseaux  - //
    $sqlRT.='/* Mise a zero des talents et reseaux */'."\n";
    $sqlRT.='UPDATE '.TBLPREFIXE.'personnages SET'."\n";
    $sqlRT.=' per_talent1Dispo =0,per_talent2Dispo =0,per_talent3Dispo =0,per_talent4Dispo =0,per_talent5Dispo =0,per_talent6Dispo =0'."\n";
    $sqlRT.=',per_reseau1Dispo =0,per_reseau2Dispo =0,per_reseau3Dispo =0,per_reseau4Dispo =0,per_reseau5Dispo =0,per_reseau6Dispo =0'."\n";
    $sqlRT.=" WHERE per_villeID=$villeId;\n\n";


    // - reset des talents Actifs - //
    // -- chargement des talentsActif de TOUS les personnages de la ville -- //
    $dbksfV3->sql->clear();
    $dbksfV3->sql->setFROM(TBLPREFIXE.'perTalActif');
    $dbksfV3->sql->setJOIN('JOIN '.TBLPREFIXE.'personnages ON per_id = perId');
    $dbksfV3->sql->setWHERE("per_villeId=$villeId");
    $dbksfV3->sql->setOPERATION('SELECT perId,talId');
    $sql=$dbksfV3->query();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

    // -- creation de la cmd sql talentDispoN=1  -- //
    $sqlRT.='/* reset des talents actifs */'."\n";
    while ($dbksfV3->fetch()){
        $perId=$dbksfV3->lignes['perId'];
        $talId=$dbksfV3->lignes['talId'];
        $sqlRT.=' UPDATE '.TBLPREFIXE.'personnages SET per_talent'.$talId."Dispo = 1 WHERE per_id=$perId;\n";

    }
    $dbksfV3->queryClose();


    // - reset des reseaux Actifs - //
    // - chargement des reseauxActif de TOUS les personnages de la ville - //
    $dbksfV3->sql->clear();
    $dbksfV3->sql->setFROM(TBLPREFIXE.'perResActif');
    $dbksfV3->sql->setJOIN('JOIN '.TBLPREFIXE.'personnages ON per_id = perId');
    $dbksfV3->sql->setWHERE("per_villeId=$villeId");
    $dbksfV3->sql->setOPERATION('SELECT perId,resId');
    $sql=$dbksfV3->query();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

    // -- creation de la cmd sql reseauxDispo=1  -- //
    $sqlRT.='/* reset des reseaux actifs */ '."\n";
    while ($dbksfV3->fetch()){
        $perId=$dbksfV3->lignes['perId'];
        $resId=$dbksfV3->lignes['resId'];
        $sqlRT.=' UPDATE '.TBLPREFIXE.'personnages SET per_reseau'.$resId."Dispo = 1 WHERE per_id=$perId;\n";
    }
    $dbksfV3->queryClose();

    // --  execution de la cmd RT reseauDispoN=1 talentsDispo=1 -- //
    $dbksfV3->sql->clear();$dbksfV3->sql->setOPERATION($sqlRT);$sql=$dbksfV3->query();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
}//function resetTalRes($villeId)


// ----------------------- //
// - edition des joueurs - //
// - page orga-joueur    - //
// -   TOUS LES JOUEURS  - //
// note et pwd sont changés//
//   via le profil joueur  //
// (functions-communes.php)//
// ----------------------- //
if(isset($_POST['editJouNo'])){
    $dbksfV3->sql->clear();

    $id=$_POST['jouNo'];
    $login=$dbksfV3->sql->formatChamp($_POST['jouLogin'],0,1);
    $nom=  $dbksfV3->sql->formatChamp($_POST['jouNom'],0,1);
    $mail= $dbksfV3->sql->formatChamp($_POST['jouMail'],0,1);
    //$isFONDATEUR=$_POST['isFONDATEUR']; // pas modifiable/activable) ici
    $isADMIN= isset($_POST['isADMIN'])?1:0;
    $isMJ=  isset($_POST['isMJ'])?1:0;
    $isDEV=  isset($_POST['isDEV'])?1:0;
    $isJOUEUR=  isset($_POST['isJOUEUR'])?1:0;
    $jouPeNo=$_POST['jouPeNo'];
    $sqlADMIN=(ISFONDATEUR===1)?",jou_isADMIN=$isADMIN":'';// seul les fondateurs peuvent acvtiver l'administration
    $sqlMJ=(ISMJ===1)?",jou_isMJ=$isMJ":'';// seul les MJ peuvent activer le status de MJ d'un joueur
    $sql='UPDATE `'.TBLPREFIXE."joueurs` SET jou_ts=jou_ts, jou_login=$login,jou_nom=$nom,jou_mail=$mail$sqlADMIN$sqlMJ,jou_isDEV=$isDEV,jou_isJOUEUR=$isJOUEUR,jou_persoNo=$jouPeNo WHERE jou_id=$id";
    $dbksfV3->sql->setOPERATION($sql);$sql=$dbksfV3->query();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
}


// ------------------------ //
// - edition d'un message - //
// ------------------------ //
function msgEdit(){
    if(isset($_POST['msgNoEdit']) AND $_POST['msgNoEdit']>0 ){
        if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_file">Edition d un message</div>';
        msgSetMetaDonnees($_POST['msgNoEdit']);
    }
}//function msgEdit()


// - recoit une commande de suppression d'un message avec confirmation - //
function msgEraseAsk(){
    //echo gestLib_inspect('$_GET[msgEraseAsk]',$_GET['msgEraseAsk']);
    if(isset($_GET['msgEraseAsk'])){
        $msgId=(int)$_GET['msgEraseAsk'];
        logAdd(PERSONO,  'orga'    ,131,'Msg N°'.$msgId);
        echo '<a name="msgEraseAsk"></a>';
        return $msgId;
    }
    return -1;
}//function msgEraseAsk()

// - recoit une commande de suppression d'un message SANS confirmation - //
function msgErase(){
    if(isset($_GET['msgErase'])){
        echo '<a name="msgErase"></a>';
        $msgId=(int)$_GET['msgErase'];
        global $dbksfV3;
        $dbksfV3->sql->clear();
        $sql='DELETE FROM `'.TBLPREFIXE.'messageries` WHERE msg_id='.$msgId;
        $dbksfV3->sql->setOPERATION($sql);
        $sql=$dbksfV3->query();
        $dbksfV3->queryClose();
        if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
        logAdd(PERSONO,  'orga'    ,132,'Msg N°'.$msgId);
        return $msgId;
    }
    return -1;
}//function msgErase()

// ------------------------- //
// - edition des plannings - //
// ------------------------- //
if(isset($_POST['plaNb']) AND $_POST['plaNb']>0 ){
    $sqlOp='';
    foreach($_POST as $key => $value){
        $lout='';

        $sql='';
        $cmd=substr($key,0,11);        // on recupere la cmd sans la refId
        $id= substr($key,11,strlen($key));    //on recupere id en enlevant de  la cmd (tailleFixe)

        switch($cmd){
            case 'plaTypeEdit':$sql='pla_type='.$value;      $lout.=" <b>type</b>: $value";break;
            case 'plaDeO_Edit':$sql='pla_deO="'.$value.'"';  $lout.=" <b>deO</b>: $value";break;
            case 'plaDeJ_Edit':$sql='pla_deJ="'.$value.'"';  $lout.=" <b>deJ</b>: $value";break;
            case 'plaDebuEdit':$sql='pla_debut="'.$value.'"';$lout.=" <b>debut</b>: $value";break;
            case 'plaFin_Edit':$sql='pla_fin="'.$value.'"';  $lout.=" <b>fin</b>: $value";break;
        }

        if($sql!="") $sqlOp.='UPDATE '.TBLPREFIXE."planning SET $sql WHERE pla_id=$id;\n";
    }
    $dbksfV3->sql->clear();$dbksfV3->sql->setOPERATION($sqlOp);$sql=$dbksfV3->query();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    logAdd(0,  'orga'    ,0,"modif des plannings");
}//if(isset($_POST['talNb'])){


// --------------------------- //
// - edition d'UNE DECOUVERTE - //
// --------------------------- //
if(isset($_POST['decouverteSet'])){
    $dbksfV3->sql->clear();
    $sqlOp='UPDATE '.TBLPREFIXE.'decouvertes Set '."\n";
    $dbksfV3->sql->setWHERE('dec_id='.$_POST['decouverteNo']);
    $sqlOp.='dec_nom="'.addslashes($_POST['dec_nom']).'"';
    //$sqlOp.=$dbksfV3->sql->strChamp('dec_AD',$_POST['dec_AD']);

    $sqlOp.=',dec_code="'.addslashes($_POST['dec_code']).'"'."\n";
    $sqlOp.=',dec_description="'.addslashes($_POST['dec_description']).'"'."\n";
    $sqlOp.=',dec_message="'.addslashes($_POST['dec_msg']).'"'."\n";

    $sqlOp.=',cdt_persoNo='.$_POST['cdt_persoNo'];

    $sqlOp.=',cdt_richessePrix='.$_POST['cdt_richessePrix'];
    $sqlOp.=',cdt_prochePrix='.$_POST['cdt_prochePrix'];
    $sqlOp.="\n";
    for($i=1;$i<=TALNB;$i++) $sqlOp.=', cdt_tal'.$i.'='.$_POST["cdt_tal$i"];$sqlOp.="\n";
    for($i=1;$i<=RESNB;$i++) $sqlOp.=', cdt_res'.$i.'='.$_POST["cdt_res$i"];//$sqlOp.="\n";
    //for($i=1;$i<=POUNB;$i++) $sqlOp.=', cdt_poS'.$i.'='.$_POST["cdt_poS$i"];//pouvoirScore

    $dbksfV3->sql->setOPERATION($sqlOp);    $sql=$dbksfV3->query();    $dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
}



// ===================================================== //
// = edition des tables: talents/resaux/pouvoirs/items = //
// ===================================================== //
// - edition des talents - //
if(isset($_POST['talNb']) AND $_POST['talNb']>0 ){
    $dbksfV3->sql->clear();

    $lout='';
    $sqlOp='';
    foreach($_POST as $key => $value){
        $lout='';
        $sql='';

        $cmd=substr($key,0,11);               // on recupere la cmd sans la refId
        $id= substr($key,11,strlen($key));    //on recupere id en enlevant de  la cmd (tailleFixe)

        switch($cmd){
            case 'talNameEdit':$sql='tal_nom="'.$value.'"';        $lout.=" nom: $value";break;
            case 'talDescEdit':$sql='tal_description="'.$value.'"';$lout.=" decription: $value";break;
        }
        //post valide -> maj    
        if($sql!=""){
            $sqlOp.='UPDATE '.TBLPREFIXE."talents Set $sql WHERE tal_id=$id;\n";
            logAdd(PERSONO,  'orga'    ,101,"le talent N°$id a été modifié: $lout");// masse!
        }
    }

    // -- construction du sql -- //
    $dbksfV3->sql->setOPERATION($sqlOp);$sql=$dbksfV3->query();    $dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

}//if(isset($_POST['talNb'])){


// - edition des reseaux- //
if(isset($_POST['resNb']) AND $_POST['resNb']>0 ){
    $dbksfV3->sql->clear();
    $sqlOp='';
    foreach($_POST as $key => $value){
        $lout='';
        $sql='';

        $cmd=substr($key,0,11);               // on recupere la cmd sans la refId
        $id= substr($key,11,strlen($key));    //on recupere id en enlevant de  la cmd (tailleFixe)

        switch($cmd){
            case 'resNameEdit':$sql='res_nom="'.$value.'"';        $lout.="nom: $value";break;
            case 'resDescEdit':$sql='res_description="'.$value.'"';$lout.=" decription: $value";break;
        }

        //post valide -> maj    
        if($sql!=""){
            $sqlOp.='UPDATE '.TBLPREFIXE."reseaux Set $sql WHERE res_id=$id;\n";
            logAdd(0,  'orga'    ,102,"le réseau N°$id a été modifié: $lout");// a destination du MJ
        }
    }
    $dbksfV3->sql->setOPERATION($sqlOp);$sql=$dbksfV3->query();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
}//if(isset($_POST['resNb'])){



// - edition des pouvoirs - //
if(isset($_POST['pouNb']) AND $_POST['pouNb']>0 ){
    $dbksfV3->sql->clear();

    $lout='';
    $sqlOp='';
    foreach($_POST as $key => $value){
        $sql='';
        $cmd=substr($key,0,11);               // on recupere la cmd sans la refId
        $id= substr($key,11,strlen($key));    // on recupere id en enlevant de  la cmd (tailleFixe)

        switch($cmd){
            case 'pouNameEdit':$sql='pou_nom="'.$value.'"';        $lout.="nom: $value";break;
            case 'pouDescEdit':$sql='pou_description="'.$value.'"';$lout.=" decription: $value";break;
        }

        //post valide -> maj    
        if($sql!=""){
            $sqlOp.='UPDATE '.TBLPREFIXE."pouvoirs Set\n $sql WHERE pou_id=$id;\n";
        //logAdd(0,  'orga'    ,103,"le pouvoir N°$id a été modifié: $lout");// a destination du MJ
        }
    }
    $dbksfV3->sql->setOPERATION($sqlOp);    $sql=$dbksfV3->query();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
}//if(isset($_POST['pouNb'])){



// -- descriptions des items -- //
if(isset($_POST['itDNb']) AND $_POST['itDNb']>0 ){
    $dbksfV3->sql->clear();
    $sqlOp='';
    foreach($_POST as $key => $value){
        $lout='';
        $sql='';

        $cmd=substr($key,0,11);            // on recupere la cmd sans la refId
        $id= substr($key,11,strlen($key));    // on recupere id en enlevant de  la cmd (tailleFixe)

        switch($cmd){
            case 'itDNameEdit':$sql='nom="'.$value.'"';$lout.=" nom: $value";break;
            case 'itDOrgaEdit':$sql='diO="'.$value.'"';$lout.=" Txtorga: $value";break;
            case 'itDJoueEdit':$sql='diJ="'.$value.'"';$lout.=" TxtJoueur: $value";break;
        }
        if($sql!=""){
            $sqlOp.='UPDATE '.TBLPREFIXE."itemsDescriptions Set $sql WHERE `nom`='$id';\n";
            logAdd(0,  'orga'    ,104,"l'item N°$id a été modifié: $lout");// a destination du MJ
        }
    }
    $dbksfV3->sql->setOPERATION($sqlOp);$sql=$dbksfV3->query();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
}//if(isset($_POST['deONb'])){




// =========================== //
// = edition des personnages = //
//       via orgas-persos    = //
//et perso-fiche(partie orga)= //
// =========================== //

// - edition des personnages : nomPrenom - //
if(isset($_POST['editPerNo'])){

    $dbksfV3->sql->clear();

    $id=(int)$_POST['perNo'];
    $per_etat=(int)$_POST['per_etat'];
    $prenom=$_POST['perPrenEdit'];
    $nom=$_POST['perNameEdit'];
    $lout=" nom: $nom prenom: $prenom";

    logAdd($id,  'perso'    ,220,"le personnage N°$id a été modifié: $lout");// laissé activé car cela concerne le perso
    $sqlOp='UPDATE '.TBLPREFIXE."personnages Set per_etat= $per_etat ,per_prenom = '$prenom',per_nom = '$nom' WHERE per_id=$id;\n";
    $dbksfV3->sql->setOPERATION($sqlOp);$sql=$dbksfV3->query();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
}//if(isset($_POST['editPerNo'])){


function selectOptionEtat($per_etat){
    global $per_etatTxt;
    $o='';
    $etatNb=count($per_etatTxt);// nombre d'etat d'un  perso
    for ($etatNu=0;$etatNu<$etatNb;$etatNu++){
        $selected=($per_etat===$etatNu)?' selected class="selected"':'';
        $o.='<option value="'.$etatNu.'" title="'.$per_etatTxt[$etatNu].'"'.$selected.'> '.$per_etatTxt[$etatNu].'</option>'."\n";
    }
    return $o;
}


// =========================== //
// = edition d'UN personnage = //
//      via perso-fiche        //
//        (fonctions orga)     //
// =========================== //


// - edition de la civilité d'un personnage - //
if(isset($_POST['perCiviliteEdit']) AND ISORGA===1){
	//echo'perCiviliteEdit<br>';
	$dateN  =$dbksfV3->sql->formatChamp($_POST['perDateN'],0,1);
	$sexe   =$dbksfV3->sql->formatChamp($_POST['perSexe'],0,1);
	$adresse=$dbksfV3->sql->formatChamp(addslashes($_POST['perAdresse']),0,1);
	$villeId=$dbksfV3->sql->formatChamp(addslashes($_POST['perVilleId']),0,1);
	$osmNode=$dbksfV3->sql->formatChamp($_POST['perOsmNode'],0,1);
	$msgCode=$dbksfV3->sql->formatChamp($_POST['perMsgCode'],0,1);


	$sqlOp="UPDATE '.TBLPREFIXE.'personnages Set per_dateN= $dateN, per_sexe=$sexe, per_adresse=$adresse, per_villeId=$villeId, per_osm_node=$osmNode, per_msg_code=$msgCode";
	$sqlOp.=' WHERE per_id='.PERSONO;
	$dbksfV3->sql->setOPERATION($sqlOp);$sql=$dbksfV3->query();$dbksfV3->queryClose();
	if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.$sql.'</div>';
	unset($sql,$sqlOp,$dateN,$sexe,$adresse,$ville,$osmNode);
	//logAdd(PERSONO,  'perso',220,);
}


// ============================== //
// - (des)activation des caracs - //
// -      via perso-fiche       - //
// ============================== //
// -------------------------------- //
// - (des)activation  d'un talent - //
// -------------------------------- //
if(isset($_POST['setTalentNo'])){
	$talId=$_POST['setTalentNo'];
	$dbksfV3->sql->clear();
	$dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'perTalActif` (`perId`, `talId`) VALUES ('.PERSONO.", $talId)");
	$sql=$dbksfV3->query();$dbksfV3->queryClose();
	if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.$sql.'</div>';
	logAdd(PERSONO,  'perso',204,"N°$talId ");
}

if(isset($_POST['unsetTalentNo'])){
	$talId=$_POST['unsetTalentNo'];
	$dbksfV3->sql->clear();
	$dbksfV3->sql->setOPERATION('DELETE FROM `'.TBLPREFIXE.'perTalActif` WHERE `perId`='.PERSONO." AND `talId`= $talId");
	$sql=$dbksfV3->query();$dbksfV3->queryClose();
	if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.$sql.'</div>';
	logAdd(PERSONO,  'perso',205," N°$talId ");
}

// ------------------------------- //
// - (des)activation d'un reseau - //
// ------------------------------- //
if(isset($_POST['setReseauNo'])){
	$resId=$_POST['setReseauNo'];
	$dbksfV3->sql->clear();
	$dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'perResActif` (`perId`, `resId`) VALUES ('.PERSONO.", $resId)");
	$sql=$dbksfV3->query();$dbksfV3->queryClose();
	if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.$sql.'</div>';
	logAdd(PERSONO,  'perso',206," N°$resId ");
}

if(isset($_POST['unsetReseauNo'])){
	$resId=$_POST['unsetReseauNo'];
	$dbksfV3->sql->clear();
	$dbksfV3->sql->setOPERATION('DELETE FROM `'.TBLPREFIXE.'perResActif` WHERE `perId`='.PERSONO.' AND `ResId`= '.$resId);
	$sql=$dbksfV3->query();$dbksfV3->queryClose();
	if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.$sql.'</div>';
	logAdd(PERSONO,  'perso',207," N°$resId ");
}


// =========================== //
// = edition des personnages = //
//       via orgas-persos    = //
//       (fonctions orga)    = //
// =========================== //
if(isset($_POST['orgPouEdit'])){
	$pouId=$_POST['orgPouEdit'];

	if(isset($_POST['setPouvoir'])){
		$dbksfV3->sql->clear();
		$dbksfV3->sql->setOPERATION('INSERT INTO `'.TBLPREFIXE.'perPouActif` (`perId`, `pouId`) VALUES ('.PERSONO.", $pouId)");
		$sql=$dbksfV3->query();$dbksfV3->queryClose();
		if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
		logAdd(PERSONO,  'perso',208," N°$pouId.");
	}

	if(isset($_POST['unsetPouvoir'])){
		$dbksfV3->sql->clear();
		$dbksfV3->sql->setOPERATION('DELETE FROM `'.TBLPREFIXE.'perPouActif` WHERE `perId`='.PERSONO.' AND `pouId`= '.$pouId);
		$sql=$dbksfV3->query();$dbksfV3->queryClose();
		if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
		logAdd(PERSONO,  'perso',209," N°$pouId.");
	}
}


// ------------------------------------ //
// - edition des personnages :DEC/INC - //
// via orgas-persos(per_id) ou perso-fiche(PERSONO)
// ------------------------------------ //
if(isset($_GET['per_id'])){
	$per_id=$_GET['per_id'];
	//$dbksfV3->sql->clear();
	if(isset($_GET['edit'])){
		$edit=$_GET['edit'];
		switch($edit){

			// - autorisation - //
			case 'aut_isMesOn':
				logAdd($per_id,  'perso'    ,230);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_isMes=1 WHERE per_id='.$per_id);
				break;
			case 'aut_isMesOff':
				logAdd($per_id,  'perso'    ,231);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_isMes=0 WHERE per_id='.$per_id);
				break;
			case 'aut_isActOn':
				logAdd($per_id,  'perso'    ,232);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_isAct=1 WHERE per_id='.$per_id);
				break;
			case 'aut_isActOff':
				logAdd($per_id,  'perso'    ,233);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_isAct=0 WHERE per_id='.$per_id);
				break;
			case 'aut_isRecOn':
				logAdd($per_id,  'perso'    ,234);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_isRec=1 WHERE per_id='.$per_id);
				break;
			case 'aut_isRecOff':
				logAdd($per_id,  'perso'    ,235);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_isRec=0 WHERE per_id='.$per_id);
				break;
			case 'aut_isEchOn':
				logAdd($per_id,  'perso'    ,236);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_isEch=1 WHERE per_id='.$per_id);
				break;
			case 'aut_isEchOff':
				logAdd($per_id,  'perso'    ,237);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_isEch=0 WHERE per_id='.$per_id);
				break;
			case 'aut_isDecOn':
				logAdd($per_id,  'perso'    ,238);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_isDec=1 WHERE per_id='.$per_id);
				break;
			case 'aut_isDecOff':
				logAdd($per_id,  'perso'    ,239);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_isDec=0 WHERE per_id='.$per_id);
				break;

			// - caracs - //

			case 'procheDispoInc':
				logAdd($per_id,  'perso'    ,210);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_procheDispo=per_procheDispo+1 WHERE per_id='.$per_id);
				break;
			case 'procheDispoDec':
				logAdd($per_id,  'perso'    ,211);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_procheDispo=per_procheDispo-1 WHERE per_id='.$per_id.' AND per_procheDispo>0');
				break;
			case 'procheMaxInc':
				logAdd($per_id,  'perso'    ,212);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_procheMax=per_procheMax+1 WHERE per_id='.$per_id);
				break;
			case 'procheMaxDec':
				logAdd($per_id,  'perso'    ,213);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_procheMax=per_procheMax-1 WHERE per_id='.$per_id.' AND per_procheMax>0');
				break;

			case 'richesseDispoInc':
				logAdd($per_id,  'perso'    ,214);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_richesseDispo=per_richesseDispo+1 WHERE per_id='.$per_id);
				break;
			case 'richesseDispoDec':
				logAdd($per_id,  'perso'    ,215);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_richesseDispo=per_richesseDispo-1 WHERE per_id='.$per_id.' AND per_richesseDispo>0');
				break;

			case 'richesseMaxInc':
				logAdd($per_id,  'perso'    ,216);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_richesseMax=per_richesseMax+1 WHERE per_id='.$per_id);
				break;
			case 'richesseMaxDec':
				logAdd($per_id,  'perso'    ,217);
				$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'personnages Set per_richesseMax=per_richesseMax-1 WHERE per_id='.$per_id.' AND per_richesseMax >0;');
				break;

			case 'scoreInc':
				if(isset($_GET['pouNo'])){
					$pouNo=$_GET['pouNo'];
					logAdd($per_id,  'perso'    ,0,'Vous avez gagné 1 point dans le pouvoir n°'.$pouNo);
					$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'perPouActif Set score=score+1 WHERE '."perId=$per_id AND pouId=$pouNo");
				}
				break;
			case 'scoreDec':
				if(isset($_GET['pouNo'])){
					$pouNo=$_GET['pouNo'];
					logAdd($per_id,  'perso'    ,0,'Vous avez perdu 1 point dans le pouvoir n°'.$pouNo);
					$dbksfV3->sql->setOPERATION('UPDATE '.TBLPREFIXE.'perPouActif Set score=score-1 WHERE '."perId=$per_id AND pouId=$pouNo AND score>0");
				}
				break;
		}//switch
	}
	$sql=$dbksfV3->query();	$dbksfV3->queryClose();
	if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

}//if(isset($_GET['per_id']))

