<?php
// =========================== //
// = edition d'UN personnage = //
//      via perso-fiche        //
// =========================== //

// - edition de la note d'un personnage - //
if(isset($_POST['perNoteEdit'])){
    $note=addslashes($_POST['perNoteData']);
    $sqlOp='UPDATE '.TBLPREFIXE.'personnages Set `per_note`= "'.$note.'" WHERE `per_id`='.PERSONO.';';
    $dbksfV3->sql->clear();$dbksfV3->sql->setOPERATION($sqlOp);$sql=$dbksfV3->query();$dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    logAdd(PERSONO,  'perso',220,$note);
    unset($sqlOp,$note);
}

// - edition d'un pouvoir d'un perso - //
if(isset($_POST['perPouEdit'])){
    for ($pouId=1;$pouId<=POUNB;$pouId++){
        if(isset($_POST['perPouNameEdit'.$pouId])){
            $pouNameEdit=addslashes($_POST['perPouNameEdit'.$pouId]);
            $pouNoteEdit='';

        $sqlOp='UPDATE `'.TBLPREFIXE."perPouActif` Set `nomP`= '$pouNameEdit', `note` = '$pouNoteEdit'";
        $dbksfV3->sql->setWHERE('(`perId`= '.PERSONO." AND `pouId` = $pouId)");
        $dbksfV3->sql->setOPERATION($sqlOp);$sql=$dbksfV3->query();$dbksfV3->queryClose();
        if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.$sql.'</div>';
        logAdd(PERSONO,  'perso',221,"$pouNameEdit");
        }
        unset($sqlOp);
    }
}


// ============================== //
// = phase d'ACTION / RECHERCHE = //
// ============================== //

// - Nb d'action q'un perso a fait -//
function perso_phaseCpt($phaseType){
    global $dbksfV3;
    $dbksfV3->sql->clear();
    $dbksfV3->sql->setOPERATION('SELECT ACpt,RCpt FROM '.TBLPREFIXE.'phasesCpt WHERE perId='.PERSONO);
    $sql=$dbksfV3->query();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';

    // --  mise en vars --- //
    $ACpt=$RCpt=0;
    while ($dbksfV3->fetch()){$ACpt=$dbksfV3->lignes['ACpt'];$RCpt=$dbksfV3->lignes['RCpt'];}
    $dbksfV3->queryClose();

    switch ($phaseType){
        case 3:return $ACpt;break;
        case 4:return $RCpt;break;
    }
}

// -- phase d'action: traitement formulaire d'action -- //

if(isset($_POST['sendPhaseAR'])){
    $phaseType=$_POST['phaseType'];

    $msgType='A';if ($phaseType === 4)$msgType='R';
    $tag='Action';    if ($phaseType === 4)$tag='Recherche';
    $TAG='ACTION';    if ($phaseType === 4)$TAG='RECHERCHE';
    $phaseNb=PHASE_A_NB;  if ($phaseType === 4)$phaseNb=PHASE_R_NB;
    $carMAX=ACTIONCARMAX; if ($phaseType === 4)$carMAX=RECHERCHECARMAX;

    // --- verifier que le perso a toujours 1 A/R disponible --- //
    $phaseCpt= perso_phaseCpt($phaseType);
    //if(ISDEV===1)echo gestLib_inspect('$phaseCpt',$phaseCpt);
    $dispo=$phaseNb - $phaseCpt;
    if($dispo>0){
    
        // --- recuperer la description Orga de la phase --- //
        $pha_id=$_POST['pha_id'];
        $MJID=$_POST['MJID'];
        $phase=new gestLigne($dbksfV3,TBLPREFIXE.'planning',$pha_id,'pla_id',['SELECT' => 'pla_deJ',['clear'=>0]]);
        if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">/* Chargement du personnage */'.ln2br($phase->legralPDO->sql->getSQL()).'</div>';
        $deJ=addslashes($phase->get('pla_deJ'));
        if(empty($deJ))$deJ=' du '.date('d/m/Y H:i');// si pas de description pour les joueurs alors on met la date et heure du jours

        // --- envoie du message au MJ de la ville --- //
        $sqlOp='';
        $sujet=" $TAG: $deJ :";
        //$message=addslashes(substr($_POST['message'],0,$carMAX)); // la taille reeldu message  peut etre plus long a cause des ajout de slash. SDonc ne pas limiter
        $message=addslashes($_POST['message']);
        $sqlOp.='INSERT INTO `'.TBLPREFIXE.'messageries` '
            .    '(`msg_type`,`msg_login`,`msg_de_perId`,`msg_pour_perId`,`msg_sujet`,`msg_message`)'
            .    'VALUES("'.$msgType.'","'.LOGIN.'",'.PERSONO.",$MJID,'$sujet','$message');\n";

        // --- incrementation du nombre de message d'action --- //
        $sqlOp.='CALL '.TBLPREFIXE.'phaseCptInc('.PERSONO.",$phaseType);";

        // -- execution du sql  -- //
        $dbksfV3->sql->clear();$dbksfV3->sql->setOPERATION($sqlOp); $sql=$dbksfV3->query();    $dbksfV3->queryClose();
        if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
        unset($sqlOp);
    }//if(PHASE_A_NB-$nb<=0)
}//if ( isset($_POST['sendPhaseAR']))


// =============== //
// = phase d'A/R = //
// = formulaires = //
// =============== //
function phaseForm($phaseType){
    $tag='Action';    if ($phaseType == 4)$tag='Recherche';
    $TAG='ACTION';    if ($phaseType == 4)$TAG='RECHERCHE';
    $phaseNb=PHASE_A_NB;    if ($phaseType == 4)$phaseNb=PHASE_R_NB;
    $carMAX=ACTIONCARMAX;    if ($phaseType == 4)$carMAX=RECHERCHECARMAX;

    // - chargement de toutes les phases (A ou R) en cours - //
    $phases=new gestTable('ksfv3',TBLPREFIXE.'planning','pla_id'
    ,[
        'SELECT'=> '*'
        ,'WHERE' => 'pla_villeId='.PER_VILLEID.' AND pla_type ='.$phaseType.' AND `pla_debut` <= NOW() AND  NOW() <= `pla_fin` '
//        ,'JOIN'=>'JOIN '.TBLPREFIXE.'planningType ON '.TBLPREFIXE.'planning.pla_type = '.TBLPREFIXE.'planningType.plt_id'
        ,'ORDERBY'=>'pla_debut'
        ,'clear'  => 0
    ]
);
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($phases->dbTable->sql->getSQL()).'</div>';
    //echo $phases->tableau();

    echo"<a name='$tag'></a><h2>$TAG</h2>";

    // - deja effectuer ? - //
    $phaseCpt=perso_phaseCpt($phaseType);//Nb de phase (selon type )fait PAR le personnage

    $dispo=$phaseNb - $phaseCpt;

    if($dispo>0)echo '<div class="txtcenter">';

    if($dispo<1)    {echo "<div class='txtcenter'>Vous n'avez plus de $tag disponible.</div>";}
    elseif($dispo<2){/*echo "Il vous reste $dispo $tag<br>";*/}
    else        {echo "Il vous reste $dispo $tag sur $phaseNb $tag".'s<br>';}

    if($dispo>0){
        echo "Quelle est votre $tag?<br>";
        echo '<form method="POST" action="?'.ARIANE_AGORIA.'#'.$tag.'">';

        //<!--destinataire(s): MJ<br-->
        echo'<input type="hidden" name="MJID" value="'.MJID.'" />';

        // - Choix de l'action - //
        echo'<input type="hidden" name="phaseType" value="'.$phaseType.'" />';
        echo 'Sujet: ';

        //if(PHASE_A_NB===1){
        //    $deJ=$phase['pla_deJ'];
        //    if(empty($deJ))$deJ='ACTION du '.date('d/m/Y H:i');// si pas de description pour les joueurs alors on met la date et heure du jours
        //}
        //else
//        if(PHASE_A_NB>1){
            echo '<select name="pha_id">';
            foreach($phases->get() as $pha_id => $phase){
                $deJ=$phase['pla_deJ'];
                if(empty($deJ))$deJ="$TAG du ".date('d/m/Y H:i');// si pas de description pour les joueurs alors on met la date et heure du jours
                echo "<option value='$pha_id'>$deJ</option>";
                //echo "$deJ<br>";
            }
            echo '</select><br>';
//        }

        echo 'caract&egrave;res:<span id="msgAPhase_Counter">0</span>/'.$carMAX;
//        echo ' (<a href="http://www.compteurs.info/caracteres.html" target="compteCar">compteur externe de caractères</a>) ';
        $span="'msgAPhase_Counter'";
        echo '<textarea onkeyup="showCountCar(this,'.$span.','.$carMAX.');"  name="message" required="required" class="msg_typeA" maxlenght="'.$carMAX.'"></textarea>';

        echo'<input type="submit" name="sendPhaseAR" value="Go!" title="Envoyer votre message. Attention ne peut être annulé!" />';
        echo'</form>';
        echo '</div>';// class="txtcenter">';
    }//if($dispo>0){

//}//if(PHASE_A_NB>0){
}// function formPhase($phaseType)


// ========================================== //
// = envoyer un mail en copie des meassages = //
// ========================================== //

function msg2mail($dest_perId,$sujet,$message,$msg_type){

    // -- on recupere le mail du joueur-destinataire -- //
    $to    = getPersoMail($dest_perId);
    
    // tester si pas de mail

    //$from  = 'postmaster@legral.fr';
    $from  = 'noreply@legral.fr';

    $JOUR  = date("Y-m-d");
    $HEURE = date("H:i");
             
    $headers  = "MIME-Version: 1.0 \n";
    $headers .= "Content-type: text/html; charset=utf8 \n";
    $headers .= "From: $from  \n";
    $headers .= "Reply-To: $from \n";
    $headers .= "Disposition-Notification-To: $from  \n";
    // Message de Priorit&eacute; haute
    // -------------------------
    //$headers .= "X-Priority: 1  \n";
    //$headers .= "X-MSMail-Priority: High \n";

    $mail_Data = '';

    $mail_Data .= '<html><head>'.$sujet.'</head>';
    $mail_Data .= '<body>';
    $mail_Data .= '<img src="https://legral.fr/agoria-engine/v0.2.3/0img/logoagoria.png" alt="logo Agoria"/> <br>'." \n";
    $mail_Data .= "Type du message: $msg_type<br> \n";
    $mail_Data .= "Date du message: $JOUR $HEURE<br> \n";
    $mail_Data .= "$message<br> \n";
    $mail_Data .= '<a href="https://legral.fr/agoria/stable/?ksfv3=connect">conn&eacute;ctez-vous!</a><br>';
    $mail_Data .= 'Ne pas r&eacute;pondre &agrave; ce mail.<br>';
    $mail_Data .= 'La fonction d\'envoie de mail est expérimentale';
    $mail_Data .= '</body></html>';

    $CR_Mail = TRUE;
    $CR_Mail = @mail ($to, $sujet, $mail_Data, $headers);
    if($CR_Mail===FALSE)    echo " Erreur lors de l'envoie du mail: $to<br>";
    else            echo'<div class="txtcenter">Votre message a &eacute;t&eacute; envoy&eacute; vers le mail de votre correspondant.</div>';
}//function msg2mail()


// ============================================ //
// - envoyer le message au(x) destinataire(s) - //
// - renvoie 1 si le message n'a pas ete envoyé //
// ============================================ //
function msgSend(){
if (isset($_POST['send']) AND $_POST['send']=='envoyer'){
    global $dbksfV3;
    // - validation du destinataire - //
    if(empty($_POST['destinataires_perId'])){echo '<div class="notewarning">Aucun destinataire s&eacute;l&eacute;ctionn&eacute;.</div>';return 1;}

    // - validation des champs - //
    // les datas proviennent de msgForm OU phaseForm ($_POST[])
    $msg_type='N';
    if(isset($_POST['msg_type']))$msg_type=$_POST['msg_type'];
    $msg_type=$dbksfV3->sql->formatChamp($msg_type,0,1);
    
    $msg_de_perId=(int)$_POST['de_perId'];
    //$msg_de_txt=isset($_POST['de_txt'])?addslashes($_POST['de_txt']):NULL;
    $sujet=addslashes($_POST['sujet']);
    $message=addslashes($_POST['message']);

    // - orgas - //
    // -  les param orgas sont changés dans functions-orga.php - //
    $codeD=isset($_POST['codeD'])?addslashes($_POST['codeD']):NULL;

    // -  pour chaque destinataire - //
    $sql='';
    foreach($_POST['destinataires_perId'] as $dest_perId){
        // -- envoie du message -- //
        $sql.='INSERT INTO `'.TBLPREFIXE.'messageries` '
            //.    '(msg_type,msg_login,`msg_de_perId`,`msg_de_txt`,`msg_pour_perId`,`msg_sujet`,`msg_message`)'
            //.    "VALUES('$msg_type','".LOGIN."',$msg_de_perId,'$msg_de_txt',$dest_perId,'$sujet','$message' );\n";
            .    '(msg_type,msg_login,`msg_de_perId`,`msg_pour_perId`,`msg_sujet`,`msg_message`)'
            .    "VALUES($msg_type,'".LOGIN."',$msg_de_perId,$dest_perId,'$sujet','$message' );\n";
    }

    // -- envoie du/des message(s) -- //
    $dbksfV3->sql->clear();    $dbksfV3->sql->setOPERATION($sql); $sql=$dbksfV3->query();     $dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';


    // - recuperer sql lastId - //
    $dbksfV3->sql->clear();    $dbksfV3->sql->setOPERATION('SELECT LAST_INSERT_ID() AS lastId;'); $sql=$dbksfV3->query();
    while ($dbksfV3->fetch()){$lastId=$dbksfV3->lignes['lastId'];}
    $dbksfV3->queryClose();
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($sql).'</div>';
    //if(ISDEV===1)echo gestLib_inspect('$lastId',$lastId);

    // - mise a jours des metaDonnees - //
    msgSetMetaDonnees($lastId);

    // - envoie d'un mail à chaque destinataires - //

    foreach($_POST['destinataires_perId'] as $dest_perId){
        msg2mail($dest_perId,$sujet,$message,$msg_type);
    }
    return 0;
}
}//function msgSend()


// ===================================== //
// = formulaire d'envoie ou de reponse = //
// ===================================== //
function msgForm(&$destinataires){

    if(PER_ISMES===0){echo '<div class="notewarning">Vous êtes dans l\'impossibilité d\'envoyer des messages.</div>';return '';}

    echo'<h2 class="h2">envoyer un message</h2><a name="msgForm"></a>';

    // le $_GET provient de repondre
    // le $_POST provient de msgForm
    define ('DESTID',isset($_GET['destId'])?$_GET['destId']:0);
    define ('SUJET',isset($_GET['sujet'])?$_GET['sujet']:'');
    
    // -- type du message -- //
    $msg_type='N';
    if(isset($_GET['msg_type']))$msg_type=$_GET['msg_type'];
    //if(isset($_POST['msg_type']))$msg_type=$_POST['msg_type'];

    $msgCSS='msg_type';
    if($msg_type!='N')$msgCSS='msg_type'.$msg_type;
    ?>
    <div class="msg_sendBox">
        <form method="POST" action="?<?php echo ARIANE_AGORIA?>#msgForm">
        <div class="txtcenter">
        de <input type="hidden" name="de_perId" value="<?php echo PERSONO?>" /><?php echo $destinataires->get(PERSONO,'perPN')?>
        <?php
        /*
        if(ISMJ===1 AND FONCTIONAVANCE===1){
            $o='';
            $o.='(sera emmis par):<br>';
            $o.='<select name="de_perId">'."\n";
            foreach($destinataires->get() as $_perso_id => $_perso){
                $selected=($_perso_id==PERSONO)?' selected="selected" class="selected"':'';
                if(isset($_GET['emmetteur'])){
                    $emmetteur=$_GET['emmetteur'];
                    $selected=($_perso_id==$emmetteur)?' selected="selected" class="selected"':'';
                }
                $o.='<option value="'.$_perso_id.'" title="'.$_perso['perPN'].'"'.$selected.'> '.$_perso['perPN'].'</option>'."\n";
            }
            $o.='</select><br>';
            $o.='nom vu par le destinataire<br><input name="de_txt" title="(facultatif)Le destinataire verra ce texte à la place du nom ci dessus" />';
            echo "<span class='orgas'$o</span>";
        }
        else{
            echo '<input type="hidden" name="de_perId" value='.PERSONO.' />'.$destinataires->get(PERSONO,'perPN');
        }
        */
        ?>
    destinataire(s):<br>
    (S&eacute;l&eacute;ction multiples: Ctrl + clics)<br>
    <?php
    if (ISMJ===1)echo '<div class="orgas">Ne sont affich&eacute;s que les persos en jeu</div>';
    $persoEtat=NULL;
    $o='<select size="5" name="destinataires_perId[]" multiple="multiple" required="required">';

    foreach($destinataires->get() as $_perso_id => $_perso){
        //if($_perso_id<=0)continue;
        $persoEtat=$_perso['per_etat'];
        $selected=($_perso_id==DESTID)?' selected="selected" class="selected"':'';
        $msgSelfCSS=$_perso_id==PERSONO?'msg_self':'';
        $o.='<option class="'.$msgSelfCSS.'" value="'.$_perso_id.'" title="'.$_perso['perPN'].'"'.$selected.'> '.$_perso['perPN'].'</option>'."\n";
    }
    echo $o;
?>
        </select>
    </div>

    <div class="clear"></div>
    sujet: <input name="sujet" required="required" class="<?php echo $msgCSS ?>" size="50"  maxlenght="50" value="<?php echo SUJET?>"/><br>

<?php
    // - code decouverte - //
    if(ISMJ===1)echo '<span class="orgas">Code d&eacute;couverte associ&eacute;: <input name="msg_codeD"></span><br>';

    // - Type du message - //
    echo 'Type: ';

    $checked=$msg_type==='N'?" checked":'';
    $css=$checked===''?'':'msg_type';
    echo '<span class="'.$css.'">';
    echo '<input id="sendMsgtypeN" type="radio" name="msg_type" value="N"'.$checked.'>';
    echo '<label for="sendMsgtypeN"> Normal</label>';
    echo '</span>';

    if(ISMJ===1){
        $checked=$msg_type==='A'?" checked":'';
        $css=$checked===''?'':' msg_type'.$msg_type;
        echo ' <span class="orgas'.$css.'">';
        echo '<input id="sendMsgtypeA" type="radio" name="msg_type" value="A"'.$checked.'>';
        echo '<label for="sendMsgtypeA">Action</label>';
        echo '</span>';

        $checked=$msg_type==='R'?" checked":'';
        $css=$checked===''?'':' msg_type'.$msg_type;
        echo ' <span class="orgas'.$css.'">';
        echo '<input id="sendMsgtypeR" type="radio" name="msg_type" value="R"'.$checked.'>';
        echo '<label for="sendMsgtypeR">Recherche</label>';
        echo '</span>';
    }
    $checked=$msg_type==='H'?" checked":'';
    $css=$checked===''?'':'msg_type'.$msg_type;
    echo '<span class="'.$css.'">';
    echo ' <input id="sendMsgtypeH" type="radio" name="msg_type" value="H"'.$checked.'>';
    echo '<label for="sendMsgtypeH"> HRP</label>';
    echo '</span>';

    echo '<br>';

    echo 'message (<span id="msgG_Counter">0</span>/'.MSGCARMAX.'';
    //echo ' - <a href="http://www.compteurs.info/caracteres.html" target="compteCar">compteur de caractère</a>';
    echo '):';
    echo "<textarea  onkeyup=\"showCountCar(this,'msgG_Counter',".MSGCARMAX.")\" name=\"message\" required=\"required\" class=\"$msgCSS\" size=\"".MSGCARMAX."\" maxlenght=\"".MSGCARMAX."\" ></textarea>";

    echo '<input type="submit" name="send" value="envoyer" />';
    echo '</form>';
    echo '</div><!-- msg_sendBox -->';
}//function msgForm($persos)


// ===================== //
// = messagerie filtre = //
// ===================== //
function msgFiltreSet(){
    $msgFiltreN=1;
    $msgFiltreA=$msgFiltreR=$msgFiltreH=1;

    // - session ou db - //
    if(isset($_SESSION['msgFiltreN']))$msgFiltreN=$_SESSION['msgFiltreN'];
    if(isset($_SESSION['msgFiltreA']))$msgFiltreA=$_SESSION['msgFiltreA'];
    if(isset($_SESSION['msgFiltreR']))$msgFiltreR=$_SESSION['msgFiltreR'];
    if(isset($_SESSION['msgFiltreH']))$msgFiltreH=$_SESSION['msgFiltreH'];

    // - surcharge par post - //
    if(isset($_POST['msgFiltre'])){
        $msgFiltreN=(isset($_POST['msgFiltreN']))?1:0;
        $msgFiltreA=(isset($_POST['msgFiltreA']))?1:0;
        $msgFiltreR=(isset($_POST['msgFiltreR']))?1:0;
        $msgFiltreH=(isset($_POST['msgFiltreH']))?1:0;
    }

    // - surcharge par phase - //
    if (PHASE_A_NB>0) $msgFiltreA=1;
    if (PHASE_R_NB>0) $msgFiltreR=1;

    // - sauver dans constante et (session ou db) - //
    define('MSGFILTREN',$msgFiltreN);$_SESSION['msgFiltreN']=MSGFILTREN;
    define('MSGFILTREA',$msgFiltreA);$_SESSION['msgFiltreA']=MSGFILTREA;
    define('MSGFILTRER',$msgFiltreR);$_SESSION['msgFiltreR']=MSGFILTRER;
    define('MSGFILTREH',$msgFiltreH);$_SESSION['msgFiltreH']=MSGFILTREH;
}


function msgFiltreForm(){
    msgFiltreSet();
    echo '<a name="msgFiltre"></a>';    
    echo '<form id="FmsgFiltre" method="POST" action="?'.ARIANE_AGORIA.'#msgFiltre">';
    echo '<input type="submit" name="msgFiltre" value="Filtrer"><br>';

    // - Normal - //
    $checked=MSGFILTREN===1?' checked':'';
    $cssType=$checked===''?'':' msg_typeN';
    echo ' <span class="msg_type'.$cssType.'" ><input type="checkbox" id="msgFiltreN" name="msgFiltreN"'.$checked.'>';
    echo ' <label for="msgFiltreN">Normal</label></span>';

    // - Action- //
    $disabled=$title=$style=$PHASE_A_NB='';
    if (PHASE_A_NB>0){
        $disabled=' disabled';
        $title=' title="Phase Action en cours"';
        $style=' style="cursor:default"';
        $PHASE_A_NB=':'.PHASE_A_NB;
    }
    $checked=MSGFILTREA===1?' checked':'';
    $cssType=$checked===''?'':' msg_typeA';
    echo ' <span class="msg_type'.$cssType.$style.'"><input type="checkbox" id="msgFiltreA" name="msgFiltreA"'.$checked.$disabled.$title.$style.'>';
    echo ' <label for="msgFiltreA"'.$title.$style.'>Action'.$PHASE_A_NB.'</label></span>';

    // - Recherche - //
    $disabled=$title=$style=$PHASE_R_NB='';
    if (PHASE_R_NB>0){
        $disabled=' disabled';
        $title=' title="Phase Recherche en cours"';
        $style=' style="cursor:default"';
        $PHASE_R_NB=':'.PHASE_R_NB;
    }
    $checked=MSGFILTRER===1?' checked':'';
    $cssType=$checked===''?'':' msg_typeR';
    echo ' <span class="msg_type'.$cssType.$style.'"><input type="checkbox" id="msgFiltreR" name="msgFiltreR"'.$checked.$disabled.$title.$style.'>';
    echo ' <label for="msgFiltreR"'.$title.$style.'>Recherche'.$PHASE_R_NB.'</label></span>';


    // - Hrp - //
    $checked=MSGFILTREH===1?' checked':'';
    $cssType=$checked===''?'':' msg_typeH';
    echo ' <span class="msg_type'.$cssType.'"><input type="checkbox"  id="msgFiltreH" name="msgFiltreH"'.$checked.'>';
    echo ' <label for="msgFiltreH">HRP</label></span>';
    echo '</form><br>';
}


// ============ //
// = echanges = //
// ============ //
// ======================== //
// - formulaire transaction execution - //
// &$preteur: persoBeneficiaire
// $emprunteurId: seulement l'id de l'emprenteur
// $disponibilites: tableau contenant les quantités a echanger pour chaque talent ou reseau
// $disponibilites['type']= 'talent' | 'reseau'
// $sql commande sql a "appender" ajoute le code sql pour les echange de talents ou reseaux
// ======================== //
function transactionTalRes(&$preteur,$emprunteurId,$disponibilites,&$sql,&$logTxt){

    //if(ISDEV==1)echo $preteur->tableau();
    //if(ISDEV==1)echo $talents->tableau();
    
    $type=$disponibilites['type'];    // 'talent','reseau'
    $errNo=0;
    //$toId=$emprunteurId;

    $sep='';//separateur de champs sql ',';
    $sqlPChamps=$sqlEChamps='';//champs sql du Preteur et de l'Empremteur
    $n=0;//nb de talent setter (utile pour la virgule des champs sql)

    for($i=1;$i<=TALNB;$i++){
        if($errNo!==0)continue;
        $dispo=$disponibilites[$i];
        //echo"dispoN° $i: dispoVal:$dispo<br>";

        if($dispo>0){
            if($n>0)$sep=',';

            $label="per_$type$i".'Dispo';


            // --- verification de la solvabilité du preteur --- //
            $Pdispo=$preteur->get($label);
            if($Pdispo<$dispo){
                // le preteur a moins que ce qu'il offre!
                // on a truander le POST ???
                $errNo=100+$i;// erreur d'insolvabilité
                echo "<div class='noteclassic'>vous n'etes pas solvable! C'est très bizard ca!!!</div>";
            }
            else{
                // -- Deduction du nb de dispo au préteur -- //
                $sqlPChamps.= "$sep$label=$label-$dispo";

                // --  Ajout du nb de dispo dans le talent de l'emprunteur -- //
                $sqlEChamps.= "$sep$label=$label+$dispo";

                // -- -- //
                $n++;
                $trNom=($type==='talent')?getTalentNom($i):getReseauNom($i);
                
                $logTxt.="$type: $trNom: $dispo ";
            }
        }
    }


    if($errNo!==0)return $errNo;

    if($n>0){
        // echo 'transaction en cours<br>';
        $sql.='UPDATE '.TBLPREFIXE."personnages SET $sqlPChamps WHERE per_id = ".PERSONO.';';
        $sql.='UPDATE '.TBLPREFIXE."personnages SET $sqlEChamps WHERE per_id = $emprunteurId;";
        // la variable &$sql est renvoyé modifier
    }

    return 0; // l'offre est vide
}//function transactionTalent(&$preteur,$emprunteurId,$talDispo,&$sql)

