<?php
// - affichage - //
global $dbksfV3,$gestLogins;
//if(ISDEV===1)echo gestLib_inspect(__FILE__.':'.__LINE__.':$arianeAgoria',$arianeAgoria);


// - chargement du joueur actif - //
// -- toutes les infos du joueurs sauf login et pwd (qui est gérer par gestLogins) -- //
$joueur=new gestLigne($dbksfV3,TBLPREFIXE.'joueurs',JOUNO,'jou_id');
$nom=$joueur->get('jou_nom');
$mail=$joueur->get('jou_mail');
$jouNote=$joueur->get('jou_note');
$persoNo=$joueur->get('jou_persoNo');

echo '<h1 class="h1">'.JOUNOM.'</h1>';

if(empty($joueur->get())) echo '<div class="notewarning">pas d\'entr&eacute;e dans la base de donn&eacute;e en tant que joueur. La fonction note ne fonctionnera pas.</div>';?>

<form id="logMail_change" method="POST" action="?<?php echo ARIANE_AGORIA?>#">
    <input type="submit" name="jouDataEdit" value="sauver" /><br>

    <label for="jouLoginData">Login:</label>
    <input id="jouLoginData" name="jouLoginData" type="text" value="<?php echo LOGIN?>" /><br>
    <b>ATTENTION NE VOUS TROMPEZ PAS!</b><br>
    (Vous devrez vous d&eacute;connecter/reconnecter)<br>
    <br>
    <label for="jouMailData">mail:</label>
    <input id="jouMailData" name="jouMailData" type="text" value="<?php echo $mail?>" required /><br>
    (Pour r&eacute;cup&eacute;rer votre mot de passe et recevoir les messages pas mail)<br>
</form>

<h2 class="h2">Changer le mot de passe</h2>
<form id="pwd_change" method="POST" action="?<?php echo ARIANE_AGORIA?>#">
    <input type="submit" name="jouPaswEdit" value="changer" /><br>
    <input name="jouPaswData" type="text" value="" placeholder="nouveau mot de passe" required />
<?php
if(isset($_POST['jouPaswEdit']))echo '<div class="noteclassic">Mot de passe modifi&eacute;</div>';
?>
</form>

<br>
<a name="noteDuJoueur"></a><h2 class="h2">Note du joueur (non visible par les mj)</h2>

<form id="FjouNote" method="POST" action="?<?php echo ARIANE_AGORIA?>#noteDuJoueur">
    <input type="submit" name="jouNoteEdit" value="sauver" /><br>
    <textarea id="jouNote" style="width:100%; min-height:2em;" name="jouNoteData" onkeyup="IDhautAuto(this.id);" value="<?php echo $jouNote?>"><?php echo $jouNote?></textarea>
</form>


<h2 class="h2">Les flux RSS</h2>
<p class="txtcenter">
<a class="rssLine" href="?rss=agoria"><img alt="logo RSS" src="<?php echo IMG_ROOT?>rss-logo-16x16.png" /> l'actualit&eacute; du moteur de jeu</a><br>
<a class="rssLine" href="?rss=messagerie&rssUUID=<?php echo getPersoRssUUID()?>"><img alt="logo RSS" src="<?php echo IMG_ROOT?>rss-logo-16x16.png" /> la boite aux lettres du personnage</a><br>
</p>


<?php
// - table des joueurs connectes - //
if(ISMJ===1){
    echo'<div class="orgas txtcenter">';
    echo'<h2 class="h2">joueurs connéctés (15 dernieres minutes)</h2>';

    $jc=new gestTable('ksfv3',TBLPREFIXE.'joueurs','jou_id'
            ,[
            'SELECT'=>'jou_id,jou_nom'
            ,'WHERE'=>'jou_ts + INTERVAL 900 SECOND >= CURRENT_TIMESTAMP'
            ]
    );
    foreach ($jc->get() as $noms)echo $noms['jou_nom'].' ';
    echo'</div>';

}//if (ISMJ)
?>
<br>
<?php
if(JOURNAL_SHOW===1){
    echo '<h1 class="h1">Journal</h1>';

    $addOrga=PERSONO<1?'(domaine="joueur" OR domaine="orga")':'domaine="joueur"';
    $logs= new gestTable('ksfv3',TBLPREFIXE.'logs','log_id'
    ,     [
        'SELECT'  =>'log_id,ts,domaine,fromLogin,toPersoNo,per_prenom,per_nom,actionNo,lox_texte,extra'
        ,'JOIN'   =>
            ' INNER JOIN '.TBLPREFIXE.'logTextes ON '.TBLPREFIXE.'logs.actionNo =   '.TBLPREFIXE.'logTextes.lox_id'
        .   ' INNER JOIN '.TBLPREFIXE.'personnages ON '.TBLPREFIXE.'logs.toPersoNo = '.TBLPREFIXE.'personnages.per_id'
            ,'WHERE'  => 'toPersoNo = '.PERSONO.' AND '.$addOrga
            ,'ORDERBY'=>'ts DESC'
        ,'clear'  => 0
        ]
    );
    if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($logs->dbTable->sql->getSQL()).'</div>';
    //if(ISDEV===1)echo $logs->tableau();

    echo '<a href="#joueurJournalBottom">Aller en bas de la liste</a>';
    echo '<div class="" style="max-height:25em;overflow:auto;">';

    foreach($logs->get() as  $_log){
        $ts=$_log['ts'];
        $domaine=$_log['domaine'];
        $orgaCSS=$domaine=='orga'?'orgas':'';
        $fromLogin=$_log['fromLogin'];
        $toPersoNo=$_log['toPersoNo'];
        $per_pn=$_log['per_prenom'].''.$_log['per_nom'];
        $actionNo=$_log['actionNo'];
        $texte=$_log['lox_texte'];
        $extra=$_log['extra'];
        //echo "<div>$ts $fromLogin [$domaine] $per_pn $texte $extra</div>";
        echo "<div class='$orgaCSS'>$ts de $fromLogin <!--pour $per_pn -->$texte $extra</div>";
    }
    unset($logs);
    echo '<a name="joueurJournalBottom"></a>';
    echo '</div>';
}//if(JOURNAL_SHOW===1){

if(ISADMIN===1){
    echo '<br><div class="devs txtcenter bold">';
    echo'<h2 class="h2">domaines</h2>';
    echo $gestLogins->showDomaines();

    echo '<h2 class="h2">attributs</h2>';
    echo $gestLogins->showAttributs();

    echo '<h2 class="h2">is?</h2>';
    if (ISFONDATEUR===1)echo 'FONDATEUR ';
    if (ISADMIN===1)echo 'ADMIN ';
    if (ISMJ===1)echo 'MJ ';
    if (ISJOUEUR===1)echo 'JOUEUR ';
    if (ISDEV===1)echo 'DEV ';
    echo '</div>';
}
?>
<br>
<!--h2 class="h2">Rapporter un bug</h2-->
<div id="footerDroit"><a href="?ksfv3=bugsToDo">Rapporter un bug</a></div>
