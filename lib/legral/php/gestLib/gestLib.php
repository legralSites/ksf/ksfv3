<?php
/*!******************************************************************
fichier: gestLib.php
auteur : Pascal TOLEDO
date de creation: 01 fevrier 2012
date de modification: 08 juin 2014
source: https://git.framasoft.org/legraLibs/gestlib
tuto:   https://legral.fr/intersites/lib/php/gestLib
depend de:
	* aucune
description:
	* 
*******************************************************************/
define ('GESTLIBVERSION','2.1.0-dev');
define ('SCRIPTDEBUT', microtime(TRUE));
// ***************** //
//  ** intersites ** //
// ***************** //

// -- Calcul du reseau -- //

//echo phpinfo();
define('SERVER_NAME',$_SERVER['SERVER_NAME']);define('SERVER_ADDR',$_SERVER['SERVER_ADDR']);
if	 ((substr(SERVER_ADDR,0,3)=='127')OR(SERVER_ADDR=='::1')){define('SERVER_RESEAU','LOCALHOST');}
elseif(substr(SERVER_ADDR,0,2)=='192'){define('SERVER_RESEAU','LAN');}
elseif(substr(SERVER_ADDR,0,2)=='10'){define('SERVER_RESEAU','VPN');}
else  {define('SERVER_RESEAU','INTERNET');}



//*************************************** //
// Gestion de textes multilangues pouvant //
// servir de gestionnaire d'erreurs       //
//*************************************** //
class gestTextes{
	private $langDefaut;
	private $lang;	// langue en cours
	private $no;	//index (numeric ou alphanumerique) du texte (ou de l'erreur)
	private $lastNo;//dernier no setter
	public $textes=array();//contient le tableau des langues

	function __construct($langDefaut='fr'){
		$this->langDefaut=$langDefaut;
		$this->lang=$this->langDefaut;
		$this->no=0;
		$this->lastNo=0;
		$this->textes=array();
	}

	function __destruct(){
	}

	function getLangue(){return $this->lang;}

	// - $force: force la creation - //
	function setLangue($lang='fr',$force=0){
		if( (!isset($this->textes[$lang])) OR ($force===1)){
			$this->textes[$lang]=array();
		}
		$this->lang=$lang;
	}

	function setNo($nu){$this->no=$nu;$this->lastNo=$nu;}
	function getNo(){return $this->no;}
	function getLastNo(){return $this->lastNo;}

	function setTexte($nu,$txt,$lang=NULL,$force=0){
		// - attention le tableau peut ne pas exister ! - //
		$lang=($lang===NULL)?$this->langDefaut:$lang;
		if( (!isset($this->textes[$lang][$nu])) OR ($force===1)){
			$this->textes[$lang][$nu]=$txt;
		}
	}

	function getTexte($nu=NULL,$lang=NULL){
		$nu=($nu===NULL)?$this->lastNo:$nu;
		$lang=($lang===NULL)?$this->langDefaut:$lang;
		if(isset($this->textes[$lang][$nu]))return $this->textes[$lang][$nu];
		return '';
	}
} // class gestTextes


// **************************** //
// CLASS de gestion d'attributs //
// **************************** //
class gestAttributs{
	//private $attrs;
	public $attrs;
	function __construct(){
	$this->attrs=array();
	}

	function set($key,$val,$force=1){
		if((!isset($this->attrs[$key])OR($force===1))){$this->attrs[$key]=$val;}
	}
	function _unset($key){
		//if(!(isset($this->attrs[$key])OR($force===1)))$this->attrs[$key]=$val;
		unset($this->attrs[$key]);
	}

	function get($key=NULL){
		if($key===NULL)return $this->attrs;
		if(isset($this->attrs[$key]))return $this->attrs[$key];
		return NULL;
	}


}

// **************************** //
// CLASS DE GESTION DES ERREURS
// **************************** //
//if (gettype($gestLib)=='NULL')	// protection gestLib
//{
class LEGRALERR{
	const ALWAYS=-1;//sera toujours afficher quelque soit le niveau atrtribuer pour la lib
	const NOERROR=0;
	const CRITIQUE=1;
	const DEBUG=2;
	const WARNING=3;
	const INFO=4;
	const ALL=5;
}
function LEGRALERR_toString($err){
	switch ($err){
		case LEGRALERR::NOERROR :return 'NOERROR';
		case LEGRALERR::CRITIQUE :return 'CRITIQUE';
		case LEGRALERR::DEBUG :return 'DEBUG';
		case LEGRALERR::WARNING :return 'WARNING';
		case LEGRALERR::INFO :return 'INFO';
		case LEGRALERR::ALWAYS :return 'ALWAYS';
	}
}

//********************************************
// CLASS DE GESTION DES ETATS
//********************************************
class LEGRAL_LIBETAT{
	const NOLOADED=0;
	const LOADING=1;
	const LOADED=2;
}
function LEGRAL_LIBETAT_toString($etat){
	switch ($etat)
		{
		case LEGRAL_LIBETAT::NOLOADED :return 'NOLOADED';
		case LEGRAL_LIBETAT::LOADING :return 'LOADING';
		case LEGRAL_LIBETAT::LOADED :return 'LOADED';
		}
}

//********************************************
// function gestLib_format()
// -  formate les fonctions gestLib_inspect, debugShow(),debugShowVar()
//********************************************
function gestLib_format($txt,$f='br'/*,$classe=''*/){
	switch($f){
		case '':case 'nobr':case NULL:return $txt;break;
		case 'ln':	$o="$txt\n";break;
//		case 'div':	$o="<div class='$classe'>$txt</div>";break;
//		case 'p':	$o="<p class='$classe'>$txt.</p>";break;
		case 'br':default: $o="$txt<br />";
		}
	return "$o";
}

//********************************************
// function gestLib_inspect() 
// - manipuler des variables independantes des libs - //
// - retourne les donnees d'une variable independante (nom,$var) au couleur de gestLib.css - //
// - si $varNom='' alors valeur seul - //
// - line,method: voir les info de debuggage - //
// http://php.net/manual/fr/language.constants.predefined.php
//********************************************
function gestLib_inspectOrigine($varNom,$var,$commentaire='',$metaDatas='',$f='br'/*,$classe=''*/){
	return  '<span class="gestLibVar">'.$commentaire.':'.$metaDatas.':'.$varNom.':</span>'
	.	'<div class="gestLib_inspectOrigine">'.gestLib_inspect($varNom,$var,$commentaire='',$metaDatas='',$f='br').'</div>';
}

//function gestLib_inspect
function gestLib_inspect($varNom,$var,$commentaire=0,$metaDatas='',$f='br'/*,$classe=''*/){
	$out='';
	$out.='<span class="gestLib_inspect">';
	$t=gettype($var);
        if ($metaDatas!=''){$out.="[<span class='gestLibInfo'>$metaDatas</span>]";}
        if ($commentaire!==0){$out.="<span class='gestLibInfo'>:$commentaire</span>&nbsp;";}
        if($varNom!='')$out.='<span class="gestLibVar">'.$varNom.'</span>='; // pas de nom preciser

	$t=gettype($var);

	//$out.='<span class="gestLibVal">';
	$out.="[$t]";

	switch($t){
		case'boolean': case'integer': case'double':
		        if ($var===TRUE)  {$out.='<span class="gestLibVal">TRUE strict</span>';}
        		if ($var===FALSE)  {$out.='<span class="gestLibVal">FALSE strict</span>';}
			$out.="<span class='gestLibVal'>$var</span>";
			break;
		case'string':
			$out.="'<span class='gestLibVal'>$var</span>'";break;
		case'NULL':
			$out.='<span class="gestLibValSpecial">NULL</span>';break;
		case'ressource':
			$out.='';break;
		case'array':
			$nb=count($var);
			if($nb>0){
				$out.="(array:$nb)<ul class='gestLibArray'>";
				foreach($var as $key => $value)$out.='<li style="list-style-type:decimal">'.gestLib_inspect($key,$value).'</li>';
				}
			$out.='</ul>';
                        break;

		case'object':
			$out.='(object:'.count($var).')<ol class="gestLibObject">';
			/// if(get_class_vars($var))$out.='.';	// renvoie les valeurs par defaut // err500
			if(!get_class_methods($var))$out.='<span class="gestLibValSpecial">Classe sans methode.</span>';

			foreach($var as $key => $value)$out.='<li style="list-style-type:upper-roman">'.gestLib_inspect($key,$value).'</li>';
			$out.='</ol>';

			break;
		case'unknow type':
			$out.='<span class="gestLibValSpecial">unknow type</span>';break;	
		default:
			$out.="(autre):$var";break;
	}

	if (empty($var)){$out.=' <span class="gestLibValSpecial">empty</span> ';}
	//$out.='</span>';
	$out.='</span><!--span class="gestLib_inspect"-->';
	return gestLib_format($out,$f/*,$classe*/);
}



//********************************************
// CLASS DE DONNEES
//********************************************
class gestLib_gestLib{
	public $nom=NULL;
	public $fichier=NULL;
	public $version=NULL;

	public $auteur='fenwe';
	public $site='http://legral.fr';
	public $git='https://git.framasoft.org/u/fenwe';
	public $description='';

	public $dur=0;
        public $deb=0;
        public $fin=0;
	public $etat=NULL;	//1: en cours de chargement;2: chargement terminer

	public $err_level=0;
	public $err_level_Backup=NULL;	//sauvegarde de l'etat

	function setEtat($etat){$this->etat=$etat;}
	function getDuree(){return number_format($this->dur,6);}

	function end(){
		$this->etat=LEGRAL_LIBETAT::LOADED;
		$this->fin=microtime(1);$this->dur=$this->fin - $this->deb;
	}

	function setErr($err){$this->err_level=$err;}
	function setErrLevelTemp($err){$this->err_level_Backup=$this->err_level;$this->err_level=$err_level;}
	function setErrLevelTemp_NOERROR(){$this->err_level_Backup=$this->err_level;$this->err_level=LEGRALERR::NOERROR;}
	function restoreErrLevel(){$this->err_level=$this->err_level_Backup;}


	function __construct($nom,$file,$version,$description=NULL){
		$this->deb=microtime(1);
		$this->err_level=LEGRALERR::NOERROR;
		$this->nom=$nom;
		$this->fichier=$file;
		$this->version=$version;
		$this->etat=1;
		$this->description=$description;
	}


	//function __toString() {return '';}

	// - renvoie le texte si le niveau d'erreur concorde - //
	function debugShow($level,$commentaire='',$metaDatas='',$txt='',$format='br',$classe=''){
		if($level>$this->err_level){return NULL;}
		$out='<span class="gestLibVar">'.$this->nom;
		$out.='{'.LEGRALERR_toString($level).'}';
		if ($commentaire!==''){$out.="($commentaire)";}
		if ($metaDatas!==''){$out.="[$metaDatas]";}
		$out.=':</span>';
		$out.='<span class="gestLibVal">'."$txt</span>";
		return gestLib_format($out,$format,$classe);
	}

	
	function debugShowVar($level,$commentaire,$metaDatas,$varNom,$var,$format='br',$classe=''){
		if ($level>$this->err_level){return '';}
		$out=gestLib_inspect($varNom,$var,$commentaire,$metaDatas,$format,$classe);
		return $out;
	}
} // class gestLib_gestLib


//********************************************
// CLASS GESTIONLIBRAIRIE
//********************************************
class gestionLibrairies{
	public $libs=array();	//tableau de libs
	public $erreurs;

	function __construct(){
		$this->erreurs=array();
	}


	function __toString(){
		$out='<ul class="gestlib">';
		foreach($this->lib  as $key => $value)  $out.="<li>$key= $value</li>";$out.="</ul></li>";
		$out.='</ul>';
		return $out;
	}
	

	function loadLib($nom,$file,$version,$description=NULL){
	//	if ( isset($this->libs[$nom]->nom) ){$this->libs[$nom]->nom=NULL;}//si deja charge

		$this->libs["$nom"]=new gestLib_gestLib($nom,$file,$version,$description);
		$this->erreurs["$nom"]=new gestTextes();
	}


	function tableau(){
		$out ='<table class="gestLib"><caption>Librairies PHP</caption>';
		$out.='<thead><tr><th>nom</th><th>version</th><th>etat</th><th>err level</th><th>durée</th><th>description</th><th>auteur</th> <th>git</th> </tr></thead>';
		foreach($this->libs as $key => $lib){
			//$lib= gestLib[index]
	//		$libNom=$lib->nom;
			$out.='<tr>';
			$out.='<td>'.$lib->nom.'</td>';
			$out.='<td>'.$lib->version.'</td>';
			$out.='<td>'.LEGRAL_LIBETAT_toString($lib->etat).'</td>';
			$out.='<td>'.LEGRALERR_toString($lib->err_level).'</td>';
			$out.='<td>'.$lib->getDuree().'</td>';
			$out.='<td>'.$lib->description.'</td>';
			$t=$lib->auteur;	$out.="<td><a target='exterieur' href='$lib->site'>$t</a></td>";
			$t=$lib->git;		$out.="<td><a target='git' href='$t'>$t</a></td>";
			$out.="</tr>\n";
			};

		$out.='</table>';
		return $out;
	}

	function libTableau(){return $this->tableau();}

	function setEtat($lib,$etat){$this->libs[$lib]->setEtat($etat);}
	function getDuree($lib)     {$this->libs[$lib]->getDuree();}
	function end($lib)          {$this->libs[$lib]->end();}
	function getLibError($lib)  {if(isset($this->libs[$lib]))return LEGRALERR_toString($this->libs[$lib]->err_level);}

	function debugShowOrigine($lib,$level,$commentaire='',$metaDatas='',$txt='',$format='br',$classe=''){
		$r ='<span class="gestLibVar">'.$lib.':'.$commentaire.':'.$metaDatas.'</span>';
		$r.='<div class="gestLib_inspectOrigine">'.$this->debugShow($lib,$level,$commentaire,$metaDatas,$txt,$format,$classe).'</div>';
		return $r;
	}
	function debugShow($lib,$level,$commentaire='',$metaDatas='',$txt='',$format='br',$classe=''){
		if(isset($this->libs[$lib]))return $this->libs[$lib]->debugShow($level,$commentaire,$metaDatas,$txt,$format,$classe);
		return '';
	}

	// - accee aux libs - //
	function debugShowVarOrigine($lib,$level,$commentaire='',$metaDatas='',$varNom='',$var='',$format='br',$classe=''){
		$r ='<span class="gestLibVar">'.$lib.':'.$commentaire.':'.$metaDatas.'</span>';
		$r.='<div class="gestLib_inspectOrigine">'.$this->debugShowVar($lib,$level,$commentaire,$metaDatas,$varNom,$var,$format,$classe).'</div>';
		return $r;
	}

	function debugShowVar($lib,$level,$commentaire='',$metaDatas='',$varNom='',$var='',$format='br',$classe=''){
		if(isset($this->libs[$lib]))return $this->libs[$lib]->debugShowVar($level,$commentaire,$metaDatas,$varNom,$var,$format,$classe);
		return '';
	}

}	// class gestionLibrairie

// - creation d'une instance prefedinie - //
$gestLib= new gestionLibrairies();
$gestLib->loadLib('gestLib',__FILE__,GESTLIBVERSION,'gestionnaire de librairies');
$gestLib->libs['gestLib']->git='https://git.framasoft.org/legraLibs/gestlib';
$gestLib->end('gestLib');

//echo  gestLib_inspect('gestLib',$gestLib,__LINE__);
?>
